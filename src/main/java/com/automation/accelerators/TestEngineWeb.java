package com.automation.accelerators;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.automation.report.ReporterConstants;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class TestEngineWeb {
	public final Logger LOG = Logger.getLogger(TestEngineWeb.class);
	protected WebDriver WebDriver = null;
	protected AppiumDriver appiumDriver = null;
	public static EventFiringWebDriver Driver = null;
	public DesiredCapabilities capabilitiesForAppium = new DesiredCapabilities();

	public static String browser = ReporterConstants.BROWSER_NAME;
	public static String platform = ReporterConstants.PLATFORM_TYPE;
	public static String appPackage = ReporterConstants.APP_PACKAGE;
	public static String appActivity = ReporterConstants.APP_ACTIVITY;
	public static String appiumUrl = ReporterConstants.APPIUM_URL;
	public static String deviceID = ReporterConstants.DEVICE_ID;
	public static String deviceName = ReporterConstants.DEVICE_NAME;
	public static String platformName = ReporterConstants.PLATFORM_NAME;
	public static String platformVersion = ReporterConstants.PLATFORM_VERSION;

	//@SuppressWarnings("static-access")
	public boolean driverInitiation(String url) throws IOException, InterruptedException {
		boolean flag = false;
		try {
			LOG.info(" Executing on " + platform);
			
			if(platform.equalsIgnoreCase("WEB")){
				switch (browser) {
				case "Firefox":
					Thread.sleep(13000);
					this.WebDriver = new FirefoxDriver();
					Thread.sleep(5000);
					break;

				case "ie":
					Thread.sleep(10000);
					LOG.info("iam in case IE");
					DesiredCapabilities capab = DesiredCapabilities.internetExplorer();
					capab.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
					capab.internetExplorer().setCapability("ignoreProtectedModeSettings", true);

					File file = new File("Drivers\\IEDriverServer.exe");
					System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
					capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					capab.setJavascriptEnabled(true);
					capab.setCapability("requireWindowFocus", true);
					capab.setCapability("enablePersistentHover", false);

					this.WebDriver = new InternetExplorerDriver(capab);
					Thread.sleep(8000);
					break;

				case "Chrome":

					Thread.sleep(2000);
					LOG.info("iam in case Chrome");
					System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
					DesiredCapabilities capabilities = DesiredCapabilities.chrome();
					ChromeOptions options = new ChromeOptions();
					options.addArguments("test-type");
					capabilities.setCapability(ChromeOptions.CAPABILITY, options);
					this.WebDriver = new ChromeDriver(capabilities);
					Thread.sleep(10000);
					break;

				case "Phantom":
					Thread.sleep(2000);
					LOG.info("i am in case PhantomJS");
					System.setProperty("phantomjs.binary.path", "Drivers\\phantomjs.exe");				
					this.WebDriver = new PhantomJSDriver();
					Thread.sleep(10000);
					break;	


				case "Safari":

					for (int i = 1; i <= 10; i++) {
						try {
							this.WebDriver = new SafariDriver();
							break;
						} catch (Exception e1) {
							Runtime.getRuntime().exec("taskkill /F /IM Safari.exe");
							Thread.sleep(3000);
							Runtime.getRuntime().exec("taskkill /F /IM plugin-container.exe");
							Runtime.getRuntime().exec("taskkill /F /IM WerFault.exe");
							continue;
						}
					}
				}
				Driver = new EventFiringWebDriver(this.WebDriver);
				MyListener myListener = new MyListener();
				Driver.register(myListener);
				Driver.get(url);
				Driver.manage().window().maximize();
				Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				LOG.info(" driver in test engine .." + Driver);
				Thread.sleep(3000);
			}
			else if (platform.equalsIgnoreCase("Android")) {				
				capabilitiesForAppium.setCapability(MobileCapabilityType.APP_PACKAGE, appPackage);
				capabilitiesForAppium.setCapability(MobileCapabilityType.APP_ACTIVITY, appActivity);
				//					capabilitiesForAppium.setCapability(MobileCapabilityType.APP, app);
				capabilitiesForAppium.setCapability(MobileCapabilityType.PLATFORM_NAME, platformName);
				capabilitiesForAppium.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
				capabilitiesForAppium.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
				appiumDriver = new AndroidDriver(new URL(appiumUrl),capabilitiesForAppium);
				Thread.sleep(2000);
				LOG.info("i am in case Android");	
			}			

			flag =true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
}
