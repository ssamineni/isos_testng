package com.automation.testrail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.automation.CSVUility.CsvHandler;
import com.automation.CSVUility.Xls_Reader;
import com.automation.accelerators.TestEngineWeb;
import com.automation.report.ReporterConstants;
import com.isos.tt.libs.CommonLib;
import com.sample.TestCaseCreationUtility.CreateDynamicTestCase;
import com.sample.TestCaseCreationUtility.GetLatestCodeFromBitBucket;

public class TestRail extends TestScriptDriver {

	public APIClient client;
	private JSONArray projects;
	private JSONArray activeProjects;
	private JSONArray mileStones;
	private JSONArray testSuites;
	private JSONArray testPlans;

	static String testcase_startTime;
	static String testcase_endTime;
	public String testPlanId;
	public String projectId;
	public String suiteId;

	public TestRail() {
		client = new APIClient(ReporterConstants.testRailUrl);
		client.setUser(GetLatestCodeFromBitBucket.decrypt_text(ReporterConstants.testRail_userName));
		client.setPassword(GetLatestCodeFromBitBucket.decrypt_text(ReporterConstants.testRail_paswd));
	}
	
//	String.valueOf(FindJArrayObjectKeyValue(runs, "name",
//			testRunName, "id"));

	public Object FindJArrayObjectKeyValue(JSONArray jArrayObject,
			String keyToSearch, String searchKeyValue, String keyValueToReturn) {

		for (Object jsonObject : jArrayObject) {
			String name = (String) GetJsonObjectKeyValue(jsonObject,
					keyToSearch);
			if (searchKeyValue.equals(name)) {
				return GetJsonObjectKeyValue(jsonObject, keyValueToReturn);
			}
		}
		return "";
	}

	@SuppressWarnings("unchecked")
	public Object FilterJArrayObjectByKeyValue(JSONArray jArrayObject,
			String filterKey, Object filterKeyValue) {
		JSONArray filteredJArray = new JSONArray();
		for (Object jsonObject : jArrayObject) {
			Object name = GetJsonObjectKeyValue(jsonObject, filterKey);
			if (filterKeyValue.equals(name)) {
				filteredJArray.add(jsonObject);
			}
		}
		return filteredJArray;
	}

	public Object GetJSONObjectFromJSONArrayByKeyValue(JSONArray jArrayObject,
			String filterKey, Object filterKeyValue) {
		for (Object jsonObject : jArrayObject) {
			Object name = GetJsonObjectKeyValue(jsonObject, filterKey);
			if (filterKeyValue.equals(name)) {
				return jsonObject;
			}
		}
		return null;
	}

	//change it to private - dont forget
	@SuppressWarnings("rawtypes")
	private Object GetJsonObjectKeyValue(Object object, String keyName) {
		JSONObject jsonObject = (JSONObject) object;
		Object returnVal = "";

		for (Iterator iterator = jsonObject.keySet().iterator(); iterator
				.hasNext();) {
			String key = (String) iterator.next();
			if (key.equals(keyName)) {
				returnVal = jsonObject.get(key);
				break;
			}
		}

		return returnVal;

	}

	public void GetProjects() {
		try {
			projects = (JSONArray) client.sendGet("get_projects");
			activeProjects = ((JSONArray) client
					.sendGet("get_projects&is_completed=0"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String GetProjectId(String projectName) {
		return String.valueOf(FindJArrayObjectKeyValue(projects, "name",
				projectName, "id"));
	}

	public String GetActiveProjectId(String projectName) {
		return String.valueOf(FindJArrayObjectKeyValue(activeProjects, "name",
				projectName, "id"));
	}

	public String GetActiveProjectMileStoneId(String projectName,
			String mileStoneName) {
		String projectId = GetActiveProjectId(projectName);
		try {
			mileStones = (JSONArray) client.sendGet("get_milestones/"
					+ projectId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return String.valueOf(FindJArrayObjectKeyValue(mileStones, "name",
				mileStoneName, "id"));
	}

	public String GetActiveTestSuiteId(String projectName, String testSuiteName) {
		String projectId = GetActiveProjectId(projectName);
		try {
			testSuites = (JSONArray) client.sendGet("get_suites/" + projectId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		suiteId = String.valueOf(FindJArrayObjectKeyValue(testSuites, "name",
				testSuiteName, "id"));
		return suiteId;
	}

	public String GetActiveTestPlanId(String projectName, String testPlanName) {
		projectId = GetActiveProjectId(projectName);
		try {
			testPlans = (JSONArray) client.sendGet("get_plans/" + projectId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return String.valueOf(FindJArrayObjectKeyValue(testPlans, "name",
				testPlanName, "id"));
	}

	public JSONArray GetActiveTestRuns(String projectName, String testPlanName,
			String testSuiteName) {
		testPlanId = GetActiveTestPlanId(projectName, testPlanName);
		try {
			JSONObject testPlan = (JSONObject) client.sendGet("get_plan/"
					+ testPlanId);
			LOG.info(testPlan);
			JSONArray suiteEntries = (JSONArray) GetJsonObjectKeyValue(
					testPlan, "entries");
			JSONArray runs = (JSONArray) FindJArrayObjectKeyValue(suiteEntries,
					"name", testSuiteName, "runs");
			return (JSONArray) FilterJArrayObjectByKeyValue(runs,
					"is_completed", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new JSONArray();
	}

	public String GetActiveTestRunId(String projectName, String testPlanName,
			String testSuiteName, String testRunName) {
		JSONArray runs = GetActiveTestRuns(projectName, testPlanName,
				testSuiteName);
		return String.valueOf(FindJArrayObjectKeyValue(runs, "name",
				testRunName, "id"));
	}
	
	public String getBrowserFromRun(String runId){
		String browser = "";
		try {
			JSONObject testRun = (JSONObject) client.sendGet("get_run/"+runId);
			if((String) testRun.get("config")!= null)
				browser = (String) testRun.get("config");
			else
				browser = ReporterConstants.BROWSER_NAME;; 
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return browser;
	}

	public List<String> GetActiveTestRunIdsForRun(String projectName, String testPlanName,
			String testSuiteName, String testRunName) {
		List<String> runIds = new ArrayList<String>();
		JSONArray runs = GetActiveTestRuns(projectName, testPlanName,
				testSuiteName);
		for (Object jsonObject : runs) {
			String name = (String) GetJsonObjectKeyValue(jsonObject,
					"name");
			if (testRunName.equals(name)) {
				LOG.info(GetJsonObjectKeyValue(jsonObject, "id"));
				runIds.add(GetJsonObjectKeyValue(jsonObject, "id").toString());
				LOG.info("---------");
			}
		}
		return runIds;
	}
	
	public JSONArray GetTestCasesFromTestRun(String runId) {
		/*String runId = GetActiveTestRunId(projectName, testPlanName,
				testSuiteName, testRunName);*/
		try {
			return (JSONArray) client.sendGet("get_tests/" + runId);
		} catch (Exception e) {
			e.printStackTrace();
			return new JSONArray();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void UpdateTestCaseStep(String testCaseName, String testCaseId,
			int testCaseStatus, String testCaseComments,
			List<Map> testStepResults) {
		Map stepDataDetail = new HashMap();
		stepDataDetail.put("custom_step_results", testStepResults);
		stepDataDetail.put("comment", testCaseComments);
		LOG.info("test case satus ----> " + testCaseStatus);
		stepDataDetail.put("status_id", testCaseStatus);

		try {
			client.sendPost("add_result/" + testCaseId, stepDataDetail);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes" })
	public HashMap loadDynamicClass(String currtestcase) {

		HashMap<Integer, Boolean> testStepStatus = new HashMap<Integer, Boolean>();
		Object o = null;
		// StepResult stepResult= new StepResult();
		// LOG.info("Execution started for the Test case : " + currtestcase);
		try {
			Class<?> c = Class.forName("com.isos.scripts." + currtestcase);
			o = c.newInstance();
			LOG.info("curr test case ---> " + currtestcase);
			Method m = c.getDeclaredMethod(currtestcase);
			testStepStatus = (HashMap<Integer, Boolean>) m.invoke(o, new Class[] {});

		} catch (Exception e) {
			e.printStackTrace();
		}

		o = null;
		return testStepStatus;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List updateTestStepResults(List testSteps, HashMap<Integer, String> testcasestatus,
			List expectedStatements, List actualStatements) {

		List<HashMap> testStepResults = new ArrayList<HashMap>();
		try {

			LOG.info("======== HasMap ======");
			LOG.info(testcasestatus);
			
			 Set<Integer> keys = testcasestatus.keySet();
			 
	         Iterator<Integer> iterator = keys.iterator();
	         List<Integer> keyList = new ArrayList<Integer>();
	         while(iterator.hasNext()) {
	        	 String i=iterator.next()+"";
	        	 keyList.add(Integer.parseInt(i));
	         }
	         Collections.sort(keyList);
	         for(int key: keyList){
	               	 
	        	 HashMap testStepResult = new HashMap();
					String stepContent = (String) testSteps.get(key-1);
					String stepExpected = (String) expectedStatements.get(key-1);
					testStepResult.put("content", stepContent);
					testStepResult.put("expected", stepExpected);
					testStepResult.put("actual", actualStatements.get(key-1));
					LOG.info("Test Step status testcasestatus.get(" + key	+ ") = " + (testcasestatus.get(String.valueOf(key))));
					testStepResult.put("status_id", (testcasestatus.get(String.valueOf(key))));
					testStepResults.add(testStepResult);
	         }
	         
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return testStepResults;
	}

	@SuppressWarnings("rawtypes")
	public int getTestcaseStatus(HashMap testcaseStatus) {
		int flag = 1;
		for (int i = 0; i < testcaseStatus.size(); i++) {
			if (testcaseStatus.get(i) == Boolean.FALSE) {
				flag = 5;
				break;
			}
		}
		return flag;
	}

	public void createScreenshotFolder(String testCasename) throws IOException {
		String path = TestScriptDriver.getScreenShotDirectoryPath();
		new File(path + "\\" + testCasename).mkdir();
		TestScriptDriver.setScreenShotDirectory_testCasePath(path + "\\"
				+ testCasename);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void ExecuteTestSuite(String projectName, String testPlanName,
			String testSuiteName, String testRunName, String runId) throws Exception {

		try {
			JSONArray testCases = GetTestCasesFromTestRun(runId);
			LOG.info("Number of test cases in the Suite - "
					+ testCases.size());
			Iterator itr = testCases.iterator();
			while (itr.hasNext()) {
				JSONObject testcase = (JSONObject) itr.next();
				String currtestcase = String.valueOf(GetJsonObjectKeyValue(
						testcase, "title"));
				String suiteId = GetActiveTestSuiteId(projectName,
						testSuiteName);
				JSONArray testSteps = (JSONArray) GetJsonObjectKeyValue(
						testcase, "custom_steps_separated");
				List<String> methodstrings = new ArrayList();
				List<String> expectedStrings = new ArrayList();
				String testCaseId = String.valueOf(GetJsonObjectKeyValue(
						testcase, "id"));
				for (Object testStep : testSteps) {
					// Map testStepResult = new HashMap();

					String stepContent = (String) GetJsonObjectKeyValue(
							testStep, "content");
					String stepExpected = (String) GetJsonObjectKeyValue(
							testStep, "expected");
					String command = stepContent.substring(
							stepContent.indexOf("'") + 1,
							stepContent.lastIndexOf("'"));
					methodstrings.add(command);
					expectedStrings.add(stepExpected);

				}
				
				//get runs, => their run ids, browser (config), if null get the property else the config from json
			//	List<String> runIds = GetActiveTestRunIdsForRun(projectName, testPlanName, testSuiteName, testRunName);
			//	for(String runId : runIds){
					
					
					CsvHandler.writeTestCase(
							projectName,
							GetActiveProjectId(projectName),
							testPlanName,
							GetActiveTestPlanId(projectName, testPlanName),
							testSuiteName,
							suiteId,
							testRunName, runId, currtestcase, testCaseId, getBrowserFromRun(runId));
					CsvHandler.writeTestCaseSteps(methodstrings, expectedStrings);
			//	}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void ExecuteTestCase(String projectName, String testPlanName,
			String testSuiteName, String testRunName, String testCaseName, String runId)
			throws Exception {

		try {
			JSONArray testCases = GetTestCasesFromTestRun(runId);
			JSONObject testCase = (JSONObject) GetJSONObjectFromJSONArrayByKeyValue(
					testCases, "title", testCaseName);
			
			JSONArray testSteps = (JSONArray) GetJsonObjectKeyValue(testCase,
					"custom_steps_separated");
			List<String> methodstrings = new ArrayList();
			List<String> expectedStrings = new ArrayList();
			String testCaseId = String.valueOf(GetJsonObjectKeyValue(testCase,
					"id"));
			for (Object testStep : testSteps) {
				String stepContent = (String) GetJsonObjectKeyValue(testStep,
						"content");
				String stepExpected = (String) GetJsonObjectKeyValue(testStep,
						"expected");
				String command = stepContent.substring(
						stepContent.indexOf("'") + 1,
						stepContent.lastIndexOf("'"));
				methodstrings.add(command);
				expectedStrings.add(stepExpected);
			}
			
		//	List<String> runIds = GetActiveTestRunIdsForRun(projectName, testPlanName, testSuiteName, testRunName);
			
			CsvHandler.writeTestCase(
					projectName,
					GetActiveProjectId(projectName),
					testPlanName,
					GetActiveTestPlanId(projectName, testPlanName),
					testSuiteName,
					GetActiveTestSuiteId(projectName, testSuiteName),
					testRunName, runId, testCaseName,
					testCaseId, getBrowserFromRun(runId));
			CsvHandler.writeTestCaseSteps(methodstrings, expectedStrings);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void readExcel_ExecuteTestCase(Xls_Reader xls) throws IOException,
	InterruptedException {
		try {
			String sheetName = "TestCaseResult";
			for (int i = 2; i <= xls.getRowCount(sheetName); i++) {

				String testCaseId = xls.getCellData(sheetName, "test_case_id", i);

				if (!testCaseId.isEmpty()) {
					String currtestcase = xls.getCellData(sheetName, "test_title", i);
					//totalTestCases.add(currtestcase);
					List methodStrings = new ArrayList();
					List expectedStrings = new ArrayList();
					// get the method string for the test case
					for (int j = i; j <= xls.getRowCount(sheetName); j++) {
						String command = xls.getCellData(sheetName, "Step", j);
						String expectedString = xls.getCellData(sheetName,
								"Expected_Result", j);
						if (command.isEmpty()) {
							break;
						}
						methodStrings.add(command);
						expectedStrings.add(expectedString);
					}
					
					for(Object method :  methodStrings)
					{
						
						
					}
					LOG.info(" ---------------------------");
					LOG.info("creating screenshot folder ..for "
							+ currtestcase);
					createScreenshotFolder(currtestcase);
					//set the browser in test web engine
					String browserToExecute = xls.getCellData(sheetName, "browser", i);
					TestEngineWeb.browser = browserToExecute;
					CreateDynamicTestCase.createTestClass(currtestcase,
							methodStrings);
					Thread.sleep(5000);
					String javaCommand = "javac -cp "
							+ System.getProperty("user.dir")
							+ "\\lib\\testng-6.0.jar;"
//							+ "C:/Users/E002443/.jenkins/jobs/ISOS_testNG/workspace/target/classes;"
							+ System.getProperty("user.dir")
							+ "\\classfiles -d "
							+ System.getProperty("user.dir")
							+ "\\classfiles "
							+ System.getProperty("user.dir")
							+ "\\src\\test\\java\\com\\isos\\scripts\\"
							+ currtestcase + ".java";
					LOG.info("JAVA command is.... "+  javaCommand );
					Runtime.getRuntime().exec(
							javaCommand);
					LOG.info("Waiting 10 seconds to place the class file....");
					Thread.sleep(10000);
					
					String startTestCaseTime = (CommonLib.getCurrentTime());
				    totalTestCases.add(Class.forName("com.isos.scripts."+currtestcase).newInstance());
		/*			
					Class<?> c = Class.forName("com.isos.scripts." + currtestcase);
					o = c.newInstance();
					LOG.info("curr test case ---> " + currtestcase);
					Method m = c.getDeclaredMethod(currtestcase);
					testStepStatus = (HashMap<Integer, Boolean>) m.invoke(o, new Class[] {});*/

					/*String endTestCaseTime = (CommonLib.getCurrentTime());
					CsvHandler.setTestCaseStatus(testCaseId,
							getTestcaseStatus(testStepStatus), startTestCaseTime, endTestCaseTime);
					CsvHandler.setTestStepStatus(testCaseId, testStepStatus,
							CommonLib.componentActualresult, CommonLib.componentStartTimer, CommonLib.componentEndTimer);

					CommonLib.componentActualresult.clear();
					CommonLib.componentStartTimer.clear();
					CommonLib.componentEndTimer.clear();
					LOG.info(" ---------------------------");*/
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}



	public void readConfigFile_WriteInExcel() throws Exception {

		BufferedReader br = null;
		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader(
					System.getProperty("user.dir")
							+ "/UploadData/testCaseConfig"));

			while ((sCurrentLine = br.readLine()) != null) {

				String[] parts = sCurrentLine.split("\\|");

				if (parts.length == 5) {
					String project = parts[0];
					String plan = parts[1];
					String suite = parts[2];
					String run = parts[3];
					String testcase = parts[4];

					LOG.info("==========");
					LOG.info("Project Name : " + project);
					LOG.info("Test Plan Name : " + plan);
					LOG.info("Test Suite Name : " + suite);
					LOG.info("Test Run Name : " + run);
					LOG.info("Test Case Name : " + testcase);
					LOG.info("==========");

					List<String> runIds = GetActiveTestRunIdsForRun(project, plan, suite, run);
					LOG.info("=====>size of runs -> " + runIds.size());
					for(String runId : runIds){
						LOG.info("======>run id ---> " + runId);
						ExecuteTestCase(project, plan, suite, run, testcase, runId);
					}
				} else {
					String project = parts[0];
					String plan = parts[1];
					String suite = parts[2];
					String run = parts[3];

					LOG.info("==========");
					LOG.info("Project Name : " + project);
					LOG.info("Test Plan Name : " + plan);
					LOG.info("Test Suite Name : " + suite);
					LOG.info("Test Run Name : " + run);
					LOG.info("==========");
					List<String> runIds = GetActiveTestRunIdsForRun(project, plan, suite, run);
					LOG.info("=====>size of runs -> " + runIds.size());
					for(String runId : runIds){
						LOG.info("======>run id ---> " + runId);
						ExecuteTestSuite(project, plan, suite, run, runId);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

/*	public List<Object> GetClassesList()
	{
		
		
		
		return classObjects;
	}
	*/
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean updateResultsToTestRailFromxcel(Xls_Reader xls) {

		Boolean flagtoCheckInsertinTestRail = false;
		try {

			String sheetName = "TestCaseResult";
			for (int i = 2; i <= xls.getRowCount(sheetName); i++) {

				String currtestcase = xls.getCellData(sheetName, "test_title", i);
				String testCaseId = xls.getCellData(sheetName, "test_case_id", i);
				
				List methodStrings = new ArrayList();
				List expectedStrings = new ArrayList();
				List actualStrings = new ArrayList();
				HashMap testStepStatus = new HashMap();

				if (!currtestcase.isEmpty()) {
					String testCaseStatus = xls.getCellData(sheetName, "test_Status", i);

					LOG.info("test -> " + currtestcase);

					// get the method string for the test case
					for (int j = i; j <= xls.getRowCount(sheetName); j++) {
						String command = xls.getCellData(sheetName, "Step", j);
						if (!command.isEmpty()) {
							String expectedString = xls.getCellData(sheetName,
									"Expected_Result", j);
							String actualString = xls.getCellData(sheetName,
									"Actual_Result", j);
							testStepStatus.put(xls.getCellData(sheetName,
									"Step_Id", j), xls.getCellData(sheetName,
									"Step_Status", j));
							LOG.info(" > > command - " + command);
							methodStrings.add(command);
							expectedStrings.add(expectedString);
							actualStrings.add(actualString);
						} else
							break;
					}
					LOG.info(" ---------------------------");

					List testStepLevelResults = updateTestStepResults(
							methodStrings, testStepStatus, expectedStrings,
							actualStrings);
					String testCaseComments = "Test is executed by automation.";
					UpdateTestCaseStep(currtestcase, testCaseId,
							Integer.parseInt(testCaseStatus),
							testCaseComments, testStepLevelResults);
					System.out
							.println("===>Now inserting yes in flag in excel <=== \n ");
					CsvHandler.setTrue_InsertResult_TestRail(currtestcase, testCaseId );
					flagtoCheckInsertinTestRail = true;

					methodStrings.clear();
					expectedStrings.clear();
					actualStrings.clear();
					testStepStatus.clear();
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return flagtoCheckInsertinTestRail;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean updateResultsToTestRailFromxcel_IfNotupdated(Xls_Reader xls) {

		Boolean flagtoCheckInsertinTestRail = false;
		try {

			String sheetName = "TestCaseResult";
			for (int i = 2; i <= xls.getRowCount(sheetName); i++) {

				String currtestcase = xls.getCellData(sheetName, "test_title", i);
				String testCaseId = xls.getCellData(sheetName, "test_case_id", i);
				List methodStrings = new ArrayList();
				List expectedStrings = new ArrayList();
				List actualStrings = new ArrayList();
				HashMap testStepStatus = new HashMap();

				if (!currtestcase.isEmpty()&&(!(xls.getCellData(sheetName, "updated_TestRail", i).equalsIgnoreCase("yes")))) {
					String testCaseStatus = xls.getCellData(sheetName, "test_Status", i);

					LOG.info("test -> " + currtestcase);

					// get the method string for the test case
					for (int j = i; j <= xls.getRowCount(sheetName); j++) {
						String command = xls.getCellData(sheetName, "Step", j);
						if (!command.isEmpty()) {
							String expectedString = xls.getCellData(sheetName,
									"Expected_Result", j);
							String actualString = xls.getCellData(sheetName,
									"Actual_Result", j);
							testStepStatus.put(xls.getCellData(sheetName,
									"Step_Id", j), xls.getCellData(sheetName,
									"Step_Status", j));
							LOG.info(" > > command - " + command);
							methodStrings.add(command);
							expectedStrings.add(expectedString);
							actualStrings.add(actualString);
						} else
							break;
					}
					LOG.info(" ---------------------------");

					List testStepLevelResults = updateTestStepResults(
							methodStrings, testStepStatus, expectedStrings,
							actualStrings);
					String testCaseComments = "Test is executed by automation.";
					UpdateTestCaseStep(currtestcase, testCaseId,
							Integer.parseInt(testCaseStatus),
							testCaseComments, testStepLevelResults);
					System.out
							.println("===>Now inserting yes in flag in excel <=== \n ");
					CsvHandler.setTrue_InsertResult_TestRail(currtestcase, testCaseId);
				

					methodStrings.clear();
					expectedStrings.clear();
					actualStrings.clear();
					testStepStatus.clear();
				}
			}
			flagtoCheckInsertinTestRail = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return flagtoCheckInsertinTestRail;
	}
	
}
