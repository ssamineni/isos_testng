package com.automation.testrail;

import io.appium.java_client.AppiumDriver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Factory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.automation.CSVUility.CsvHandler;
import com.automation.CSVUility.Xls_Reader;
import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.automation.database.Dbupdation;
import com.automation.report.ConfigFileReadWrite;
import com.automation.report.ReporterConstants;
import com.isos.tt.libs.*;

/**
 * @author E002149
 *
 */
public class TestScriptDriver {
	public final static Logger LOG = Logger.getLogger(TestScriptDriver.class);
	private static String screenShotPath;
	private static String screenShotPath_testCase;

	public static String executionStartTime;
	public static String testResultPath;
	public  static String currentDate;
	public  static String currtestcaseTitle;
	public static List<Object> totalTestCases = new ArrayList<Object>();	
	public static TestRail testRail;
	public static HashMap<Integer, Boolean> testStepStatus;
	public boolean proceedExecution;
	/** The nodes selected. */
	public String nodes = null;
	/**
	 * This method will decide the test run will happen in jenkins 
	 * or local env.
	 * 
	 * 
	 * @return class instantiated  object
	 * @throws IOException
	 * @throws InterruptedException
	 */
	@Factory
	public Object[] FactoryClass() throws IOException, InterruptedException
	{
		if(System.getenv("Env")!=null)
		{
			updateConfigurationFromJenkins();
			this.setUp();

		}else
		{
			this.setUp();
		}

		for(int i =0 ; i<totalTestCases.size(); i++)
		{  
			LOG.info("Testcase to Execute is: " + totalTestCases.get(i));  
		}
		List<Object> objectList1 = new ArrayList<Object>(totalTestCases);
		Object[] data = objectList1.toArray();
		return data;

	}

	/**
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private void updateConfigurationFromJenkins() throws IOException, InterruptedException 
	{
		LOG.info("Running from Jenkins Server");
		String url = System.getenv("EnvURL").trim().toString();
		selectTestsFromNodes();
		/*String propFile = System.getProperty("user.dir")+"/resources/gallopReporter";
		ConfigFileReadWrite.write(propFile, "testenv", url);
		String TestcaseConfig = System.getenv("Testcase_Selection").trim().toString();
		LOG.info(TestcaseConfig);
		String testConfigFilePath = System.getProperty("user.dir")+ "/UploadData/testCaseConfig";
		File f = new File(testConfigFilePath);		
		FileWriter fw = new FileWriter(f);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.flush();
		bw.write(TestcaseConfig);
		bw.close();*/
	}

	private void selectTestsFromNodes()
	{

		nodes = System.getenv("Nodes");
		if (nodes.equalsIgnoreCase("Node1")) {
			if (System.getenv("Test_Suite").equalsIgnoreCase("Smoke")) {
				ConfigFileReadWrite
				.writeToConfigFile(ReporterConstants.smoke_TT_Chrome
						.toString().trim());
				LOG.info("Running job from "+ nodes + " on Chrome browser");
			}

		} else if (nodes.equalsIgnoreCase("Node2")) {
			if (System.getenv("Test_Suite").equalsIgnoreCase("Smoke")) {
				ConfigFileReadWrite
				.writeToConfigFile(ReporterConstants.smoke_TT_FF
						.toString().trim());
				LOG.info("Running job from "+ nodes + " on Firefox browser");
			}
		} else if (nodes.equalsIgnoreCase("Node3")) {
			if (System.getenv("Test_Suite").equalsIgnoreCase("Smoke")) {
				ConfigFileReadWrite
				.writeToConfigFile(ReporterConstants.smoke_TT_IE
						.toString().trim());
				LOG.info("Running job from "+ nodes + " on IE browser");
			}
		}
	}


	private void setUp() throws IOException {
		testRail = new TestRail();
		testRail.GetProjects();
		try {
			PropertyConfigurator.configure(System.getProperty("user.dir")
					+ "/src/main/resources/log4j.properties");
			/*
			 * create a new folder name with timestamp under screenshot folder.
			 * To store the images caught in exception To write all the
			 * Testcases and its results in it under testresults folder.
			 */
			String directoryName = getDirectoryName();

			new File(".\\Screenshots\\" + directoryName).mkdir();
			System.out
			.println("Failure Screenshots are under this directory - "
					+ directoryName);
			setScreenShotDirectoryPath(".\\Screenshots\\" + directoryName);

			CsvHandler.CreateDir(directoryName);
			testResultPath = System.getProperty("user.dir") + "/TestResults"
					+ "/" + directoryName + "/Temp_TestRun.xlsx";
			CsvHandler.xls = new Xls_Reader(testResultPath);

			testRail.readConfigFile_WriteInExcel();
			// currentDate = getDate();

			testRail.readExcel_ExecuteTestCase(CsvHandler.xls);
			executionStartTime = CommonLib.getCurrentTime();


		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}





	/*public void setup() throws Exception {
		testRail = new TestRail();
		testRail.GetProjects();

		try {
			PropertyConfigurator.configure(System.getProperty("user.dir")
					+ "/src/main/resources/log4j.properties");

	 * create a new folder name with timestamp under screenshot folder.
	 * To store the images caught in exception To write all the
	 * Testcases and its results in it under testresults folder.

			String directoryName = getDirectoryName();

			new File(".\\Screenshots\\" + directoryName).mkdir();
			System.out
					.println("Failure Screenshots are under this directory - "
							+ directoryName);
			setScreenShotDirectoryPath(".\\Screenshots\\" + directoryName);

			CsvHandler.CreateDir(directoryName);
			testResultPath = System.getProperty("user.dir") + "/TestResults"
					+ "/" + directoryName + "/Temp_TestRun.xlsx";
			CsvHandler.xls = new Xls_Reader(testResultPath);

			testRail.readConfigFile_WriteInExcel();
			// currentDate = getDate();

			testRail.readExcel_ExecuteTestCase(CsvHandler.xls);
			executionStartTime = CommonLib.getCurrentTime();


		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}*/

	@AfterSuite
	public void afterSuite() {
		currentDate = getDate();

		String executionEndTime = CommonLib.getCurrentTime();
		CsvHandler.setExecutionDetails(currentDate, executionStartTime,
				executionEndTime);

		boolean flag_toInsertTestRail = testRail
				.updateResultsToTestRailFromxcel(CsvHandler.xls);
		String updatedFileName = CsvHandler
				.renameFileNameAfterTestRailUpdation(flag_toInsertTestRail,
						testResultPath);

		/*boolean flag_toInsertInDB = Dbupdation
				.enterDetailsInDatabaseAfterExecution(updatedFileName);
		String updatedFileNameAfterDbInsertion = CsvHandler
				.renameFileNameAfterDbInsertion(flag_toInsertInDB,
						updatedFileName);*/
		/*LOG.info(" File Name After DB Insertion ===> "
				+ updatedFileNameAfterDbInsertion);
		LOG.info(" File Name After DB Insertion ===> "
				+ updatedFileNameAfterDbInsertion);*/
		LOG.info(" File Name after Testrail results Insertion ===> "
				+ updatedFileName);

	}

	/*	@AfterClass
	public void afterClass() {

		String testCaseId = CsvHandler.getTestCaseId(currtestcaseTitle,
				TestEngineWeb.browser);
		String startTestCaseTime = (CommonLib.getCurrentTime());
		// HashMap<Integer, Boolean> testStepStatus = ;
		String endTestCaseTime = (CommonLib.getCurrentTime());
		CsvHandler.setTestCaseStatus(testCaseId,
				TestRail.getTestcaseStatus(testStepStatus), startTestCaseTime,
				endTestCaseTime);
		CsvHandler.setTestStepStatus(testCaseId, testStepStatus,
				CommonLib.componentActualresult, CommonLib.componentStartTimer,
				CommonLib.componentEndTimer);

		CommonLib.componentActualresult.clear();
		CommonLib.componentStartTimer.clear();
		CommonLib.componentEndTimer.clear();
		LOG.info(" ---------------------------");

	}*/

	public static String getDate() {

		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

		LOG.info("Current Date: " + ft.format(dNow));

		return ft.format(dNow);
	}

	private static String getDirectoryName() throws IOException {
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy_hh.mm.ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public static void setScreenShotDirectoryPath(String path)
			throws IOException {
		screenShotPath = path;

	}

	public static String getScreenShotDirectoryPath() throws IOException {
		return screenShotPath;

	}

	public static void setScreenShotDirectory_testCasePath(String path)
			throws IOException {
		screenShotPath_testCase = path;

	}

	public static String getScreenShotDirectory_testCasePath()
			throws IOException {
		return screenShotPath_testCase;

	}

}
