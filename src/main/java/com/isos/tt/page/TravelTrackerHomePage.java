package com.isos.tt.page;

import org.openqa.selenium.By;

public class TravelTrackerHomePage {

	public static By userName;
	public static By password;
	public static By loginButton;
	public static By travellerCount;
	public static By loadingImage;
	public static By messageIcon;
	public static By subject;
	public static By messageBody;
	public static By twoSMSResponse;
	public static By sendMessageButton;
	public static By travellerName;
	public static By email;
	public static By emailCheckbox;
	public static By phone;
	public static By phoneCheckbox;
	public static By messageLength;
	public static By closePopup;
	public static By communicationHistory;
	public static By messageType;
	public static By searchBtn;
	public static By searchResults;
	public static By searchResultsSubject;
	public static By logOff;
	public static By tools;
	public static By intlSOSPortal;
	public static By searchButton_isosPortal;
	public static By printButton;
	public static By exportToExcelBtn;
	public static By exportToZipBtn;
	public static By messageManagerLink;
	public static By helpLink;
	public static By userGuide;
	public static By feedbackLink;
	public static By userSettingsLink;
	public static By changePwdChallengeQstns;
	public static By updateEmailAddress;
	public static By updateButton;
	public static By welcomePage;
	public static By noOfPages;
	public static By intlSOS;
	public static By intlSOSURL;
	public static By resetBtn;
	public static By filtersBtn;
	public static By location;
	public static By locationValue;
	public static By now;
	public static By last31Days;
	public static By next24Hours;
	public static By next1to7Days;
	public static By next8to31Days;
	public static By ticketingStatusHeader;
	public static By alertCount;
	public static By internationalCheckbox;
	public static By domesticCheckbox;
	public static By expatriateCheckbox;
	public static By americas;
	public static By asiaAndPacific;
	public static By homeCountry;
	public static By extremeTravelChkbox;
	public static By highTravelChkbox;
	public static By mediumTravelChkbox;
	public static By lowTravelChkbox;
	public static By insignificantTravelChkbox;
	public static By extremeMedicalChkbox;
	public static By highMedicalChkbox;
	public static By mediumMedicalChkbox;
	public static By lowMedicalChkbox;
	public static By searchDropdown;
	public static By searchBox;
	public static By search;
	public static By tileContent;
	public static By locationName;
	public static By countryName;
	public static By travellerPanel;
	public static By locationsTravellerCount;
	public static By travellerCountPanel;
	public static By export;
	public static By messageBtn;
	public static By travellerNameSearch;
	public static By travellerDetailsHeader;
	public static By mapHomeTab;
	public static By year;
	public static By yearValue;
	public static By month;
	public static By monthValue;
	public static By day;
	public static By fromCalendarIcon;
	public static By dateRange;
	public static By flightCount;
	public static By flightName;
	public static By india;
	public static By zoomIn;
	public static By travellersList;
	public static By closeTravellersList;
	public static By travellerCheckbox;
	public static By sortBy;
	public static By country;
	public static By travellerCountSort;
	public static By alertsTab;
	public static By alertTile;
	public static By readMoreLink;
	public static By alertPopupHeader;
	public static By alertPopupLocation;
	public static By alertPopupEventDate;
	public static By closeAlertPopup;

	public void TravelTracker_Page()

	{

		travellerCount = By
				.xpath("//div[@id='tileContent']//following-sibling::ul//div[@id='location_11']//h2");
		loadingImage = By.xpath("//div[@id='tt-loading-overlay']");
		messageIcon = By.xpath("//div[@id='messageLink1']/a/img");
		subject = By.xpath("//input[@id='ctl00_MainContent_txtSubject']");
		messageBody = By
				.xpath("//textarea[@id='ctl00_MainContent_txtMessage']");
		twoSMSResponse = By
				.xpath("//input[@id='ctl00_MainContent_chkResponseOptions']");
		sendMessageButton = By
				.xpath("//input[@id='ctl00_MainContent_btnSendMessage']");
		travellerName = By
				.xpath("//span[@id='ctl00_MainContent_gvRecipients_ctl02_lblName']");
		email = By
				.xpath("//span[@id='ctl00_MainContent_gvRecipients_ctl02_lblEmail']");
		emailCheckbox = By
				.xpath("//input[@id='ctl00_MainContent_gvRecipients_ctl02_chkEmail']");
		phone = By
				.xpath("//span[@id='ctl00_MainContent_gvRecipients_ctl02_lblPhone']");
		phoneCheckbox = By
				.xpath("//input[@id='ctl00_MainContent_gvRecipients_ctl02_chkPhone']");
		messageLength = By
				.xpath("//span[@id='ctl00_MainContent_lblCharacterCount']");
		closePopup = By.cssSelector("#close-popup");
		communicationHistory = By.xpath("//a[text()='Communication History']");
		messageType = By
				.xpath("//select[@id='ctl00_MainContent_drpMessageTypeId']");
		searchBtn = By.xpath("//input[@id='ctl00_MainContent_btnSearch']");
		searchResults = By.xpath("//table[@id='ctl00_MainContent_gvResults']");
		searchResultsSubject = By
				.xpath("//table[@id='ctl00_MainContent_gvResults']//a[@id='ctl00_MainContent_gvResults_ctl02_lnkMessageID']");
		logOff = By.xpath("//li[@id='LogOff']");
		userName = By
				.xpath("//input[@id='ctl00_MainContent_LoginUser_txtUserName']");
		password = By
				.xpath("//input[@id='ctl00_MainContent_LoginUser_txtPassword']");
		loginButton = By
				.xpath("//input[@id='ctl00_MainContent_LoginUser_btnLogIn']");
		tools = By.xpath("//a[@id='ctl00_lnkTools']");
		intlSOSPortal = By.xpath("//li[@id='ctl00_lnkIntlSOSPortal']");
		searchButton_isosPortal = By
				.xpath("//input[@id='ctl00_MainContent_btnSearch']");
		printButton = By.xpath("//input[@id='ctl00_MainContent_btnPrint']");
		exportToExcelBtn = By
				.xpath("//input[@id='ctl00_MainContent_btnExport']");
		exportToZipBtn = By.xpath("//*[@id='ctl00_MainContent_btnZipExport']");
		messageManagerLink = By.xpath("//li[@id='ctl00_lnkMessageManager']");
		helpLink = By.xpath("//li[@id='Help']");
		userGuide = By.xpath("//div[contains(text(),'Users Guide')]");
		feedbackLink = By.xpath("//li[@id='Feedback']");
		userSettingsLink = By.xpath("//li[@id='ctl00_lnkUserSettings']");
		changePwdChallengeQstns = By
				.xpath("//a[@id='ctl00_MainContent_hlUpdatePassword']");
		updateEmailAddress = By
				.xpath("//a[@id='ctl00_MainContent_hlUpdateEmail']");
		updateButton = By
				.xpath("//input[@id='ctl00_MainContent_ProactiveEmails1_btnUpdate']");
		welcomePage = By.xpath("//span[contains(text(),'Welcome')]");
		noOfPages = By.xpath("//*[@id='numPages']");

		intlSOS = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustomers_ctl01_lstbSelectedCustomers']/option[text()='Test Customer 1']");
		intlSOSURL = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustomers_ctl01_lblEncryptedValue']");
		resetBtn = By.xpath("//label[text()='Reset Map']");
		filtersBtn = By.xpath("//div[@id='FilterButton']");
		location = By.xpath("//select[@id='presetSelect']");
		locationValue = By.xpath("//*[@id='presetSelect']/option[1]");
		last31Days = By.xpath("//input[@id='last31']");
		now = By.xpath("//input[@id='Now']");
		next24Hours = By.xpath("//input[@id='Next24h']");
		next1to7Days = By.xpath("//input[@id='Next1to7d']");
		next8to31Days = By.xpath("//input[@id='Next31d']");
		ticketingStatusHeader = By.xpath("//");
		alertCount = By.xpath("//h2[@title='Click to view details']");
		internationalCheckbox = By.xpath("//input[@id='international1']");
		domesticCheckbox = By.xpath("//input[@id='domestic1']");
		expatriateCheckbox = By.xpath("//input[@id='expatriate1']");
		homeCountry = By.xpath("//input[@id='country']");
		extremeTravelChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='extreme']");
		highTravelChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='high']");
		mediumTravelChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='medium']");
		lowTravelChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='low']");
		insignificantTravelChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Travel']/following-sibling::ul//label[@for='insignificant']");
		extremeMedicalChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='extreme1']");
		highMedicalChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='high1']");
		mediumMedicalChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='medium1']");
		lowMedicalChkbox = By
				.xpath("//div[@id='filterScreen']/div[@id='refineByTravellerType']//*[text()='Medical']/following-sibling::ul//label[@for='low1']");
		searchDropdown = By.xpath("//select[@id='searchoptions']");
		searchBox = By.xpath("//input[@name='deskSearch']");
		search = By.xpath("//button[@id='btnSearch']");
		tileContent = By.xpath("//div[@id='tileContent']");
		countryName = By
				.xpath("//div[@id='tileContent']//span[contains(text(),'Asia & the Pacific')]");
		locationsTravellerCount = By.xpath("//div[contains(text(),'People')]");
		travellerCountPanel = By.xpath("//div[@class='traveler-count-big']");
		export = By.xpath("//div[@id='exportLink']/a/img");
		messageBtn = By.xpath("//div[@id='messageLink1']/a/img");
		travellerDetailsHeader = By
				.xpath("//span[text()='Traveller Details']");
		mapHomeTab = By.xpath("//a[text()='Map Home']");
		year = By
				.xpath("//div[@class='pika-single is-bound']//select[@class='pika-select pika-select-year']");
		yearValue = By
				.xpath("//div[@class='pika-single is-bound']//select[@class='pika-select pika-select-year']/option[text()='2009']");
		month = By
				.xpath("//div[@class='pika-single is-bound']//select[@class='pika-select pika-select-month']");
		monthValue = By
				.xpath("//div[@class='pika-single is-bound']//select[@class='pika-select pika-select-month']/option[text()='May']");
		day = By.xpath("//div[@class='pika-single is-bound']//table//button[text()='8']");
		fromCalendarIcon = By.xpath("//li[@id='fromCalendarIcon']");
		dateRange = By.xpath("//li[text()='Date Range']");
		flightCount = By.xpath("//div[@id='tileContent']//div[contains(text(),'Flight')]");
		india = By
				.xpath("//div[@class='container search-container']//span[contains(text(),'[COUNTRY] India')]");
		zoomIn = By.xpath("//a[text()='+']");
		travellersList = By.xpath("//div[@id='travelerList']");
		closeTravellersList = By.xpath("//div[@id='closeTravelerDiv']//img");
		travellerCheckbox = By
				.xpath("//div[@id='scrollDiv']//li[1]//div[contains(@class,'travelDetail_Block')]//label");
		sortBy = By.xpath("//img[@title='Sort By']");
		country = By.xpath("//li[@id='alertsLocationNameField']");
		travellerCountSort = By.xpath("//li[@id='alertsTravelerCountField']");
		alertsTab = By.xpath("//div[@id='alertsTab']");
		alertTile = By.xpath("//ul[@id='alertItems']/li[1]//h1");
		readMoreLink = By
				.xpath("//ul[@id='alertItems']/li[1]//span[text()='Read more']");
		alertPopupHeader = By.xpath("//div[@id='container']//h1");
		alertPopupLocation = By.xpath("//i[text()='Location']");
		alertPopupEventDate = By.xpath("//i[text()='Event Date']");
		closeAlertPopup = By.xpath("//div[@id='close-popup']");
		americas = By.xpath("//h1[text()='Americas']");
		asiaAndPacific = By.xpath("//h1[text()='Asia & the Pacific']");

	}
}
