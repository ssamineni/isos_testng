package com.isos.tt.page;

import org.openqa.selenium.By;

public class ManualTripEntryPage {

	
	public static By manualTripEntryLink;
	public static By selectButton;
	public static By createNewTravellerBtn;
	public static By selectTraveller;
	/**
	 * FT_TT_ManualTripEntry_Create_new_traveller_and_Trip
	 */
	public static By firstName;
	public static By middleName;
	public static By lastName;
	public static By homeCountryList;
	public static By homeSite;
	public static By phonePriority;
	public static By phoneType;
	public static By countryCode;
	public static By phoneNumber;
	public static By emailPriority;
	public static By emailType;
	public static By emailAddress;
	public static By contractorId;
	public static By department;
	public static By documentCountryCode;
	public static By documentType;
	public static By comments;
	public static By saveTravellerDetails;
	public static By TravellerDetailsSuccessMessage;
	public static By travellerSearchBtn;
	public static By travellersDropDownPanel;
	public static By profileLookup;
	public static By selectTravellerBtn;
	public static By verifyTripName;
	public static By addTravellerbtn;
	public static By addTravellersDropDownPanel;
	public static By addTraveller;
	public static By copyTrip;
	public static By createNewTripTab;
	public static By addFlightTab;
	public static By airline;
	public static By airlineLoading;
	public static By flightDepartureCity;
	public static By flightDepartureCityLoading;
	public static By flightArrivalCity;
	public static By flightArrivalCityLoading;
	public static By flightNumber;
	public static By departureDate;
	public static By departureHr;
	public static By departureHrValue;
	public static By departureMin;
	public static By departureMinValue;
	public static By arrivalDate;
	public static By arrivalHr;
	public static By arrivalHrValue;
	public static By arrivalMin;
	public static By arrivalMinValue;
	public static By saveFlight;
	public static By imageProgress;
	public static By flightSuccessMessage;
	
	public static By addAccommodationTab;
	public static By hotelName;
	public static By checkInDate;
	public static By checkOutDate;
	public static By address;
	public static By city;
	public static By country;
	public static By accommodationCountry;
	public static By findButton;
	public static By spinnerImage;
	public static By suggestedAddress;
	public static By selectAddress;
	public static By saveAccommodation;
	public static By accommodationSuccessMessage;
	
	public static By addTrainTab;
	public static By trainCarrier;
	public static By trainCarrierValue;
	public static By trainDepartureCity;
	public static By trainDepartureCityLoading;
	public static By trainArrivalCity;
	public static By trainArrivalCityLoading;
	public static By trainNumber;
	public static By trainDepartureDate;
	public static By trainDepartureHr;
	public static By trainDepartureHrValue;
	public static By trainDepartureMin;
	public static By trainDepartureMinValue;
	public static By trainArrivalDate;
	public static By trainArrivalHr;
	public static By trainArrivalHrValue;
	public static By trainArrivalMin;
	public static By trainArrivalMinValue;
	public static By saveTrain;
	public static By trainSuccessMessage;
	
	public static By addGroundTransportTab;
	public static By transportName;
	public static By pickUpCityCountry;
	public static By pickUpCityCountryLoading;
	public static By dropOffCityCountry;
	public static By dropOffCityCountryLoading;
	public static By pickUpDate;
	public static By pickUpHr;
	public static By pickUpHrValue;
	public static By pickUpMin;
	public static By pickUpMinValue;
	public static By dropOffDate;
	public static By dropOffHr;
	public static By dropOffHrValue;
	public static By dropOffMin;
	public static By dropOffMinValue;
	public static By saveTransport;
	public static By groundTransportationSuccessMessage;
	public static By autoComplete;
	
	public void ManualTripEntry_Page()

	{
		manualTripEntryLink = By.xpath("//a[text()='Manual Trip Entry']");
		selectButton = By
				.xpath("//input[@id='ctl00_MainContent_btnAddProfile']");
		createNewTravellerBtn = By
				.xpath("//input[@id='ctl00_MainContent_btnCreateTraveler']");
		selectTraveller = By
				.xpath("//td[contains(text(),'Select Traveller')]");
		firstName = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtFirstName']");
		middleName = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtMiddleName']");
		lastName = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtLastName']");
		homeCountryList = By
				.xpath("//select[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_ddlHomeCountry']");
		homeSite = By
				.xpath("//select[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_ddlHomeSite']");
		phonePriority = By
				.xpath("//select[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_ddlPhonePriority']");
		phoneType = By
				.xpath("//select[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_ddlMobileType']");
		countryCode = By
				.xpath("//select[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_ddlCountry']");
		phoneNumber = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateProfile_ucPhoneDetails_gvPhoneDetail_ctl02_txtPhoneNo']");
		emailPriority = By
				.xpath("//select[@id='ctl00_MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ctl02_ddlEmailPriority']");
		emailType = By
				.xpath("//select[@id='ctl00_MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ctl02_ddlEmailType']");
		emailAddress = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateProfile_ucEmailDetails_gvEmailDatail_ctl02_txtEmailAddress']");
		contractorId = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateProfile_ucJobDetails_gvJobDetail_ctl02_txtEmpId']");
		department = By
				.xpath("//select[@id='ctl00_MainContent_ucCreateProfile_ucJobDetails_gvJobDetail_ctl02_ddlDept']");
		documentCountryCode = By
				.xpath(".//select[@id='ctl00_MainContent_ucCreateProfile_ucProfileDocumentDetails_gvDocumentDetail_ctl02_ddlCountry']");
		documentType = By
				.xpath(".//select[@id='ctl00_MainContent_ucCreateProfile_ucProfileDocumentDetails_gvDocumentDetail_ctl02_ddlDocumentTypeId']");
		comments = By
				.xpath(".//textarea[@id='ctl00_MainContent_ucCreateProfile_ucDefaultGroupDetails_txtComments']");
		saveTravellerDetails = By
				.xpath("//a[@id='ctl00_MainContent_btnUpdate']");
		TravellerDetailsSuccessMessage = By
				.xpath("//span[@id='ctl00_MainContent_ucCreateProfile_lblMessage']");
		travellerSearchBtn = By
				.xpath("//a[@id='ctl00_MiddleBarControl_linkTravelerSearch']");
		
		profileLookup = By
				.xpath("//input[@id='ctl00_MainContent_ucProfileLookup_autoCompleteTextProfilelookup']");
		selectTravellerBtn = By
				.xpath("//input[@id='ctl00_MainContent_btnAddProfile']");
		verifyTripName = By
				.xpath("//a[@id='ctl00_MainContent_ucTripList_gvTripDetail_ctl02_linkBtnTripNameOrPnr']");
		addTravellerbtn = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddTraveller_btnAddTraveller']");
		addTravellersDropDownPanel = By
				.xpath(".//ul[@id='ctl00_MainContent_ucCreateTrip_ucAddTraveller_autoComplete_completionListElem']/div[2]");
		addTraveller = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddTraveller_txtAddTraveller']");
		copyTrip = By
				.xpath("//a[@id='ctl00_MainContent_ucTripList_gvTripDetail_ctl02_linkBtnCopyTripNameOrPnr']");
		createNewTripTab = By
				.xpath("//a[@id='ctl00_MiddleBarControl_linkCreateNewTrip']");
		
		// Flight Details
				addFlightTab = By.xpath("//li[@id='addFlightBtn']/a");
				airline = By
						.xpath("//input[@id='MainContent_ucCreateTrip_ucAddEditFlightSegment_airline_txtSmartTextBoxAirline']");
				airlineLoading = By
						.xpath("//*[@id='MainContent_ucCreateTrip_ucAddEditFlightSegment_airline_autocompleteDropDownPanel']/div[1]");
				flightDepartureCity = By
						.xpath("//input[@id='MainContent_ucCreateTrip_ucAddEditFlightSegment_departureCity_txtSmartTextBoxAirport']");
				flightDepartureCityLoading = By
						.xpath("//*[@id='MainContent_ucCreateTrip_ucAddEditFlightSegment_departureCity_autocompleteDropDownPanel']/div[1]");
				flightArrivalCity = By
						.xpath("//input[@id='MainContent_ucCreateTrip_ucAddEditFlightSegment_arrivalCity_txtSmartTextBoxAirport']");
				flightArrivalCityLoading = By
						.xpath("//*[@id='MainContent_ucCreateTrip_ucAddEditFlightSegment_arrivalCity_autocompleteDropDownPanel']/div[1]");
				flightNumber = By
						.xpath("//input[@id='MainContent_ucCreateTrip_ucAddEditFlightSegment_txtAirlineNum']");
				departureDate = By
						.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtDepartureDate_txtDate')]");
				arrivalDate = By
						.xpath("//*[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtFlightArrivalDate_txtDate')]");
				departureHr = By
						.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtDepartureDate_cbHr')]");
				departureHrValue = By
						.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtDepartureDate_cbHr')]//option[text()='22']");
				departureMin = By
						.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtDepartureDate_cbMin')]");
				departureMinValue = By
						.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtDepartureDate_cbMin')]//option[text()='10']");
				arrivalHr = By
						.xpath("//select[@id='MainContent_ucCreateTrip_ucAddEditFlightSegment_txtFlightArrivalDate_cbHr']");
				arrivalHrValue = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddEditFlightSegment_txtFlightArrivalDate_cbHr']//option[text()='09']");
				arrivalMin = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddEditFlightSegment_txtFlightArrivalDate_cbMin']");
				arrivalMinValue = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddEditFlightSegment_txtFlightArrivalDate_cbMin']//option[text()='15']");
				imageProgress = By
						.xpath("//img[@id='ctl00_MainContent_ucCreateTrip_imgProgress']");
				saveFlight = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddEditFlightSegment_btnSaveFlight']");

				// Accommodation Details
				addAccommodationTab = By.xpath("//li[@id='addAccommodationBtn']/a");
				hotelName = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_txtHotelName']");
				checkInDate = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_dttimeCheckIn_txtDate']");
				checkOutDate = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_dttimeCheckOut_txtDate']");
				address = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_txtAddressNew']");
				city = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_txtCity']");
				country = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_txtCountry']");
				findButton = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_CallGeoCode']");
				spinnerImage = By
						.xpath("//*[@id='ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_spinnerImage']");
				suggestedAddress = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_lstAddressResults']/option[1]");
				selectAddress = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_btnOk']");
				saveAccommodation = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_btnSaveAccommodation']");

				// Train Details
				addTrainTab = By.xpath("//li[@id='addTrainBtn']/a");
				trainCarrier = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_ddlRailVendorsList']");
				trainCarrierValue = By
						.xpath("//*[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_ddlRailVendorsList']//option[text()='Amtrak (2V)']");
				trainDepartureCity = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_departureCity_txtSmartTextBoxRailStation']");
				trainDepartureCityLoading = By
						.xpath("//*[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_departureCity_autocompleteDropDownPanel']/div[1]");
				trainArrivalCity = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_arrivalCity_txtSmartTextBoxRailStation']");
				trainArrivalCityLoading = By
						.xpath("//*[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_arrivalCity_autocompleteDropDownPanel']/div[1]");
				trainNumber = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_txtTrainNum']");
				trainDepartureDate = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_txtDepartureDate_txtDate']");
				trainDepartureHr = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_txtDepartureDate_cbHr']");
				trainDepartureHrValue = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_txtDepartureDate_cbHr']//option[text()='23']");
				trainDepartureMin = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_txtDepartureDate_cbMin']");
				trainDepartureMinValue = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_txtDepartureDate_cbMin']//option[text()='10']");
				trainArrivalDate = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_txtArrivalDate_txtDate']");
				trainArrivalHr = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_txtArrivalDate_cbHr']");
				trainArrivalHrValue = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_txtArrivalDate_cbHr']//option[text()='08']");
				trainArrivalMin = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_txtArrivalDate_cbMin']");
				trainArrivalMinValue = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_txtArrivalDate_cbMin']//option[text()='15']");
				saveTrain = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_btnSaveTrain']");

				// Ground Transportation Details
				addGroundTransportTab = By.xpath("//li[@id='addLocalTransportBtn']/a");
				transportName = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtName']");
				pickUpCityCountry = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlPickupCityCountry_txtSmartTextBoxCityWithCountrty']");
				pickUpCityCountryLoading = By
						.xpath("//*[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlPickupCityCountry_autocompleteDropDownPanel']/div[1]");
				dropOffCityCountry = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlDropoffCityCountry_txtSmartTextBoxCityWithCountrty']");
				dropOffCityCountryLoading = By
						.xpath("//*[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlDropoffCityCountry_autocompleteDropDownPanel']/div[1]");
				pickUpDate = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtPickUpDate_txtDate']");
				pickUpHr = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtPickUpDate_cbHr']");
				pickUpHrValue = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtPickUpDate_cbHr']//option[text()='24']");
				pickUpMin = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtPickUpDate_cbMin']");
				pickUpMinValue = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtPickUpDate_cbMin']//option[text()='15']");
				dropOffDate = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtDropOffDate_txtDate']");
				dropOffHr = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtDropOffDate_cbHr']");
				dropOffHrValue = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtDropOffDate_cbHr']//option[text()='06']");
				dropOffMin = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtDropOffDate_cbMin']");
				dropOffMinValue = By
						.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtDropOffDate_cbMin']//option[text()='10']");
				saveTransport = By
						.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_btnSavetransport']");
				autoComplete = By
						.xpath("//input[@id='hfAutoCompleteExtenderResultCount']");
		
	}
}
