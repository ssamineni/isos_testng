package com.isos.tt.page;

import org.openqa.selenium.By;

public class MyTripsPage {

	/**
	 * FT_TT_Communication_Send_Message_by_Email_SMS_and_ViewHistory
	 */
	public static By userName;
	public static By password;
	public static By loginButton;
	public static By createNewTripTab;
	public static By logOff;
	public static By tripName;
	public static By ticketCountry;
	public static By addFlightTab;
	public static By airline;
	public static By airlineLoading;
	public static By flightDepartureCity;
	public static By flightDepartureCityLoading;
	public static By flightArrivalCity;
	public static By flightArrivalCityLoading;
	public static By flightNumber;
	public static By departureDate;
	public static By departureHr;
	public static By departureHrValue;
	public static By departureMin;
	public static By departureMinValue;
	public static By arrivalDate;
	public static By arrivalHr;
	public static By arrivalHrValue;
	public static By arrivalMin;
	public static By arrivalMinValue;
	public static By saveFlight;
	public static By imageProgress;
	public static By addAccommodationTab;
	public static By hotelName;
	public static By checkInDate;
	public static By checkOutDate;
	public static By address;
	public static By city;
	public static By country;
	public static By findButton;
	public static By spinnerImage;
	public static By suggestedAddress;
	public static By selectAddress;
	public static By accommodationType;
	public static By saveAccommodation;
	public static By trainCarrier;
	public static By trainCarrierValue;
	public static By trainDepartureCity;
	public static By trainDepartureCityLoading;
	public static By trainArrivalCity;
	public static By trainArrivalCityLoading;
	public static By trainNumber;
	public static By trainDepartureDate;
	public static By trainDepartureHr;
	public static By trainDepartureHrValue;
	public static By trainDepartureMin;
	public static By trainDepartureMinValue;
	public static By trainArrivalDate;
	public static By trainArrivalHr;
	public static By trainArrivalHrValue;
	public static By trainArrivalMin;
	public static By trainArrivalMinValue;
	public static By saveTrain;
	public static By transportName;
	public static By pickUpCityCountry;
	public static By pickUpCityCountryLoading;
	public static By dropOffCityCountry;
	public static By dropOffCityCountryLoading;
	public static By pickUpDate;
	public static By pickUpHr;
	public static By pickUpHrValue;
	public static By pickUpMin;
	public static By pickUpMinValue;
	public static By dropOffDate;
	public static By dropOffHr;
	public static By dropOffHrValue;
	public static By dropOffMin;
	public static By dropOffMinValue;
	public static By saveTransport;
	public static By addTrainTab;
	public static By addGroundTransportTab;
	public static By abroadCountryCode;
	public static By abroadCountryCodeValue;
	public static By abroadPhoneNumber;
	public static By contactName;
	public static By contactRelationship;
	public static By emergencyPhoneNo;
	public static By overseasProgram;
	public static By deanName;
	public static By visitType;
	public static By visitTypeValue;

	public static By flightSuccessMessage;
	public static By accommodationSuccessMessage;
	public static By trainSuccessMessage;
	public static By groundTransportationSuccessMessage;
	public static By saveTripInfo;
	public static By tripInfoSuccessMessage;
	public static By myProfileTab;
	public static By verifyTripName;
	public static By verifyTripName_myTrips;
	public static By newUserRegistration;
	public static By title;
	public static By firstName;
	public static By lastName;
	public static By email;
	public static By pwd;
	public static By reenterPwd;
	public static By securityQstn1;
	public static By securityQstn1Option;
	public static By securityQstn2;
	public static By securityQstn2Option;
	public static By securityAnswer1;
	public static By securityAnswer2;
	public static By submitButton;
	public static By myTripsSuccessRegistrationMsg;

	public static By profileGroupTab;
	public static By profileFieldsTab;
	public static By tripSegmentsTab;
	public static By unmappedProfileFieldsTab;
	public static By metadataTripQuestionTab;
	public static By customTripQuestionTab;
	public static By authorizedMyTripsCustTab;
	public static By selectProfileGroup;
	public static By profileGroupLabel;
	public static By updateLabelButton;
	public static By groupNameSuccessMsg;
	public static By profileGroupSaveBtn;
	public static By profileGroupSuccessMsg;
	public static By attributeGroup;
	public static By availableProfileFields;
	public static By selectedProfileFields;
	public static By labelForProfileField;
	public static By gender;
	public static By isRequiredOnFormCheckbox;
	public static By profileFieldMiddleName;
	public static By profileFieldUpdateLabel;
	public static By profileFieldUpdateSuccessMsg;
	public static By profileFieldSaveBtn;
	public static By profileFieldSuccessMsg;
	public static By availableSegments;
	public static By selectedSegments;
	public static By availableFields;
	public static By selectedFields;
	public static By accountNo;
	public static By addBtn;
	public static By umappedProFldSaveBtn;
	public static By umappedProFldSuccessMsg;
	public static By availableMetadataTripQstns;
	public static By selectedMetadataTripQstns;
	public static By labelName;
	public static By responseType;
	public static By availableCustomTripQstns;
	public static By selectedCustomTripQstns;
	public static By newBtn;
	public static By defaultQstn;
	public static By qstnText;
	public static By responseTypeCTQ;
	public static By applySettings;
	public static By updateCustomTripQstn;
	public static By saveChangesBtn;
	public static By CTQSuccessMsg;
	public static By availableCustomers;
	public static By selectedCustomers;
	public static By encryptedCustomerId;

	public void MyTrips_Page()

	{

		logOff = By.xpath("//a[@id='HeaderControl_Logoff']");
		userName = By
				.xpath("//input[@id='MainContent_LoginUser_txtUserName']");
		password = By
				.xpath("//input[@id='MainContent_LoginUser_txtPassword']");
		loginButton = By
				.xpath("//input[@id='MainContent_LoginUser_btnLogIn']");

		createNewTripTab = By
				.xpath("//a[@id='MiddleBarControl_linkCreateNewTrip']");
		tripName = By
				.xpath("//input[contains(@id,'ctl00_MainContent_ucCreateTrip_ucTripInformation_txtTripName')]");
		ticketCountry = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_125']");
		// Flight Details
		addFlightTab = By.xpath("//li[@id='addFlightBtn']/a");
		airline = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_airline_txtSmartTextBoxAirline')]");
		airlineLoading = By
				.xpath("//div[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_airline_autocompleteDropDownPanel')]/div");
		flightDepartureCity = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_departureCity_txtSmartTextBoxAirport')]");
		flightDepartureCityLoading = By
				.xpath("//div[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_departureCity_autocompleteDropDownPanel')]/div");
		flightArrivalCity = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_arrivalCity_txtSmartTextBoxAirport')]");
		flightArrivalCityLoading = By
				.xpath("//div[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_arrivalCity_autocompleteDropDownPanel')]/div");
		flightNumber = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtAirlineNum')]");
		departureDate = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtDepartureDate_txtDate')]");
		arrivalDate = By
				.xpath("//*[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtFlightArrivalDate_txtDate')]");
		departureHr = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtDepartureDate_cbHr')]");
		departureHrValue = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtDepartureDate_cbHr')]//option[text()='22']");
		departureMin = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtDepartureDate_cbMin')]");
		departureMinValue = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtDepartureDate_cbMin')]//option[text()='10']");
		arrivalHr = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtFlightArrivalDate_cbHr')]");
		arrivalHrValue = By
				.xpath("./select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtFlightArrivalDate_cbHr')]//option[text()='09']");
		arrivalMin = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtFlightArrivalDate_cbMin')]");
		arrivalMinValue = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_txtFlightArrivalDate_cbMin')]//option[text()='15']");
		imageProgress = By
				.xpath("//img[@id='MainContent_ucCreateTrip_imgProgress']");
		saveFlight = By
				.xpath("//input[contains(@id,'ctl00_MainContent_ucCreateTrip_ucAddEditFlightSegment_btnSaveFlight')]");

		// Accommodation Details
		addAccommodationTab = By.xpath("//li[@id='addAccommodationBtn']/a");
		hotelName = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_txtHotelName')]");
		checkInDate = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_dttimeCheckIn_txtDate')]");
		checkOutDate = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_dttimeCheckOut_txtDate')]");
		address = By
				.xpath("//input[contains(@id,'ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_txtAddressNew')]");
		city = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_txtCity')]");
		country = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_txtCountry')]");
		findButton = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_CallGeoCode')]");
		spinnerImage = By
				.xpath("//*[@id='MainContent_ucCreateTrip_ucAddEditAccommodationSegment_spinnerImage']");
		suggestedAddress = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_lstAddressResults')]/option[1]");
		selectAddress = By
				.xpath("//input[contains(@id,'ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_btnOk')]");
		accommodationType = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddEditAccommodationSegment_ddlAccommodationTypeNew')]");
		saveAccommodation = By
				.xpath("//input[contains(@id,'ctl00_MainContent_ucCreateTrip_ucAddEditAccommodationSegment_btnSaveAccommodation')]");

		// Train Details
		addTrainTab = By.xpath("//li[@id='addTrainBtn']/a");
		trainCarrier = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_ddlRailVendorsList')]");
		trainCarrierValue = By
				.xpath("//*[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_ddlRailVendorsList')]//option[text()='Amtrak (2V)']");
		trainDepartureCity = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_departureCity_txtSmartTextBoxRailStation')]");
		trainDepartureCityLoading = By
				.xpath("//div[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_departureCity_autocompleteDropDownPanel')]/div[1]");
		trainArrivalCity = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_arrivalCity_txtSmartTextBoxRailStation')]");
		trainArrivalCityLoading = By
				.xpath("//div[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_arrivalCity_autocompleteDropDownPanel')]/div[1]");
		trainNumber = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtTrainNum')]");
		trainDepartureDate = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtDepartureDate_txtDate')]");
		trainDepartureHr = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtDepartureDate_cbHr')]");
		trainDepartureHrValue = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtDepartureDate_cbHr')]//option[text()='23']");
		trainDepartureMin = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtDepartureDate_cbMin')]");
		trainDepartureMinValue = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtDepartureDate_cbMin')]//option[text()='10']");
		trainArrivalDate = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtArrivalDate_txtDate')]");
		trainArrivalHr = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtArrivalDate_cbHr')]");
		trainArrivalHrValue = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtArrivalDate_cbHr')]//option[text()='08']");
		trainArrivalMin = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtArrivalDate_cbMin')]");
		trainArrivalMinValue = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_txtArrivalDate_cbMin')]//option[text()='15']");
		saveTrain = By
				.xpath("//input[contains(@id,'ctl00_MainContent_ucCreateTrip_ucAddTrainSegment_btnSaveTrain')]");

		// Ground Transportation Details
		addGroundTransportTab = By.xpath("//li[@id='addLocalTransportBtn']/a");
		transportName = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtName')]");
		pickUpCityCountry = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlPickupCityCountry_txtSmartTextBoxCityWithCountrty')]");
		pickUpCityCountryLoading = By
				.xpath("//div[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlPickupCityCountry_autocompleteDropDownPanel')]/div[1]");
		dropOffCityCountry = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlDropoffCityCountry_txtSmartTextBoxCityWithCountrty')]");
		dropOffCityCountryLoading = By
				.xpath("//*[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlDropoffCityCountry_autocompleteDropDownPanel')]/div[1]");
		pickUpDate = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtPickUpDate_txtDate')]");
		pickUpHr = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtPickUpDate_cbHr')]");
		pickUpHrValue = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtPickUpDate_cbHr')]//option[text()='24']");
		pickUpMin = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtPickUpDate_cbMin')]");
		pickUpMinValue = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtPickUpDate_cbMin')]//option[text()='15']");
		dropOffDate = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtDropOffDate_txtDate')]");
		dropOffHr = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtDropOffDate_cbHr')]");
		dropOffHrValue = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtDropOffDate_cbHr')]//option[text()='06']");
		dropOffMin = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtDropOffDate_cbMin')]");
		dropOffMinValue = By
				.xpath("//select[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_txtDropOffDate_cbMin')]//option[text()='10']");
		saveTransport = By
				.xpath("//input[contains(@id,'ctl00_MainContent_ucCreateTrip_ucAddGroundTransportSegment_btnSavetransport')]");

		abroadCountryCode = By
				.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_tblPhone4090_4090']");
		abroadCountryCodeValue = By
				.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_tblPhone4090_4090']//option[text()='American Samoa - 011-684']");
		abroadPhoneNumber = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_tblPhone4090_txtSmartPhoneNumber4090']");
		contactName = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_CTQ4197']");
		contactRelationship = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_CTQ4198']");
		emergencyPhoneNo = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_CTQ4199']");
		overseasProgram = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_CTQ4200']");
		deanName = By
				.xpath("//input[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_CTQ4201']");
		visitType = By
				.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_CTQ4202']");
		visitTypeValue = By
				.xpath("//select[@id='ctl00_MainContent_ucCreateTrip_ucTripInformation_CTQ4202']//option[text()='Personal']");

		flightSuccessMessage = By
				.xpath("//span[text()='Your flight details saved successfully.']");
		accommodationSuccessMessage = By
				.xpath("//span[text()='Your accommodation details saved successfully.']");
		trainSuccessMessage = By
				.xpath("//span[text()='Your train details saved successfully.']");
		groundTransportationSuccessMessage = By
				.xpath("//span[text()='Your ground transportation details saved successfully.']");

		saveTripInfo = By
				.xpath("//input[contains(@id,'MainContent_ucCreateTrip_btnSaveTripInformation')]");
		// after clicking this image progresss comes
		tripInfoSuccessMessage = By
				.xpath("//span[text()='Your trip information saved successfully']");

		myProfileTab = By.xpath("//a[@id='MiddleBarControl_linkMyProfile']");
		verifyTripName_myTrips = By
				.xpath("//table[@id='MainContent_ucTripList_gvTripDetail']");

		newUserRegistration = By
				.xpath("//a[@id='MainContent_LoginUser_lnkBtnRegistration']");
		title = By
				.id("MainContent_ucRegistrationControl_ddlTitle");
		firstName = By
				.xpath("//input[@id='MainContent_ucRegistrationControl_txtFirtsName']");
		lastName = By
				.xpath("//input[@id='MainContent_ucRegistrationControl_txtLastName']");
		email = By
				.xpath("//input[@id='MainContent_ucRegistrationControl_txtEmail']");
		pwd = By.xpath("//input[@id='MainContent_ucRegistrationControl_txtPassword']");
		reenterPwd = By
				.xpath("//input[@id='MainContent_ucRegistrationControl_txtReEnter']");
		securityQstn1 = By
				.xpath("//select[@id='MainContent_ucRegistrationControl_ddlSecurityQuestion1']");
		securityQstn1Option = By
				.xpath("//select[@id='MainContent_ucRegistrationControl_ddlSecurityQuestion1']/option[contains(text(),'teacher')]");
		securityQstn2 = By
				.xpath("//*[@id='MainContent_ucRegistrationControl_ddlSecurityQuestion2']");
		securityQstn2Option = By
				.xpath("//*[@id='MainContent_ucRegistrationControl_ddlSecurityQuestion2']/option[text()='Your first car?']");
		securityAnswer1 = By
				.xpath("//input[@id='MainContent_ucRegistrationControl_txtAnswer1']");
		securityAnswer2 = By
				.xpath("//input[@id='MainContent_ucRegistrationControl_txtAnswer2']");
		submitButton = By
				.xpath("//input[@id='MainContent_ucRegistrationControl_btnSubmit']");
		myTripsSuccessRegistrationMsg = By
				.xpath("//div[@id='windowMiddle2']/div[contains(text(),'Thank you')]");

		profileGroupTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrGroup']");
		profileFieldsTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrField']");
		tripSegmentsTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment']");
		unmappedProfileFieldsTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfileExtendedAttributesGroups']");
		metadataTripQuestionTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta']");
		customTripQuestionTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn']");
		authorizedMyTripsCustTab = By
				.xpath("//span[@id='__tab_ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustomers']");

		selectProfileGroup = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrGroup_ctl01_listBoxSelectedGroups']/option[1]");
		profileGroupLabel = By.xpath("//input[@id='txtLabelValue']");
		updateLabelButton = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrGroup_ctl01_btnUpdateAttributeGroupLabel']");
		groupNameSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrGroup_ctl01_lblLabelMsg']");
		profileGroupSaveBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrGroup_ctl01_btnSave']");
		profileGroupSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrGroup_ctl01_labelErrorMessage']");

		attributeGroup = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrField_ctl01_ddlAttributeGroup']");
		availableProfileFields = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrField_ctl01_lstbAvailableProfileAttributes']");
		selectedProfileFields = By
				.xpath("//select[@id='lstbSelectedProfileAttributesFields']");
		labelForProfileField = By.xpath("//input[@id='txtFieldValue']");
		gender = By
				.xpath("//select[@id='lstbSelectedProfileAttributesFields']/option[2]");
		isRequiredOnFormCheckbox = By
				.xpath("//input[@id='chkIsRequiredProfileField']");
		profileFieldMiddleName = By
				.xpath("//select[@id='lstbSelectedProfileAttributesFields']/option[6]");
		profileFieldUpdateLabel = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrField_ctl01_btnUpdateLabel']");
		profileFieldUpdateSuccessMsg = By
				.xpath("//span[@id='ProfileAttributelblLabelMsg']");
		profileFieldSaveBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfAttrField_ctl01_btnSave']");
		profileFieldSuccessMsg = By.xpath("//span[@id='lblMessage']");

		availableSegments = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_listBoxAvailableSegment']");
		selectedSegments = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLViewSegment_ctl01_listBoxSelectedSegment']");

		availableFields = By
				.xpath("//div[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfileExtendedAttributesGroups_panelProfileExtendedAttributesGroups']/table/tbody/tr[1]/td/table/tbody/tr[2]/td[1]");
		selectedFields = By
				.xpath("//select[@id='listBoxSelectedExtendedAttributes']");
		accountNo = By
				.xpath("//select[@id='listBoxAvailableExtendedAttributes']/option[1]");
		addBtn = By.xpath("//input[@id='btnAdd']");
		umappedProFldSaveBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLProfileExtendedAttributesGroups_ctl01_btnSave']");
		umappedProFldSuccessMsg = By
				.xpath("//span[@id='labelExtendedAttributeErrorMessage']");

		availableMetadataTripQstns = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lstbAvailableProfileAttributes']");
		selectedMetadataTripQstns = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_lstbSelectedProfileAttributes']");
		labelName = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_txtLabelValue']");
		responseType = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustmMeta_ctl01_ddlResponseType']");

		availableCustomTripQstns = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_lstbAvailableProfileAttributes']");
		selectedCustomTripQstns = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_lstbSelectedProfileAttributes']");
		newBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_btnNew']");
		defaultQstn = By
				.xpath("//*[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_lstbSelectedProfileAttributes']/option[text()='Default Question Text']");
		qstnText = By
				.xpath("//textarea[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_txtCustomTripQuestionValue']");
		responseTypeCTQ = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_ddlResponseType']");
		applySettings = By.xpath("//input[@id='chkSetOnToClone']");
		updateCustomTripQstn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_btnUpdateLabel']");
		saveChangesBtn = By
				.xpath("//input[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_btnSave']");
		CTQSuccessMsg = By
				.xpath("//span[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPTLPnlCustmQtn_ctl01_lblMessage']");
		availableCustomers = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustomers_ctl01_lstbAvailableCustomers']");
		selectedCustomers = By
				.xpath("//select[@id='ctl00_ContentPlaceHolder1_MainContent_tabContainerPTL_tabPnlPTLCustomers_ctl01_lstbSelectedCustomers']");
		encryptedCustomerId = By
				.xpath("//span[text()='Encrypted CustomerId for']");

	}
}
