package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;

import com.automation.accelerators.TestEngineWeb;
import com.isos.ecms.page.ContentEditorPage;
import com.isos.tt.page.MyTripsPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TravelTrackerHomePage;

public class SiteAdminLib extends CommonLib {
	
	private static final Logger LOG = Logger.getLogger(SiteAdminLib.class);

	TestEngineWeb tWeb = new TestEngineWeb();
	/*static String component_startTime;
	static String component_endTime;
	static String methodName;

	public static List componentNamesList = new ArrayList();
	public static List componentStartTimer = new ArrayList();
	public static List componentEndTimer = new ArrayList();*/

	@SuppressWarnings("unchecked")
	public boolean verifySiteAdminpage(String custName) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			LOG.info("customer name is : " + custName);
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "Loading Image"));
			flags.add(waitForElementPresent(
					SiteAdminPage.siteAdminLink, "siteAdminLink", 60));
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink,
					"Site Admin Link"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab,
					"General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown,
					"Customer drop down"));
			flags.add(click(SiteAdminPage.customerName, "Customer Name"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.segmentationTab, "segmentationTab", 60));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified Site Admin Page successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
			.add("Verification of Site Admin Page is failed.");
		}
		return flag;
	}

	/**
	 * This method is used to add two integers. This is a the simplest form of a
	 * class method, just to show the usage of various javadoc Tags.
	 * 
	 * @return boolean This returns boolean value.
	 * @throws SiteAdminLib
	 *             throws an exception
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyManualTripEntryTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(isElementPresent(SiteAdminPage.manualTripEntryTab,
					"Manual TripEntry Tab"));
			flags.add(click(SiteAdminPage.manualTripEntryTab,
					"Manual TripEntry Tab"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.profileGrouptab,
					"profileGrouptab"));
			flags.add(assertElementPresent(SiteAdminPage.profileGrouptab,
					"profileGrouptab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified Manual Trip Entry successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Manual Trip Entry verificatin failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyMyTripsPage() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			flags.add(isElementPresent(SiteAdminPage.myTripsTab,
					"MyTrips Tab"));
			flags.add(click(SiteAdminPage.myTripsTab, "MyTrips Tab"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.profileFieldsTab,
					"Profile Fields Tab"));
			flags.add(assertElementPresent(MyTripsPage.profileFieldsTab,
					"Profile Fields Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified MyTrips page successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("MyTrips page verification failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifySegmentationPage() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(isElementPresent(SiteAdminPage.segmentationTab,
					"segmentation Tab"));
			flags.add(click(SiteAdminPage.segmentationTab,
					"segmentation Tab"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.assignGrpUserTab,
					"assignGrpUserTab"));
			flags.add(assertElementPresent(SiteAdminPage.assignGrpUserTab,
					"Assign Group To User Tab"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified Segmentation page successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Segmentation page verification failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean getNewUserRegURL() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink,
					"Site Admin Link"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link"));
			flags.add(click(SiteAdminPage.myTripsTab, "My Trips Tab"));
			flags.add(isElementPresent(TravelTrackerHomePage.intlSOS, "International SOS"));
			flags.add(click(TravelTrackerHomePage.intlSOS, "International SOS"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(isElementPresent(TravelTrackerHomePage.intlSOSURL,
					"International SOS URL"));
			String url = getText(TravelTrackerHomePage.intlSOSURL,
					"International SOS URL");
			LOG.info("URL is : " + url);
			Driver.navigate().to(url);
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("New User Registration URL is fetched successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("New User Registration URL fetching failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean clickSiteAdmin() throws Throwable {
		boolean flag = true;
		try {

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink,
					"Site Admin Link"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link"));

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean clickSegmentationTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(isElementPresent(SiteAdminPage.segmentationTab,
					"segmentationTab"));
			flags.add(click(SiteAdminPage.segmentationTab,
					"segmentationTab"));
			flags.add(waitForElementPresent(
					SiteAdminPage.assignGrpUserTab, "assignGrpUserTab", 60));
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Segmentation tab is clicked successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Clicking on Segmentation tab failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigateUserTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(isElementPresent(SiteAdminPage.userTab, "User tab"));
			flags.add(click(SiteAdminPage.userTab, "User tab"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForVisibilityOfElement(
					SiteAdminPage.selectUser_userTab,
					"User"));
			flags.add(selectByIndex(SiteAdminPage.selectUser_userTab, 3,
					"User"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "Loading Image"));
			flags.add(waitForElementPresent(
					SiteAdminPage.userName_userTab,
					"User name", 60));
			flags.add(assertElementPresent(SiteAdminPage.userName_userTab,
					"User name"));
			flags.add(assertElementPresent(SiteAdminPage.firstName_userTab,
					"First name"));
			flags.add(assertElementPresent(SiteAdminPage.lastName_userTab,
					"Last name"));
			flags.add(assertElementPresent(
					SiteAdminPage.emailAddress_userTab,
					"Email Address name"));
			flags.add(assertElementPresent(SiteAdminPage.emailStatus,
					"Email Status"));

			flags.add(isElementPopulated_byValue(SiteAdminPage.userName_userTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.firstName_userTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.lastName_userTab));
			flags.add(isElementPopulated_byValue(SiteAdminPage.emailAddress_userTab));

			flags.add(click(SiteAdminPage.updateBtn_userTab,
					"Update button"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.successMsg_userTab,
					"successMsg_userTab", 60));
			flags.add(assertElementPresent(
					SiteAdminPage.successMsg_userTab, "Success Message"));
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to User tab is successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to User tab failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigateAssignRolesToUsersTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(isElementPresent(SiteAdminPage.assignRolesToUsersTab,
					"Assign Roles To Users Tab"));
			flags.add(click(SiteAdminPage.assignRolesToUsersTab,
					"Assign Roles To Users Tab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.selectUser_roleTab,
					"selectUser_roleTab", 60));
			flags.add(assertElementPresent(
					SiteAdminPage.selectUser_roleTab, "User"));
			flags.add(assertElementPresent(SiteAdminPage.availableRole,
					"Available Role"));
			flags.add(assertElementPresent(SiteAdminPage.selectedRole,
					"Selected Role"));
			flags.add(assertElementPresent(SiteAdminPage.appRoleName,
					"Application Role Name"));
			flags.add(assertElementPresent(SiteAdminPage.appRoleDesc,
					"Application Role Description"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Assign Roles To Users Tab is successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Assign Roles To Users Tab failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigateAssignUsersToRolesTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(isElementPresent(SiteAdminPage.assignUsersToRoleTab,
					"Assign Users To Role Tab"));
			flags.add(click(SiteAdminPage.assignUsersToRoleTab,
					"Assign Users To Role Tab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.appRoleDropDown,
					"appRoleDropDown", 60));
			flags.add(assertElementPresent(SiteAdminPage.appRoleDropDown,
					"Application Role Drop Down"));
			flags.add(assertElementPresent(SiteAdminPage.roleDesc,
					"Role Description"));
			flags.add(assertElementPresent(SiteAdminPage.availableUsers,
					"Available Users"));
			flags.add(assertElementPresent(SiteAdminPage.selectedUsers,
					"Selected Users"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Assign Users To Roles Tab is successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Assign Users To Roles Tab failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigateProfileOptionsTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(isElementPresent(SiteAdminPage.profileOptionsTab,
					"Profile Options Tab"));
			flags.add(click(SiteAdminPage.profileOptionsTab,
					"Profile Options Tab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.profileField,
					"profileField", 60));
			flags.add(assertElementPresent(SiteAdminPage.profileField,
					"Profile Field"));
			flags.add(assertElementPresent(SiteAdminPage.dropDownOptions,
					"Drop Down Options"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Profile Options Tab is successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Profile Options Tab failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigateUserMigrationTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(isElementPresent(SiteAdminPage.userMigrationTab,
					"User Migration Tab"));
			flags.add(click(SiteAdminPage.userMigrationTab,
					"User Migration Tab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.migrateUsersBtn,
					"migrateUsersBtn", 60));
			flags.add(assertElementPresent(SiteAdminPage.migrateUsersBtn,
					"Migrate Users Button"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to User Migration Tab is successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to User Migration Tab failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean navigatePromptedCheckinExclusionsTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(isElementPresent(
					SiteAdminPage.promptedCheckInExclusionsTab,
					"Prompted Check-in Exclusions Tab"));
			flags.add(click(SiteAdminPage.promptedCheckInExclusionsTab,
					"Prompted Check-in Exclusions Tab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.headerMsg,
					"headerMsg", 60));
			flags.add(isElementPresent(SiteAdminPage.headerMsg,
					"Header Message"));
			flags.add(assertElementPresent(SiteAdminPage.headerMsg,
					"Header Message"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Prompted CheckIn Exclusions Tab is successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Prompted CheckIn Exclusions Tab failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyProfileGroupTab(String profileGroupLabelName)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(isElementPresent(SiteAdminPage.manualTripEntryTab,
					"Manual Trip Entry"));
			flags.add(click(SiteAdminPage.manualTripEntryTab,
					"Manual Trip Entry"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.profileGrouptab,
					"profileGrouptab"));
			flags.add(waitForElementPresent(
					SiteAdminPage.profileGrouptab,
					"profileGrouptab", 60));
			flags.add(assertElementPresent(SiteAdminPage.profileGrouptab,
					"profileGrouptab"));
			flags.add(assertElementPresent(SiteAdminPage.profileFieldtab,
					"profileFieldtab"));
			flags.add(assertElementPresent(SiteAdminPage.tripSegmentsTab,
					"tripSegments"));
			flags.add(assertElementPresent(
					SiteAdminPage.unmappedProfileFieldsTab,
					"unmappedProfileFields"));
			flags.add(assertElementPresent(
					SiteAdminPage.metadataTripQuestionTab,
					"metadataTripQuestion"));
			flags.add(assertElementPresent(
					SiteAdminPage.customTripQuestionTab,
					"customTripQuestion"));
			flags.add(click(SiteAdminPage.profileGrouptab,
					"profileGrouptab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.selectProfileGroup,
					"selectProfileGroup", 60));
			flags.add(click(SiteAdminPage.selectProfileGroup,
					"selectProfileGroup"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(isElementPopulated_byValue(SiteAdminPage.profileGroupLabel));
			flags.add(type(SiteAdminPage.profileGroupLabel,
					profileGroupLabelName, "profileGroupLabel"));
			flags.add(click(SiteAdminPage.updateLabelButton,
					"updateLabelButton"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(assertElementPresent(
					SiteAdminPage.groupNameSuccessMsg,
					"groupNameSuccessMsg"));
			flags.add(click(SiteAdminPage.profileGroupSaveBtn,
					"profileGroupSaveBtn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.profileGroupSuccessMsg,
					"profileGroupSuccessMsg", 60));
			flags.add(assertElementPresent(
					SiteAdminPage.profileGroupSuccessMsg,
					"profileGroupSuccessMsg"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Profile Group Tab is successful");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Profile Group Tab failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyProfileFieldsTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(isElementPresent(SiteAdminPage.tripSegmentsTab,
					"tripSegmentsTab"));
			flags.add(click(SiteAdminPage.profileFieldtab,
					"profileFieldtab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.availableProfileFields,
					"availableProfileFields", 60));
			flags.add(assertElementPresent(
					SiteAdminPage.availableProfileFields,
					"availableProfileFields"));
			flags.add(assertElementPresent(
					SiteAdminPage.selectedProfileFields,
					"selectedProfileFields"));
			flags.add(assertElementPresent(
					SiteAdminPage.labelForProfileField,
					"labelForProfileField"));
			flags.add(selectByIndex(SiteAdminPage.attributeGroup, 1,
					"attributeGroup"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.availableProfileFields,
					"availableProfileFields", 60));
			flags.add(isElementPopulated_byText(SiteAdminPage.availableProfileFields));
			flags.add(isElementPopulated_byText(SiteAdminPage.selectedProfileFields));
			flags.add(click(SiteAdminPage.profileFieldHomeSite,
					"profileFieldHomeSite"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			/*flags.add(waitForElementPresent(
					SiteAdminPage.labelForProfileField,
					"labelForProfileField", 60));*/
			flags.add(isElementPopulated_byValue(SiteAdminPage.labelForProfileField));
			flags.add(click(SiteAdminPage.businessLocation,
					"businessLocation"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.isRequiredOnFormCheckbox,
					"isRequiredOnFormCheckbox", 60));
			flags.add(click(SiteAdminPage.isRequiredOnFormCheckbox,
					"isRequiredOnFormCheckbox"));
			flags.add(click(SiteAdminPage.profileFieldUpdateLabel,
					"profileFieldUpdateLabel"));
			flags.add(assertElementPresent(
					SiteAdminPage.profileFieldUpdateSuccessMsg,
					"profileFieldUpdateSuccessMsg"));
			flags.add(click(SiteAdminPage.profileFieldSaveBtn,
					"profileFieldSaveBtn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.profileFieldSuccessMsg,
					"profileFieldSuccessMsg", 60));
			flags.add(assertElementPresent(
					SiteAdminPage.profileFieldSuccessMsg,
					"profileFieldSuccessMsg"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Navigation to Profile Fields Tab is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Navigation to Profile Fields Tab failed");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifytripSegmentsTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(SiteAdminPage.tripSegmentsTab,
					"tripSegmentsTab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.availableSegments,
					"availableSegments", 60));
			flags.add(assertElementPresent(SiteAdminPage.availableSegments,
					"availableSegments"));
			flags.add(assertElementPresent(SiteAdminPage.selectedSegments,
					"selectedSegments"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Trip Segments Tab is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Trip Segments Tab Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyUnmappedProfileFieldsTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(SiteAdminPage.unmappedProfileFieldsTab,
					"unmappedProfileFieldsTab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.availableFields,
					"availableFields", 60));
			flags.add(assertElementPresent(SiteAdminPage.availableFields,
					"availableFields"));
			flags.add(assertElementPresent(SiteAdminPage.selectedFields,
					"selectedFields"));
			flags.add(click(SiteAdminPage.accountNo, "accountNo"));
			flags.add(click(SiteAdminPage.addBtn, "addBtn"));
			flags.add(click(SiteAdminPage.umappedProFldSaveBtn,
					"umappedProFldSaveBtn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.umappedProFldSuccessMsg,
					"umappedProFldSuccessMsg", 60));
			flags.add(assertElementPresent(
					SiteAdminPage.umappedProFldSuccessMsg,
					"umappedProFldSuccessMsg"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Unmapped Profile Fields Tab is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Unmapped Profile Fields Tab Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyMetadataTripQstnTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(SiteAdminPage.metadataTripQuestionTab,
					"metadataTripQuestionTab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.availableMetadataTripQstns,
					"availableMetadataTripQstns", 60));
			flags.add(assertElementPresent(
					SiteAdminPage.availableMetadataTripQstns,
					"availableMetadataTripQstns"));
			flags.add(assertElementPresent(
					SiteAdminPage.selectedMetadataTripQstns,
					"selectedMetadataTripQstns"));
			flags.add(assertElementPresent(SiteAdminPage.labelName,
					"labelName"));
			flags.add(assertElementPresent(SiteAdminPage.responseType,
					"responseType"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of MetaData Trip Questions Tab is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of MetaData Trip Questions Tab Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyCustomTripQstnTab(String defaultQstnText)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(SiteAdminPage.customTripQuestionTab,
					"customTripQuestionTab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.newBtn,
					"newBtn", 60));
			flags.add(click(SiteAdminPage.newBtn, "newBtn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.defaultQstn,
					"defaultQstn", 60));
			flags.add(click(SiteAdminPage.defaultQstn, "defaultQstn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.qstnText,
					"qstnText", 60));
			flags.add(type(SiteAdminPage.qstnText, defaultQstnText,
					"qstnText"));
			flags.add(selectByIndex(SiteAdminPage.responseTypeCTQ, 1,
					"responseTypeCTQ"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.applySettings,
					"applySettings", 60));
			flags.add(click(SiteAdminPage.applySettings, "applySettings"));
			flags.add(click(SiteAdminPage.updateCustomTripQstn,
					"updateCustomTripQstn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.saveChangesBtn,
					"saveChangesBtn", 60));
			flags.add(click(SiteAdminPage.saveChangesBtn, "saveChangesBtn"));
			Driver.switchTo().alert().accept();
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.CTQSuccessMsg,
					"CTQSuccessMsg", 60));
			flags.add(assertElementPresent(SiteAdminPage.CTQSuccessMsg,
					"CTQSuccessMsg"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Custom Trip Questions Tab is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Custom Trip Questions Tab Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyProfileGroupTab_MyTrips(String profileGroupLabelName)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			new TravelTrackerHomePage().TravelTracker_Page();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(isElementPresent(SiteAdminPage.siteAdminLink,
					"Site Admin Link"));
			flags.add(click(SiteAdminPage.siteAdminLink, "Site Admin Link"));
			flags.add(waitForVisibilityOfElement(SiteAdminPage.generaltab,
					"General Tab"));
			flags.add(assertElementPresent(SiteAdminPage.generaltab,
					"General Tab"));
			flags.add(click(SiteAdminPage.selectcustomerDropDown,
					"Customer drop down"));
			flags.add(click(SiteAdminPage.customerName, "Customer Name"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					SiteAdminPage.myTripsTab,
					"myTripsTab", 60));
			flags.add(isElementPresent(SiteAdminPage.myTripsTab,
					"myTripsTab"));
			flags.add(click(SiteAdminPage.myTripsTab, "myTripsTab"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.profileGroupTab,
					"profileGrouptab"));
			flags.add(assertElementPresent(MyTripsPage.profileGroupTab,
					"profileGrouptab"));
			flags.add(assertElementPresent(MyTripsPage.profileFieldsTab,
					"profileFieldtab"));
			flags.add(assertElementPresent(MyTripsPage.tripSegmentsTab,
					"tripSegments"));
			flags.add(assertElementPresent(
					MyTripsPage.unmappedProfileFieldsTab,
					"unmappedProfileFields"));
			flags.add(assertElementPresent(MyTripsPage.metadataTripQuestionTab,
					"metadataTripQuestion"));
			flags.add(assertElementPresent(MyTripsPage.customTripQuestionTab,
					"customTripQuestion"));
			flags.add(assertElementPresent(
					MyTripsPage.authorizedMyTripsCustTab, "customTripQuestion"));
			flags.add(click(MyTripsPage.profileGroupTab, "profileGrouptab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.selectProfileGroup,
					"selectProfileGroup", 60));
			flags.add(click(MyTripsPage.selectProfileGroup,
					"selectProfileGroup"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.profileGroupLabel,
					"profileGroupLabel", 60));
			flags.add(isElementPopulated_byValue(MyTripsPage.profileGroupLabel));
			flags.add(type(MyTripsPage.profileGroupLabel,
					profileGroupLabelName, "profileGroupLabel"));
			flags.add(click(MyTripsPage.updateLabelButton, "updateLabelButton"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.groupNameSuccessMsg,
					"groupNameSuccessMsg", 60));
			flags.add(assertElementPresent(MyTripsPage.groupNameSuccessMsg,
					"groupNameSuccessMsg"));
			flags.add(click(MyTripsPage.profileGroupSaveBtn,
					"profileGroupSaveBtn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.profileGroupSuccessMsg,
					"profileGroupSuccessMsg", 60));
			flags.add(assertElementPresent(MyTripsPage.profileGroupSuccessMsg,
					"profileGroupSuccessMsg"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Profile Group Tab is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Profile Group Tab Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyProfileFieldsTab_MyTrips() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(MyTripsPage.profileFieldsTab, "profileFieldtab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.availableProfileFields,
					"availableProfileFields", 60));
			flags.add(assertElementPresent(MyTripsPage.availableProfileFields,
					"availableProfileFields"));
			flags.add(assertElementPresent(MyTripsPage.selectedProfileFields,
					"selectedProfileFields"));
			flags.add(assertElementPresent(MyTripsPage.labelForProfileField,
					"labelForProfileField"));
			flags.add(selectByIndex(MyTripsPage.attributeGroup, 1,
					"attributeGroup"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.availableProfileFields,
					"availableProfileFields", 60));
			flags.add(isElementPopulated_byText(MyTripsPage.availableProfileFields));
			flags.add(isElementPopulated_byText(MyTripsPage.selectedProfileFields));
			flags.add(click(MyTripsPage.gender, "profileFieldHomeSite"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.labelForProfileField,
					"labelForProfileField", 60));
			flags.add(isElementPopulated_byValue(MyTripsPage.labelForProfileField));
			flags.add(click(MyTripsPage.profileFieldMiddleName,
					"businessLocation"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.isRequiredOnFormCheckbox,
					"isRequiredOnFormCheckbox", 60));
			flags.add(click(MyTripsPage.isRequiredOnFormCheckbox,
					"isRequiredOnFormCheckbox"));
			flags.add(click(MyTripsPage.profileFieldUpdateLabel,
					"profileFieldUpdateLabel"));
			flags.add(assertElementPresent(
					MyTripsPage.profileFieldUpdateSuccessMsg,
					"profileFieldUpdateSuccessMsg"));
			flags.add(click(MyTripsPage.profileFieldSaveBtn,
					"profileFieldSaveBtn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.profileFieldSuccessMsg,
					"profileFieldSuccessMsg", 60));
			flags.add(assertElementPresent(MyTripsPage.profileFieldSuccessMsg,
					"profileFieldSuccessMsg"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Profile Fields Tab is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Profile Fields Tab Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifytripSegmentsTab_MyTrips() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(MyTripsPage.tripSegmentsTab, "tripSegmentsTab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.availableSegments,
					"profileFieldSuccessMsg", 60));
			flags.add(assertElementPresent(MyTripsPage.availableSegments,
					"availableSegments"));
			flags.add(assertElementPresent(MyTripsPage.selectedSegments,
					"selectedSegments"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Trip Segments Tab is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Trip Segments Tab Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyUnmappedProfileFieldsTab_MyTrips() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(MyTripsPage.unmappedProfileFieldsTab,
					"unmappedProfileFieldsTab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.availableFields,
					"availableFields", 60));
			flags.add(assertElementPresent(MyTripsPage.availableFields,
					"availableFields"));
			flags.add(assertElementPresent(MyTripsPage.selectedFields,
					"selectedFields"));
			flags.add(click(MyTripsPage.accountNo, "accountNo"));
			flags.add(click(MyTripsPage.addBtn, "addBtn"));
			flags.add(click(MyTripsPage.umappedProFldSaveBtn,
					"umappedProFldSaveBtn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.umappedProFldSuccessMsg,
					"umappedProFldSuccessMsg", 60));
			flags.add(assertElementPresent(MyTripsPage.umappedProFldSuccessMsg,
					"umappedProFldSuccessMsg"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Unmapped Profile Fields Tab is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Unmapped Profile Fields Tab Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyMetadataTripQstnTab_MyTrips() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(MyTripsPage.metadataTripQuestionTab,
					"metadataTripQuestionTab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.availableMetadataTripQstns,
					"availableMetadataTripQstns", 60));
			flags.add(assertElementPresent(
					MyTripsPage.availableMetadataTripQstns,
					"availableMetadataTripQstns"));
			flags.add(assertElementPresent(
					MyTripsPage.selectedMetadataTripQstns,
					"selectedMetadataTripQstns"));
			flags.add(assertElementPresent(MyTripsPage.labelName, "labelName"));
			flags.add(assertElementPresent(MyTripsPage.responseType,
					"responseType"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Metadata Trip Questions Tab is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Metadata Trip Questions Tab Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyCustomTripQstnTab_MyTrips(String defaultQstnText)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(MyTripsPage.customTripQuestionTab,
					"customTripQuestionTab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.newBtn,
					"newBtn", 60));
			flags.add(click(MyTripsPage.newBtn, "newBtn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.defaultQstn,
					"defaultQstn", 60));
			flags.add(click(MyTripsPage.defaultQstn, "defaultQstn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.qstnText,
					"qstnText", 60));
			flags.add(type(MyTripsPage.qstnText, defaultQstnText, "qstnText"));
			flags.add(selectByIndex(MyTripsPage.responseTypeCTQ, 1,
					"responseTypeCTQ"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.applySettings,
					"applySettings", 60));
			flags.add(click(MyTripsPage.applySettings, "applySettings"));
			flags.add(click(MyTripsPage.updateCustomTripQstn,
					"updateCustomTripQstn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.saveChangesBtn,
					"saveChangesBtn", 60));
			flags.add(click(MyTripsPage.saveChangesBtn, "saveChangesBtn"));
			Driver.switchTo().alert().accept();
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.CTQSuccessMsg,
					"CTQSuccessMsg", 60));
			flags.add(assertElementPresent(MyTripsPage.CTQSuccessMsg,
					"CTQSuccessMsg"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Custom Trip Questions Tab is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Custom Trip Questions Tab Failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifAuthorizedMyTripsCustomers() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			new TravelTrackerHomePage().TravelTracker_Page();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(MyTripsPage.authorizedMyTripsCustTab,
					"customTripQuestionTab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.intlSOS,
					"intlSOS", 60));
			flags.add(click(TravelTrackerHomePage.intlSOS, "intlSOS"));
			flags.add(assertElementPresent(MyTripsPage.availableCustomers,
					"Encrypted MyTrips URL is displayed for the selected customer"));
			flags.add(assertElementPresent(MyTripsPage.selectedCustomers,
					"Encrypted MyTrips URL is displayed for the selected customer"));
			flags.add(assertElementPresent(MyTripsPage.encryptedCustomerId,
					"Encrypted MyTrips URL is displayed for the selected customer"));
			flags.add(assertElementPresent(TravelTrackerHomePage.intlSOSURL,
					"Encrypted MyTrips URL is displayed for the selected customer"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verification of Authorized My Trips Customers is successful");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Verification of Authorized My Trips Customers Failed.");
		}
		return flag;
	}

	/*public static void setComponentStartTimer() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		component_startTime = sdf.format(date);
	}

	public static void setComponentEndTimer() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		component_endTime = sdf.format(date);
	}*/

	public boolean typeNewsReportName(String chars) throws Throwable {
		boolean flag = true;
		try {
			// List<Boolean> flags = new ArrayList<>();
			// setComponentStartTimer();
			if (browser.equalsIgnoreCase("ie")) {
				Robot rb = new Robot();
				char[] ch = chars.toCharArray();
				int i = ch.length;
				int j = 0;
				while (j < i) {
					int kcode = (int) ch[j] - 32;
					rb.keyPress(kcode);
					rb.keyRelease(kcode);
					j++;
				}
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
				// setComponentEndTimer();
				// // long duration = (endTime - startTime);
				// LOG.info("createReport took " + duration +
				// " milliseconds");
			} else {
				Alert alert = Driver.switchTo().alert();
				alert.sendKeys(chars);
				alert.accept();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();

		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyassignUserToGroupTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(SiteAdminPage.assignUserToGrpTab,
					"assignUserToGrpTab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(assertElementPresent(SiteAdminPage.group, "group"));
			flags.add(selectByIndex(SiteAdminPage.group, 3, "group"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(click(SiteAdminPage.availableUser, "availableUser"));
			flags.add(click(SiteAdminPage.moveUser, "moveUser"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(click(SiteAdminPage.saveAssignUserToGrpBtn,
					"saveAssignUserToGrpBtn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(assertElementPresent(
					SiteAdminPage.assignUserToGrpSuccessMsg,
					"assignUserToGrpSuccessMsg"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Navigated to Assign User To Group Tab successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigation to Assign User To Group Tab failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyassignGroupToUserTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(SiteAdminPage.assignGrpUserTab,
					"assignGrpUserTab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(assertElementPresent(SiteAdminPage.user, "user"));
			flags.add(selectByIndex(SiteAdminPage.user, 3, "User"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(click(SiteAdminPage.availableGrp, "availableGrp"));
			flags.add(click(SiteAdminPage.moveGrp, "moveGrp"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(click(SiteAdminPage.saveAssignGrpToUserBtn,
					"saveAssignGrpToUserBtn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(assertElementPresent(
					SiteAdminPage.assignGrpToUserSuccessMsg,
					"assignGrpToUserSuccessMsg"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Navigated to Assign Group To User Tab successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigation to Assign Group To User Tab failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyAddEditGroupTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(SiteAdminPage.addEditGrpTab, "addEditGrpTab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(assertElementPresent(SiteAdminPage.selectGrpDropdown,
					"selectGrpDropdown"));
			flags.add(selectByIndex(SiteAdminPage.selectGrpDropdown, 3,
					"selectGrpDropdown"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(click(SiteAdminPage.saveaddEditGrpBtn,
					"saveaddEditGrpBtn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(assertElementPresent(
					SiteAdminPage.addEditGrpSuccessMsg,
					"addEditGrpSuccessMsg"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Navigated to Add/Edit Group Tab successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigation to Add/Edit Group Tab failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyAddEditFilterTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(SiteAdminPage.addEditFilterTab,
					"addEditFilterTab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(assertElementPresent(SiteAdminPage.filterDropdown,
					"filterDropdown"));
			flags.add(selectByIndex(SiteAdminPage.filterDropdown, 5,
					"filterDropdown"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(click(SiteAdminPage.saveaddEditFilterBtn,
					"saveaddEditFilterBtn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(assertElementPresent(
					SiteAdminPage.addEditFilterSuccessMsg,
					"addEditFilterSuccessMsg"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Navigated to Add/Edit Filter Tab successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigation to Add/Edit Filter Tab failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyAssignFiltersToGroupTab() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(SiteAdminPage.assignFiltersToGrpTab,
					"assignFiltersToGrpTab"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(assertElementPresent(SiteAdminPage.grpDropdown,
					"grpDropdown"));
			flags.add(selectByIndex(SiteAdminPage.grpDropdown, 6,
					"grpDropdown"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(click(SiteAdminPage.availableFilter,
					"availableFilter"));
			flags.add(click(SiteAdminPage.moveFilter, "moveFilter"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(click(SiteAdminPage.saveAssignToFiltersBtn,
					"saveAssignToFiltersBtn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(assertElementPresent(
					SiteAdminPage.assignFiltersToGrpSuccessMsg,
					"assignFiltersToGrpSuccessMsg"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Navigated to Assign Filters To Group Tab successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Navigation to Assign Filters To Group Tab failed.");
		}
		return flag;
	}

	/*public static void setMethodName(String MethodName) throws IOException {
		methodName = MethodName;
		componentNamesList.add(MethodName);

	}

	public static String getMethodname() throws IOException {
		return methodName;

	}*/

}
