package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;

import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.RiskRatingsPage;
import com.isos.tt.page.TravelTrackerHomePage;

public class RiskRatingsLib extends CommonLib {

	private static final Logger LOG = Logger.getLogger(RiskRatingsLib.class);

	TestEngineWeb tWeb = new TestEngineWeb();

	/*
	 * static String component_startTime; static String component_endTime;
	 * static String methodName;
	 * 
	 * public static List componentNamesList = new ArrayList(); public static
	 * List componentStartTimer = new ArrayList(); public static List
	 * componentEndTimer = new ArrayList();
	 */

	@SuppressWarnings("unchecked")
	public boolean verifyRiskRatingsPage() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new RiskRatingsPage().RiskRatings_Page();
			new TravelTrackerHomePage().TravelTracker_Page();
			/*
			 * Actions action = new Actions(Driver);
			 * action.moveToElement(Driver.
			 * findElement(By.xpath(".//a[@id='ctl00_lnkTools']"
			 * ))).click().perform();
			 */
			/*
			 * JavascriptExecutor jsx= (JavascriptExecutor)Driver;
			 * jsx.executeScript("scroll(0, -250);"); Thread.sleep(5000);
			 */
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.tools,
					"Tools link"));
			flags.add(click(TravelTrackerHomePage.tools, "Tools link"));
			flags.add(click(RiskRatingsPage.riskRatingsLink,
					"Risk Ratings Link"));
			flags.add(assertElementPresent(RiskRatingsPage.ttRiskRating,
					"Medical Risks"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified Risk Ratings page successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Risk Ratings page verification failed."
					+ getListOfScreenShots(TestScriptDriver
							.getScreenShotDirectory_testCasePath(),
							getMethodname()));

		}
		return flag;
	} // end method

	@SuppressWarnings("unchecked")
	public boolean verifyIntlSOSPortalPage() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(click(TravelTrackerHomePage.tools, "Tools link"));
			flags.add(click(TravelTrackerHomePage.intlSOSPortal,
					"International SOS Portal Link"));
			String parentWindow = Driver.getWindowHandle();
			Set<String> childWindows = Driver.getWindowHandles();
			LOG.info("parentWindow**" + parentWindow);
			Thread.sleep(2000);
			for (String windowHandle : childWindows) {
				if (!windowHandle.equalsIgnoreCase(parentWindow)) {
					Driver.switchTo().window(windowHandle);
					LOG.info("I`m in child window");
					flags.add(waitForVisibilityOfElement(
							TravelTrackerHomePage.welcomePage, "Welcome Page"));
					flags.add(assertElementPresent(
							TravelTrackerHomePage.welcomePage, "Welcome Page"));
					Driver.close();
					break;
				}
			}
			Driver.switchTo().window(parentWindow);
			flags.add(isElementPresent(TravelTrackerHomePage.feedbackLink,
					"Feedback Link"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified IntlSOS Portal page successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("IntlSOS Portal page verification failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyOpenCommunicationHistoryPage() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(click(TravelTrackerHomePage.tools, "Tools link"));
			flags.add(click(TravelTrackerHomePage.communicationHistory,
					"Communication History Link"));
			flags.add(assertElementPresent(TravelTrackerHomePage.printButton,
					"Print Button"));
			flags.add(isElementEnabled(TravelTrackerHomePage.printButton));
			flags.add(isElementEnabled(TravelTrackerHomePage.exportToExcelBtn));
			flags.add(isElementEnabled(TravelTrackerHomePage.exportToZipBtn));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified Open Communication History page successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Open Communication History page verification failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyMessageManagerPage() throws Throwable {
		boolean flag = true;

		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(click(TravelTrackerHomePage.tools, "Tools link"));
			flags.add(click(TravelTrackerHomePage.messageManagerLink,
					"Message Manager Link"));
			flags.add(assertElementPresent(TravelTrackerHomePage.printButton,
					"Print Button"));
			flags.add(isElementEnabled(TravelTrackerHomePage.printButton));
			flags.add(isElementEnabled(TravelTrackerHomePage.exportToExcelBtn));
			flags.add(isElementEnabled(TravelTrackerHomePage.exportToZipBtn));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified Message Manager page successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Message Manager page verification failed.");
		}

		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyHelpPage() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(click(TravelTrackerHomePage.helpLink, "Help Link"));
			String parentWindow = Driver.getWindowHandle();
			Set<String> childWindows = Driver.getWindowHandles();
			LOG.info("parentWindow**" + parentWindow);
			Thread.sleep(2000);
			for (String windowHandle : childWindows) {
				if (!windowHandle.equalsIgnoreCase(parentWindow)) {
					Driver.switchTo().window(windowHandle);
					LOG.info("I`m in child window");
					LOG.info("Title is : "
							+ Driver.switchTo().window(windowHandle)
									.getCurrentUrl());
					if (Driver.switchTo().window(windowHandle).getCurrentUrl()
							.contains("pdf"))
						LOG.info("PDF is successfully validated");
					else
						LOG.info("PDF is NOT validated");
					Driver.close();
					break;
				}
			}
			Driver.switchTo().window(parentWindow);
			flags.add(isElementPresent(TravelTrackerHomePage.feedbackLink,
					"Feedback Link"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Help page successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Help page verification failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyFeedbackPage() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(click(TravelTrackerHomePage.feedbackLink, "Feedback Link"));
			Thread.sleep(10000);
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			LOG.info("Enter pressed");
			robot.keyRelease(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			LOG.info("Enter Released");
			Thread.sleep(5000);
			Driver.switchTo().alert().accept();
			flags.add(isElementPresent(TravelTrackerHomePage.feedbackLink,
					"Feedback Link"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified Feedback page successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Feedback page verification failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyUserSettingsPage() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(click(TravelTrackerHomePage.tools, "Tools link"));
			flags.add(click(TravelTrackerHomePage.userSettingsLink,
					"User Settings Link"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.changePwdChallengeQstns,
					"Change Password/Update Challenge Questions"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.updateEmailAddress,
					"Update Email Address"));
			flags.add(isElementEnabled(TravelTrackerHomePage.updateButton));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified User Settings page successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("User Settings page verification failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyTravelMedicalRisks(String countryName)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new RiskRatingsPage().RiskRatings_Page();
			flags.add(selectByIndex(RiskRatingsPage.riskRatingsCustomer, 92,
					"locationDropdown"));
			flags.add(type(RiskRatingsPage.searchCountry, countryName,
					"searchCountry"));
			flags.add(click(RiskRatingsPage.medicalCheckBox, "medicalCheckBox"));
			flags.add(click(RiskRatingsPage.travelCheckbox, "travelCheckbox"));
			flags.add(Driver
					.findElement(
							By.xpath(".//td[@id='ctl00_MainContent_CustomRiskRatingUserControl1_columnMedical']//td[text()='"
									+ countryName + "']")).isDisplayed());
			flags.add(Driver
					.findElement(
							By.xpath(".//td[@id='ctl00_MainContent_CustomRiskRatingUserControl1_columnTravel']//td[text()='"
									+ countryName + "']")).isDisplayed());

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified Travel Medical Risks successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification of Travel Medical Risks is failed.");
		}
		return flag;
	} // end method

	@SuppressWarnings("unchecked")
	public boolean updateTravelMedicalRisks() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new RiskRatingsPage().RiskRatings_Page();
			flags.add(click(RiskRatingsPage.mediumMedical, "mediumMedical"));
			flags.add(click(RiskRatingsPage.mediumTravel, "mediumTravel"));
			flags.add(click(RiskRatingsPage.applyRiskRatingsBtn,
					"applyRiskRatingsBtn"));
			flags.add(waitForInVisibilityOfElement(
					RiskRatingsPage.crrprogressImage, "crrprogressImage"));
			flags.add(waitForElementPresent(
					RiskRatingsPage.riskRatingSuccessMsg,
					"riskRatingSuccessMsg", 60));
			flags.add(assertElementPresent(
					RiskRatingsPage.riskRatingSuccessMsg,
					"riskRatingSuccessMsg"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified Risk Ratings page successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Risk Ratings page verification failed.");
		}
		return flag;
	} // end method

	@SuppressWarnings("unchecked")
	public boolean verifyLastUpdatedMsg() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new RiskRatingsPage().RiskRatings_Page();
			flags.add(assertElementPresent(RiskRatingsPage.lastUpdatedTime,
					"lastUpdatedTime"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified Last Updated message successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification of Last Updated message is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyTravelAndMedicalRatings(String countryName)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1]
					.getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new RiskRatingsPage().RiskRatings_Page();
			Thread.sleep(5000);
			flags.add(isElementPresent(RiskRatingsPage.searchCountry,
					"searchCountry"));
			flags.add(type(RiskRatingsPage.searchCountry, countryName,
					"searchCountry"));
			flags.add(assertElementPresent(RiskRatingsPage.verifyMedical,
					"verifyMedical"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Verified Last Updated message successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("Verification of Last Updated message is failed.");
		}
		return flag;
	}

	/*
	 * public static void setComponentStartTimer() { SimpleDateFormat sdf = new
	 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); Date date = new Date();
	 * component_startTime = sdf.format(date); }
	 * 
	 * public static void setComponentEndTimer() { SimpleDateFormat sdf = new
	 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); Date date = new Date();
	 * component_endTime = sdf.format(date); }
	 */
	/**
	 * jhdf.
	 */
	public boolean typeNewsReportName(String chars) throws Throwable {
		boolean flag = true;
		try {
			// List<Boolean> flags = new ArrayList<>();
			// setComponentStartTimer();
			if (browser.equalsIgnoreCase("ie")) {
				Robot rb = new Robot();
				char[] ch = chars.toCharArray();
				int i = ch.length;
				int j = 0;
				while (j < i) {
					int kcode = (int) ch[j] - 32;
					rb.keyPress(kcode);
					rb.keyRelease(kcode);
					j++;
				}
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
				// setComponentEndTimer();
				// // long duration = (endTime - startTime);
				// LOG.info("createReport took " + duration +
				// " milliseconds");
			} else {
				Alert alert = Driver.switchTo().alert();
				alert.sendKeys(chars);
				alert.accept();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();

		}
		return flag;
	}

	/*
	 * public static void setMethodName(String MethodName) throws IOException {
	 * methodName = MethodName; componentNamesList.add(MethodName);
	 * 
	 * }
	 * 
	 * 
	 * public static String getMethodname() throws IOException { return
	 * methodName;
	 * 
	 * }
	 */

}
