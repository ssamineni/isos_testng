package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.automation.CSVUility.CsvHandler;
import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.automation.report.ReporterConstants;
import com.isos.ecms.page.ContentEditorPage;
import com.isos.tt.page.MyTripsPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.sample.TestCaseCreationUtility.GetLatestCodeFromBitBucket;

public class CommonLib extends ActionEngine{

	private static final Logger LOG = Logger.getLogger(CommonLib.class);

	/*	static String component_startTime;
	static String component_endTime;
	static String testCase_startTime;
	static String testCase_endTime;*/
	static String methodName;
	//	public static List componentNamesList = new ArrayList();
	public static List componentStartTimer = new ArrayList();
	public static List componentEndTimer = new ArrayList();
	public static List componentActualresult = new ArrayList();

	public static String getCurrentTime() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		return sdf.format(date);
	}

	/*	public static void setComponentEndTimer() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		component_endTime = sdf.format(date);
	}

	public static void setComponentStartTimer() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		component_startTime = sdf.format(date);
	}

	public static void setComponentEndTimer() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		component_endTime = sdf.format(date);
	}
	 */

	
	@SuppressWarnings("unused")
	 public  String getListOfScreenShots(String screenShotDirectory_testCasePath, final String methodname) {
	  String finalString = "";
	  try {
	   File f = new File(screenShotDirectory_testCasePath);
	   List<String> list = Arrays.asList(f.list(new FilenameFilter() {
	    public boolean accept(File dir, String name) {
	     return name.startsWith(methodname);    
	    
	    }
	   }
	    
	   ));
	   String ScreenShotPath = screenShotDirectory_testCasePath;
	   ScreenShotPath = ScreenShotPath.replace(".\\", "\\");
	   
	   if (!list.isEmpty()) {
	    String[] files = (String[]) list.toArray();
	    for (int i = 0; i < files.length; i++) {
	     String filess = files[i] ;
	     filess = filess.replaceAll(" ", "%20");

	     finalString = finalString +"[ Failed screeenshot: "+ methodname + "] [i]"+"\n"+"[i]: "+ ReporterConstants.Jenkins_path +ScreenShotPath +"\\"+ filess+"\n";  }
	   }
	  } catch (Exception e) {
	   e.printStackTrace();
	  }

	  return finalString;
	 }
	
	@SuppressWarnings("unchecked")
	public boolean doPandemicLogout() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			flags.add(click(ContentEditorPage.PandemicMember, "Member icon"));
			flags.add(click(ContentEditorPage.PandemicLogout, "Logout  icon"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}


	public void checkPageIsReadyUsingJavaScript() {
		JavascriptExecutor js = (JavascriptExecutor) Driver;
		// Initially bellow given if condition will check ready state of page.
		if (js.executeScript("return document.readyState").toString()
				.equals("complete")) {
			LOG.info("Page Is loaded.");
			return;
		}

		// This loop will rotate for 25 times to check If page Is ready after
		// every 1 second.
		// You can replace your value with 25 If you wants to Increase or
		// decrease wait time.
		for (int i = 0; i < 25; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			// To check page ready state.
			if (js.executeScript("return document.readyState").toString()
					.equals("complete")) {
				break;
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static void setMethodName(String MethodName) throws IOException {
		methodName = MethodName;
		//	componentNamesList.add(MethodName);
	}

	public static String getMethodname() throws IOException {
		return methodName;

	}

	@SuppressWarnings("unchecked")
	public boolean openBrowser(String url) throws IOException,
	InterruptedException {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			driverInitiation(url);
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Browser opened successfully.");
		} catch (Exception e) {
			flag = false;
			// TODO Auto-generated catch block
			e.printStackTrace();
			componentActualresult.add("Failed to open the browser.");
			componentEndTimer.add(getCurrentTime());
		}
		return flag;
	
	}
	public boolean openBrowser() throws IOException,
	InterruptedException {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			driverInitiation(ReporterConstants.envUrl);
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Browser opened successfully.");
		} catch (Exception e) {
			flag = false;
			// TODO Auto-generated catch block
			e.printStackTrace();
			componentActualresult.add("Failed to open the browser.");
			componentEndTimer.add(getCurrentTime());
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean quitBrowser() throws Exception {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Driver.close();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Browser Closed successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Browser failed to Close.");
			
		}		
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean login(String userName, String pwd) throws Throwable {
		boolean flag = true;
		try {

			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			new SiteAdminPage().SiteAdmin_Page();
			LOG.info("Login");
			flags.add(type(TravelTrackerHomePage.userName, userName, "User Name"));
			flags.add(typeSecure(TravelTrackerHomePage.password, GetLatestCodeFromBitBucket.decrypt_text(pwd), "Password"));
			flags.add(click(TravelTrackerHomePage.loginButton, "Login Button"));
			checkPageIsReadyUsingJavaScript();
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "Loading Image"));
			/*flags.add(waitForElementPresent(
					SiteAdminPage.siteAdminLink,
					"siteAdminLink", 60));*/
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged in successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add("User login failed.");
			componentEndTimer.add(getCurrentTime());
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean logOut() throws Throwable {
		boolean flag = true;
		try {

			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			LOG.info("logOut");
			flags.add(click(TravelTrackerHomePage.logOff, "Logoff"));

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged out successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add("User logout failed.");
			componentEndTimer.add(getCurrentTime());
		}
		return flag;
	}

	
	public String getBrowser() {
		return this.browser;
	}

}
