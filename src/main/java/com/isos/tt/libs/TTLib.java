package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.automation.report.ReporterConstants;
import com.isos.tt.page.ManualTripEntryPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TravelTrackerHomePage;

public class TTLib extends CommonLib {

	private static final Logger LOG = Logger.getLogger(TTLib.class);

	/*static String component_startTime;
	static String component_endTime;
	static String methodName;

	public static List componentNamesList = new ArrayList();
	public static List componentStartTimer = new ArrayList();
	public static List componentEndTimer = new ArrayList();*/

	@SuppressWarnings("unchecked")
	public boolean clickTravelercount() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			Driver.switchTo().frame("ctl00_MainContent_mapiframe");
			flags.add(isElementPresent(TravelTrackerHomePage.travellerCount,
					"Traveler Count"));
			flags.add(click(TravelTrackerHomePage.travellerCount, "Traveler Count"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "Loading Image"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.tileContent,
					"tileContent", 60));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Clicked on Traveler count.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Failed to click on Traveler count.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean logOut() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			LOG.info("logOut");
			flags.add(click(TravelTrackerHomePage.logOff, "Logoff"));
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged out successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logout failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean validateMessageWindow() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(isElementPresent(TravelTrackerHomePage.messageIcon,
					"Message Icon"));
			flags.add(click(TravelTrackerHomePage.messageIcon, "Message Icon"));
			Driver.switchTo().frame("messageIframe");
			flags.add(assertElementPresent(TravelTrackerHomePage.subject,
					"Message Subject"));
			flags.add(assertElementPresent(TravelTrackerHomePage.messageBody,
					"Message Body"));
			flags.add(assertElementPresent(TravelTrackerHomePage.twoSMSResponse,
					"Two SMS Response"));
			flags.add(assertElementPresent(TravelTrackerHomePage.sendMessageButton,
					"Send Message Button"));
			flags.add(assertElementPresent(TravelTrackerHomePage.travellerName,
					"Traveller Name"));
			flags.add(assertElementPresent(TravelTrackerHomePage.email, "Email"));
			flags.add(assertElementPresent(TravelTrackerHomePage.emailCheckbox,
					"Email Checkbox"));
			flags.add(assertElementPresent(TravelTrackerHomePage.phone, "Phone"));
			flags.add(assertElementPresent(TravelTrackerHomePage.phoneCheckbox,
					"Phone Checkbox"));
			flags.add(assertElementPresent(TravelTrackerHomePage.messageLength,
					"Message Length"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Validate Message Window is successful.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Failed to validate Message Window.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean sendMessage(String subject, String msgBody) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(type(TravelTrackerHomePage.subject, subject,
					"Message Subject"));
			flags.add(type(TravelTrackerHomePage.messageBody, msgBody,
					"Message Body"));
			flags.add(click(TravelTrackerHomePage.twoSMSResponse,
					"Two Way SMS Response"));
			flags.add(assertElementPresent(TravelTrackerHomePage.sendMessageButton,
					"Send Message Button"));
			flags.add(JSClick(TravelTrackerHomePage.sendMessageButton,
					"Send Message Button"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Send message is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Send message is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean closeMessageWindow() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			Driver.switchTo().defaultContent();
			Driver.switchTo().frame("ctl00_MainContent_mapiframe");
			flags.add(click(TravelTrackerHomePage.closePopup,
					"Close Message Window"));
			Driver.switchTo().defaultContent();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Close Message Window is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Close Message Window is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean openCommunicationHistory() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(click(TravelTrackerHomePage.tools, "Tools link"));
			flags.add(click(TravelTrackerHomePage.communicationHistory,
					"Communication History Link"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("open Communication History is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("open Communication History is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean validateMessageContent(String subject) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(assertElementPresent(TravelTrackerHomePage.messageType,
					"Message Type"));
			flags.add(click(TravelTrackerHomePage.searchBtn, "Search Button"));
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.searchResults,
					"Search Results"));
			flags.add(assertElementPresent(TravelTrackerHomePage.searchResults,
					"Search Results"));
			flags.add(assertTextMatching(
					TravelTrackerHomePage.searchResultsSubject, subject,
					"Message Subject in search Results"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Validate Message Content is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Validate Message Content is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean updateRefineByTravelInfo_InLocation() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			Driver.switchTo().frame("ctl00_MainContent_mapiframe");

			flags.add(assertElementPresent(TravelTrackerHomePage.resetBtn,
					"resetBtn"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button"));
			int countryCount_BeforeApply = getCountryCount();
			LOG.info("countryCount_BeforeApply :"
					+ countryCount_BeforeApply);
			flags.add(selectByIndex(TravelTrackerHomePage.location, 1,
					"locationDropdown"));
			Thread.sleep(5000);
			flags.add(isElementPresent(TravelTrackerHomePage.last31Days,
					"last31Days"));
			flags.add(JSClick(TravelTrackerHomePage.last31Days, "last31Days Button"));
			flags.add(JSClick(TravelTrackerHomePage.next24Hours,
					"next24Hours Button"));
			flags.add(JSClick(TravelTrackerHomePage.next1to7Days,
					"next1to7Days Button"));
			flags.add(JSClick(TravelTrackerHomePage.next8to31Days,
					"next8to31Days Button"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn", 60));
			int countryCount_AfterApply = getCountryCount();
			LOG.info("countryCount_AfterApply : "
					+ countryCount_AfterApply);
			if (!(countryCount_BeforeApply == countryCount_AfterApply)) {
				System.out
						.println("The traveller count under the left location pane is updated successfully");
				componentActualresult
						.add("The traveller count under the left location pane is updated successfully");
			} else
				componentActualresult
						.add("The traveller count under the left location pane is NOT updated ");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("The traveller count under the left location pane is NOT updated ");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean updateRefineByTravelInfo_Arriving() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(assertElementPresent(TravelTrackerHomePage.resetBtn,
					"resetBtn"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button"));
			int countryCount_BeforeApply = getCountryCount();
			LOG.info("countryCount_BeforeApply "
					+ countryCount_BeforeApply);
			Driver.findElement(By.xpath(".//select[@id='presetSelect']"))
			.sendKeys(Keys.UP);
			Driver.findElement(By.xpath(".//select[@id='presetSelect']"))
			.sendKeys(Keys.ENTER);
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn", 60));
			int countryCount_AfterApply = getCountryCount();
			LOG.info("countryCount_AfterApply "
					+ countryCount_AfterApply);
			if (!(countryCount_BeforeApply == countryCount_AfterApply))
				componentActualresult
						.add("The traveller count under the left location pane is updated successfully");
			else
				componentActualresult
						.add("The traveller count under the left location pane is NOT updated ");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("The traveller count under the left location pane is NOT updated ");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean updateRefineByTravelInfo_Departing() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(assertElementPresent(TravelTrackerHomePage.resetBtn,
					"resetBtn"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button"));
			int countryCount_BeforeApply = getCountryCount();
			LOG.info("countryCount_BeforeApply "
					+ countryCount_BeforeApply);
			Driver.findElement(By.xpath(".//select[@id='presetSelect']"))
			.sendKeys(Keys.DOWN);
			Driver.findElement(By.xpath(".//select[@id='presetSelect']"))
			.sendKeys(Keys.ENTER);
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn", 60));
			int countryCount_AfterApply = getCountryCount();
			LOG.info("countryCount_AfterApply "
					+ countryCount_AfterApply);
			if (!(countryCount_BeforeApply == countryCount_AfterApply))
				componentActualresult
						.add("The traveller count under the left location pane is updated successfully");
			else
				componentActualresult
						.add("The traveller count under the left location pane is NOT updated ");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("The traveller count under the left location pane is NOT updated ");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean updateRefineByTravelInfo_DepartingAndArriving()
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(assertElementPresent(TravelTrackerHomePage.resetBtn,
					"resetBtn"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button"));
			int countryCount_BeforeApply = getCountryCount();
			LOG.info("countryCount_BeforeApply "
					+ countryCount_BeforeApply);
			Driver.findElement(By.xpath(".//select[@id='presetSelect']"))
			.sendKeys(Keys.DOWN);
			Driver.findElement(By.xpath(".//select[@id='presetSelect']"))
			.sendKeys(Keys.DOWN);
			Driver.findElement(By.xpath(".//select[@id='presetSelect']"))
			.sendKeys(Keys.ENTER);
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn", 60));
			int countryCount_AfterApply = getCountryCount();
			LOG.info("countryCount_AfterApply"
					+ countryCount_AfterApply);
			if (!(countryCount_BeforeApply == countryCount_AfterApply))
				componentActualresult
						.add("The traveller count under the left location pane is updated successfully");
			else
				componentActualresult
						.add("The traveller count under the left location pane is NOT updated ");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("The traveller count under the left location pane is NOT updated ");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public int getCountryCount() throws Throwable {
		int total_count = 0;
		try {
			for (int i = 1; i < 6; i++) {

				List<WebElement> count = Driver.findElements(By
						.xpath(".//div[@id='tileContent']//li[" + i + "]//h2"));
				for (WebElement e : count) {
					total_count = total_count + Integer.parseInt(e.getText());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOG.info("Total count is :" + total_count);
		return total_count;
	}

	@SuppressWarnings("unchecked")
	public boolean updateRefineByRisks() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(assertElementPresent(TravelTrackerHomePage.resetBtn,
					"resetBtn"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button"));

			boolean extremeTravelChkbox = isElementSelected(TravelTrackerHomePage.extremeTravelChkbox);
			if (extremeTravelChkbox == true)
				flags.add(true);
			else
				flags.add(click(TravelTrackerHomePage.extremeTravelChkbox,
						"extremeTravelChkbox"));

			boolean highTravelChkbox = isElementSelected(TravelTrackerHomePage.highTravelChkbox);
			if (highTravelChkbox == true)
				flags.add(true);
			else
				flags.add(click(TravelTrackerHomePage.highTravelChkbox,
						"highTravelChkbox"));

			boolean mediumTravelChkbox = isElementSelected(TravelTrackerHomePage.mediumTravelChkbox);
			if (mediumTravelChkbox == true)
				flags.add(true);
			else
				flags.add(click(TravelTrackerHomePage.mediumTravelChkbox,
						"mediumTravelChkbox"));

			boolean lowTravelChkbox = isElementSelected(TravelTrackerHomePage.lowTravelChkbox);
			if (lowTravelChkbox == true)
				flags.add(true);
			else
				flags.add(click(TravelTrackerHomePage.lowTravelChkbox,
						"lowTravelChkbox"));

			boolean insignificantTravelChkbox = isElementSelected(TravelTrackerHomePage.insignificantTravelChkbox);
			if (insignificantTravelChkbox == true)
				flags.add(true);
			else
				flags.add(click(TravelTrackerHomePage.insignificantTravelChkbox,
						"insignificantTravelChkbox"));

			boolean extremeMedicalChkbox = isElementSelected(TravelTrackerHomePage.extremeMedicalChkbox);
			if (extremeMedicalChkbox == true)
				flags.add(true);
			else
				flags.add(click(TravelTrackerHomePage.extremeMedicalChkbox,
						"extremeMedicalChkbox"));

			boolean highMedicalChkbox = isElementSelected(TravelTrackerHomePage.highMedicalChkbox);
			if (highMedicalChkbox == true)
				flags.add(true);
			else
				flags.add(click(TravelTrackerHomePage.highMedicalChkbox,
						"highMedicalChkbox"));

			boolean mediumMedicalChkbox = isElementSelected(TravelTrackerHomePage.mediumMedicalChkbox);
			if (mediumMedicalChkbox == true)
				flags.add(true);
			else
				flags.add(click(TravelTrackerHomePage.mediumMedicalChkbox,
						"mediumMedicalChkbox"));

			boolean lowMedicalChkbox = isElementSelected(TravelTrackerHomePage.lowMedicalChkbox);
			if (lowMedicalChkbox == true)
				flags.add(true);
			else
				flags.add(click(TravelTrackerHomePage.lowMedicalChkbox,
						"lowMedicalChkbox"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("The traveller count under the left location pane is NOT updated ");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean updateRefineByRisks_TravelAndMedical(String riskType)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();

			boolean extremeTravel;
			boolean highTravel;
			boolean mediumTravel;
			boolean lowTravel;
			boolean insignificantTravel;
			boolean extremeMedical;
			boolean highMedical;
			boolean mediumMedical;
			boolean lowMedical;

			Driver.switchTo().frame("ctl00_MainContent_mapiframe");
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.resetBtn,
					"resetBtn"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button"));

			switch (riskType) {
			case "travel_extreme":
				extremeTravel = isElementSelected(TravelTrackerHomePage.extremeTravelChkbox);
				if (extremeTravel == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.extremeTravelChkbox,
							"extremeTravelChkbox"));
				break;

			case "travel_high":
				highTravel = isElementSelected(TravelTrackerHomePage.highTravelChkbox);
				if (highTravel == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.highTravelChkbox,
							"highTravelChkbox"));
				break;

			case "travel_medium":
				mediumTravel = isElementSelected(TravelTrackerHomePage.mediumTravelChkbox);
				if (mediumTravel == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.mediumTravelChkbox,
							"mediumTravelChkbox"));
				break;

			case "travel_low":
				lowTravel = isElementSelected(TravelTrackerHomePage.lowTravelChkbox);
				if (lowTravel == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.lowTravelChkbox,
							"lowTravelChkbox"));
				break;
			case "travel_insignificant":
				insignificantTravel = isElementSelected(TravelTrackerHomePage.insignificantTravelChkbox);
				if (insignificantTravel == true)
					flags.add(true);
				else
					flags.add(click(
							TravelTrackerHomePage.insignificantTravelChkbox,
							"insignificantTravelChkbox"));
				break;

			case "medical_extreme":
				extremeMedical = isElementSelected(TravelTrackerHomePage.extremeMedicalChkbox);
				if (extremeMedical == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.extremeMedicalChkbox,
							"extremeMedicalChkbox"));
				break;

			case "medical_high":
				highMedical = isElementSelected(TravelTrackerHomePage.highMedicalChkbox);
				if (highMedical == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.highMedicalChkbox,
							"highMedicalChkbox"));
				break;

			case "medical_medium":
				mediumMedical = isElementSelected(TravelTrackerHomePage.mediumMedicalChkbox);
				if (mediumMedical == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.mediumMedicalChkbox,
							"mediumMedicalChkbox"));
				break;

			case "medical_low":
				lowMedical = isElementSelected(TravelTrackerHomePage.lowMedicalChkbox);
				if (lowMedical == true)
					flags.add(true);
				else
					flags.add(click(TravelTrackerHomePage.lowMedicalChkbox,
							"lowMedicalChkbox"));
				break;
			}
			
			/*flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn"));*/
			int travellersCount = getCountryCount();
			LOG.info("Travellers Count :" + travellersCount);
			Driver.switchTo().defaultContent();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("User is able to select "
							+ riskType
							+ " Check box and the Application has refreshed counts on map accordingly.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("User is NOT able to select "
							+ riskType
							+ " Check box and the Application has NOT refreshed counts on map.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean updateRefineByLocation(String locationType, String fte)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();

			if (fte == "1") {
				Driver.switchTo().frame("ctl00_MainContent_mapiframe");
				fte += 1;
				flags.add(assertElementPresent(TravelTrackerHomePage.resetBtn,
						"resetBtn"));
				flags.add(JSClick(TravelTrackerHomePage.resetBtn, "Reset Button"));
				flags.add(waitForInVisibilityOfElement(
						TravelTrackerHomePage.loadingImage, "loadingImage"));
				flags.add(waitForElementPresent(
						TravelTrackerHomePage.filtersBtn,
						"filtersBtn", 60));
				flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button"));
				flags.add(click(TravelTrackerHomePage.dateRange, "dateRange"));
				flags.add(waitForInVisibilityOfElement(
						TravelTrackerHomePage.loadingImage, "loadingImage"));
				flags.add(waitForElementPresent(
						TravelTrackerHomePage.filtersBtn,
						"filtersBtn", 60));
				flags.add(assertElementPresent(
						TravelTrackerHomePage.fromCalendarIcon, "fromCalendarIcon"));
				flags.add(click(TravelTrackerHomePage.fromCalendarIcon,
						"fromCalendarIcon Button"));
				flags.add(isElementPresent(TravelTrackerHomePage.year, "year"));
				flags.add(selectByIndex(TravelTrackerHomePage.year, 9, "year"));
				flags.add(selectByIndex(TravelTrackerHomePage.month, 4, "month"));
				flags.add(click(TravelTrackerHomePage.day, "day"));
				flags.add(waitForInVisibilityOfElement(
						TravelTrackerHomePage.loadingImage, "loadingImage"));
				flags.add(waitForElementPresent(
						TravelTrackerHomePage.filtersBtn,
						"filtersBtn", 60));
			}
			boolean intlCheckbox = false;
			boolean domCheckbox = false;
			boolean expCheckbox = false;

			if (locationType == "International") {
					flags.add(JSClick(TravelTrackerHomePage.domesticCheckbox,
							"extremeTravelChkbox"));
					flags.add(JSClick(TravelTrackerHomePage.expatriateCheckbox,
						"expatriateCheckbox"));
					flags.add(waitForInVisibilityOfElement(
						TravelTrackerHomePage.loadingImage, "loadingImage"));
					flags.add(waitForElementPresent(
							TravelTrackerHomePage.filtersBtn,
							"filtersBtn", 60));
			} else if (locationType == "Domestic") {
					flags.add(JSClick(TravelTrackerHomePage.domesticCheckbox,
						"domesticCheckbox"));
					flags.add(JSClick(TravelTrackerHomePage.internationalCheckbox,
						"internationalCheckbox"));
					flags.add(waitForInVisibilityOfElement(
						TravelTrackerHomePage.loadingImage, "loadingImage"));
					flags.add(waitForElementPresent(
							TravelTrackerHomePage.filtersBtn,
							"filtersBtn", 60));
			} else {
					flags.add(JSClick(TravelTrackerHomePage.expatriateCheckbox,
						"expatriateCheckbox"));
					flags.add(JSClick(TravelTrackerHomePage.domesticCheckbox,
						"domesticCheckbox"));
					flags.add(waitForInVisibilityOfElement(
						TravelTrackerHomePage.loadingImage, "loadingImage"));
					flags.add(waitForElementPresent(
							TravelTrackerHomePage.filtersBtn,
							"filtersBtn", 60));
			}

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("The checkbox "+locationType+"is selected successfully and the application is refreshed counts on map accordingly." );

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("The checkbox "+locationType+"is NOT selected and the application is has not refreshed counts on map.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean viewTravellerByLocation(String location) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(type(TravelTrackerHomePage.searchBox, location,
					"homeCountry"));
			flags.add(click(TravelTrackerHomePage.search, "search"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn", 60));
			flags.add(JSClick(TravelTrackerHomePage.countryName,
					"countryName"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn", 60));
			flags.add(assertElementPresent(TravelTrackerHomePage.locationsTravellerCount,
					"locationsTravellerCount"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("People panel is displayed with list of all travelers for the selected region successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("People panel is NOT displayed with list of all travelers for the selected region .");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean updateRefineByHomeCountry(String countryName)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(assertElementPresent(TravelTrackerHomePage.resetBtn,
					"resetBtn"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "Reset Button"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "Filters Button"));
			int countryCount_BeforeApply = getCountryCount();
			flags.add(type(TravelTrackerHomePage.homeCountry, countryName,
					"homeCountry"));
			Driver.findElement(By.xpath(".//input[@id='country']")).sendKeys(Keys.ENTER);
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			
			int countryCount_AfterApply = getCountryCount();
			LOG.info("countryCount_AfterApply : "+countryCount_AfterApply);
			if (!(countryCount_BeforeApply == countryCount_AfterApply))
				componentActualresult
						.add("The traveller count under the left location pane is updated successfully");
			else
				componentActualresult
						.add("The traveller count under the left location pane is NOT updated as expected ");
			componentEndTimer.add(getCurrentTime());
			
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
			.add("The traveller count under the left location pane is updated successfully");
			
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("The traveller count under the left location pane is NOT updated as expected.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean performLocationSearch(String countryName) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			Driver.switchTo().frame("ctl00_MainContent_mapiframe");
		/*	flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.searchDropdown,
					"searchDropdown"));*/
			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown,
					"searchDropdown"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 0,
					"searchDropdown"));
			flags.add(type(TravelTrackerHomePage.searchBox, countryName,
					"searchBox"));
			flags.add(click(TravelTrackerHomePage.search, "search Button"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(JSClick(TravelTrackerHomePage.india, "India"));
			/*flags.add(waitForElementPresent(
					TravelTrackerHomePage.tileContent,
					"tileContent", 20));*/
			flags.add(assertElementPresent(TravelTrackerHomePage.tileContent,
					"tileContent"));
			//Driver.findElement(By.xpath(".//div[@id='tileContent']//following-sibling::ul//span[contains(text(),'New "+countryName+"')]"));
			
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
		/*	flags.add(waitForElementPresent(
					TravelTrackerHomePage.locationsTravellerCount,
					"locationsTravellerCount", 20));*/
			flags.add(assertElementPresent(
					TravelTrackerHomePage.locationsTravellerCount,
					"locationsTravellerCount"));
			flags.add(assertElementPresent(TravelTrackerHomePage.export, "export"));
			flags.add(assertElementPresent(TravelTrackerHomePage.messageBtn,
					"messageBtn"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Location Search is performed successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Location Search is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean performTravellerSearch(String travellerName)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			new SiteAdminPage().SiteAdmin_Page();
			/*flags.add(click(TravelTrackerHomePage.dateRange,
					"dateRange"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.fromCalendarIcon,
					"fromCalendarIcon", 60));
			flags.add(click(TravelTrackerHomePage.fromCalendarIcon,
					"fromCalendarIcon Button"));
			flags.add(isElementPresent(TravelTrackerHomePage.year, "year"));
			flags.add(selectByIndex(TravelTrackerHomePage.year, 9, "year"));
			flags.add(selectByIndex(TravelTrackerHomePage.month, 4, "month"));
			flags.add(click(TravelTrackerHomePage.day, "day"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.searchDropdown,
					"searchDropdown", 60));*/
			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown,
					"searchDropdown"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 2,
					"searchDropdown"));
			flags.add(type(TravelTrackerHomePage.searchBox, travellerName,
					"searchBox"));
			flags.add(click(TravelTrackerHomePage.search, "search Button"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			/*flags.add(waitForElementPresent(
					TravelTrackerHomePage.tileContent,
					"tileContent", 60));*/
			flags.add(assertElementPresent(TravelTrackerHomePage.tileContent,
					"tileContent"));
			flags.add(assertElementPresent(TravelTrackerHomePage.export, "export"));
			flags.add(assertElementPresent(TravelTrackerHomePage.messageBtn,
					"messageBtn"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.travellerCountPanel,
					"travellerCountPanel"));
			Driver.findElement(By.xpath(".//*[@id='scrollDiv']//h2[contains(text(),'"+travellerName+"')]")).click();
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			/*flags.add(waitForElementPresent(
					TravelTrackerHomePage.messageBtn,
					"travellerDetailsHeader", 60));*/
			flags.add(assertElementPresent(
					TravelTrackerHomePage.messageBtn,
					"travellerDetailsHeader"));
			/*Driver.switchTo().defaultContent();
			flags.add(click(SiteAdminPage.siteAdminLink, "siteAdminLink"));
			flags.add(click(TravelTrackerHomePage.mapHomeTab, "mapHomeTab"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn", 60));
			Driver.switchTo().frame("ctl00_MainContent_mapiframe");*/
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Traveller Search is performed successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean performFlightSearch(String flightName) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			
			
			/*flags.add(waitForElementPresent(
					TravelTrackerHomePage.searchDropdown,
					"searchDropdown", 60));*/
			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown,
					"searchDropdown"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 1,
					"searchDropdown"));
			/*flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.dateRange, "dateRange"));
			flags.add(click(TravelTrackerHomePage.dateRange,
					"dateRange"));*/
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			/*flags.add(waitForElementPresent(
					TravelTrackerHomePage.fromCalendarIcon,
					"fromCalendarIcon", 60));*/
			flags.add(click(TravelTrackerHomePage.fromCalendarIcon,
					"fromCalendarIcon Button"));
			flags.add(isElementPresent(TravelTrackerHomePage.year, "year"));
			flags.add(selectByIndex(TravelTrackerHomePage.year, 9, "year"));
			flags.add(selectByIndex(TravelTrackerHomePage.month, 4, "month"));
			flags.add(click(TravelTrackerHomePage.day, "day"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(type(TravelTrackerHomePage.searchBox, flightName, "searchBox"));
			flags.add(click(TravelTrackerHomePage.search, "search"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			/*flags.add(waitForElementPresent(
					TravelTrackerHomePage.tileContent,
					"tileContent", 60));*/
			flags.add(assertElementPresent(TravelTrackerHomePage.tileContent,
					"tileContent"));
			flags.add(assertElementPresent(TravelTrackerHomePage.flightCount,
					"flightCount"));
			Driver.findElement(By.xpath(".//label[contains(text(),'"+flightName+"')]")).click();
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			/*flags.add(waitForElementPresent(
					TravelTrackerHomePage.export,
					"export", 60));*/
			flags.add(assertElementPresent(TravelTrackerHomePage.export, "export"));
			flags.add(assertElementPresent(TravelTrackerHomePage.messageBtn,
					"messageBtn"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Flight Search is performed successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Flight Search is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean mapZoom(String countryName) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();

			Driver.switchTo().frame("ctl00_MainContent_mapiframe");
			flags.add(assertElementPresent(TravelTrackerHomePage.searchDropdown,
					"searchDropdown"));
			flags.add(selectByIndex(TravelTrackerHomePage.searchDropdown, 0,
					"searchDropdown"));
			flags.add(type(TravelTrackerHomePage.searchBox, countryName,
					"searchBox"));
			flags.add(click(TravelTrackerHomePage.search, "search Button"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			/*flags.add(waitForElementPresent(
					TravelTrackerHomePage.tileContent,
					"tileContent", 60));*/
			flags.add(assertElementPresent(TravelTrackerHomePage.tileContent,
					"tileContent"));
			//Driver.findElement(By.xpath(".//div[@id='tileContent']//span[contains(text(),'[COUNTRY] "+countryName+"')]")).click();
			flags.add(JSClick(TravelTrackerHomePage.india, "India"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Map zoom is successful.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Map zoom is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyTravellercount(String countryName) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			//Driver.findElement(By.xpath(".//h1[text()='"+countryName+"']/../../following-sibling::div//h2")).click();
			flags.add(waitForVisibilityOfElement(TravelTrackerHomePage.travellersList,
					"travellersList"));
			flags.add(assertElementPresent(TravelTrackerHomePage.travellersList,
					"travellersList"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.locationsTravellerCount,
					"locationsTravellerCount"));
			flags.add(assertElementPresent(TravelTrackerHomePage.export, "export"));
			flags.add(assertElementPresent(TravelTrackerHomePage.messageBtn,
					"messageBtn"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Traveller Search is performed successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean CheckTravellerCheckboxTravellerPanel() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(JSClick(
					TravelTrackerHomePage.travellerCheckbox, "travellerCheckbox"));
			flags.add(click(TravelTrackerHomePage.export, "export"));

			Robot robot = new Robot();
			Thread.sleep(5000);
			robot.mouseMove(510, 390);
			robot.delay(1500);
			robot.mousePress(InputEvent.BUTTON1_DOWN_MASK); // press left click
			robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK); // release left
																// click
			robot.delay(1500);
			robot.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(8000);

			List<String> results = new ArrayList<String>();
			File[] files = new File(ReporterConstants.downloaded_file_path)
					.listFiles();
			for (File file : files) {
				if (file.isFile()) {
					results.add(file.getName());
				}
			}

			String filename = ReporterConstants.downloaded_file_path + "/"
					+ results.get(0);
			List<String> columnNames = new ArrayList<String>();
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(filename);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
				XSSFSheet sheet = workbook.getSheetAt(0);
				XSSFRow row = sheet.getRow(2);
				int noOfRows = sheet.getLastRowNum();
				int noOfTravellers = noOfRows - 3;
				LOG.info("noOfTravellers" + noOfTravellers);
				for (int i = 0; i < row.getLastCellNum(); i++) {
					LOG.info("data***"
							+ row.getCell(i).getStringCellValue().trim());
					columnNames.add(i, row.getCell(i).getStringCellValue()
							.trim());

				}
				for (int j = 0; j < columnNames.size(); j++)
					if (columnNames.get(j).equalsIgnoreCase("First Name"))
						System.out
								.println("Customer name is present successfully in excel sheet");

			} catch (Exception e) {
				e.printStackTrace();
			}
			flags.add(click(TravelTrackerHomePage.closeTravellersList,
					"closeTravellersList"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Traveller Search is performed successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyAlertTile() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			Driver.switchTo().frame("ctl00_MainContent_mapiframe");
			flags.add(click(TravelTrackerHomePage.alertsTab, "alertsTab"));
			flags.add(click(TravelTrackerHomePage.sortBy, "sortBy"));
			flags.add(assertElementPresent(TravelTrackerHomePage.country, "country"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.travellerCountSort, "travellerCountSort"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Traveller Search is performed successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean selectAlertTile() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(click(TravelTrackerHomePage.alertTile, "alertTile"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.tileContent,
					"tileContent", 60));
			flags.add(assertElementPresent(TravelTrackerHomePage.tileContent,
					"tileContent"));
			flags.add(assertElementPresent(TravelTrackerHomePage.export, "export"));
			flags.add(assertElementPresent(TravelTrackerHomePage.messageBtn,
					"messageBtn"));
			flags.add(click(TravelTrackerHomePage.closeTravellersList,
					"closeTravellersList"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Traveller Search is performed successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyAlertDetails() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			flags.add(click(TravelTrackerHomePage.readMoreLink, "readMoreLink"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "loadingImage"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.alertPopupHeader,
					"alertPopupHeader", 60));
			flags.add(assertElementPresent(TravelTrackerHomePage.alertPopupHeader,
					"alertPopupHeader"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.alertPopupLocation, "alertPopupLocation"));
			flags.add(assertElementPresent(
					TravelTrackerHomePage.alertPopupEventDate, "alertPopupLocation"));
			flags.add(click(TravelTrackerHomePage.closeAlertPopup,
					"closeAlertPopup"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Traveller Search is performed successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
		}
		return flag;
	}

	

	@SuppressWarnings("unchecked")
	public boolean clickSearchTraveller() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			new ManualTripEntryPage().ManualTripEntry_Page();
			flags.add(click(ManualTripEntryPage.travellerSearchBtn,
					"travellerSearchBtn"));
			flags.add(assertElementPresent(
					ManualTripEntryPage.createNewTravellerBtn,
					"createNewTravellerBtn"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Traveller Search is performed successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean searchExistingTraveller(String travellername)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			new SiteAdminPage().SiteAdmin_Page();
			new ManualTripEntryPage().ManualTripEntry_Page();
			flags.add(type(ManualTripEntryPage.profileLookup, travellername,
					"travellername"));
			List<WebElement> elements = Driver.findElements(By.xpath(".//*[@id='ctl00_MainContent_ucProfileLookup_autocompleteDropDownPanel']/div"));
			for(WebElement element : elements) {
				LOG.info(element.getText());
			}
			flags.add(waitForInVisibilityOfElement(
					ManualTripEntryPage.autoComplete, "autoComplete"));
			Driver.findElement(By.xpath(".//input[@id='ctl00_MainContent_ucProfileLookup_autoCompleteTextProfilelookup']")).sendKeys(Keys.DOWN);
			Driver.findElement(By.xpath(".//input[@id='ctl00_MainContent_ucProfileLookup_autoCompleteTextProfilelookup']")).sendKeys(Keys.ENTER);
			flags.add(isElementPresent(ManualTripEntryPage.selectTravellerBtn,
					"selectTravellerBtn"));
			Actions actions = new Actions(Driver);
			actions.moveToElement(Driver.findElement(By.xpath("//input[@id='ctl00_MainContent_btnAddProfile']"))).click().perform();
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Traveller Search is performed successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean createNewTrip() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			new ManualTripEntryPage().ManualTripEntry_Page();
			flags.add(isElementPresent(ManualTripEntryPage.createNewTripTab,
					"Create New Trip Tab"));
			flags.add(click(ManualTripEntryPage.createNewTripTab,
					"Create New Trip Tab"));
			// flags.add(assertElementPresent(MyTripsPage.tripName,
			// "Trip Name"));
			/*
			 * flags.add(waitForVisibilityOfElement( MyTripsPage.tripName,
			 * "tripName"));
			 */
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Create New Trip is successful.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Create New Trip is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean selectPeopleFilter(String checkBx, String location)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());


			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();

			Driver.switchTo().frame("ctl00_MainContent_mapiframe");
			flags.add(waitForVisibilityOfElement(
					TravelTrackerHomePage.resetBtn, "resetBtn"));
			flags.add(click(TravelTrackerHomePage.resetBtn, "resetBtn"));
			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "Loading Image"));
			flags.add(waitForElementPresent(
					TravelTrackerHomePage.filtersBtn,
					"filtersBtn", 60));
			flags.add(click(TravelTrackerHomePage.filtersBtn, "filtersBtn"));

			switch (checkBx) {
			case "Last 31 days":
				flags.add(click(TravelTrackerHomePage.location, "location"));
				Driver.findElement(By.xpath(".//select[@id='presetSelect']"))
						.sendKeys(Keys.ENTER);
				flags.add(waitForInVisibilityOfElement(
						TravelTrackerHomePage.loadingImage, "Loading Image"));
				/*flags.add(waitForElementPresent(
						TravelTrackerHomePage.last31Days,
						"last31Days", 60));*/
				flags.add(isElementPresent(TravelTrackerHomePage.last31Days,
						"last31Days"));
				flags.add(JSClick(TravelTrackerHomePage.last31Days,
						"last31Days Button"));
				break;

			case "Now":
				flags.add(click(TravelTrackerHomePage.location, "location"));
				Driver.findElement(By.xpath(".//select[@id='presetSelect']"))
						.sendKeys(Keys.ARROW_DOWN);
				flags.add(waitForInVisibilityOfElement(
						TravelTrackerHomePage.loadingImage, "Loading Image"));
				/*flags.add(waitForElementPresent(
						TravelTrackerHomePage.last31Days,
						"last31Days", 60));*/
				flags.add(isElementPresent(TravelTrackerHomePage.last31Days,
						"last31Days"));
				if (isElementSelected(TravelTrackerHomePage.now))
					flags.add(true);
				else
					flags.add(JSClick(TravelTrackerHomePage.now, "Now"));
				break;

			case "Next 24 hours":
				flags.add(click(TravelTrackerHomePage.location, "location"));
				Driver.findElement(By.xpath(".//select[@id='presetSelect']"))
						.sendKeys(Keys.ENTER);
				flags.add(waitForInVisibilityOfElement(
						TravelTrackerHomePage.loadingImage, "Loading Image"));
				/*flags.add(waitForElementPresent(
						TravelTrackerHomePage.next24Hours,
						"next24Hours", 60));*/
				flags.add(isElementPresent(TravelTrackerHomePage.next24Hours,
						"next24Hours"));
				flags.add(JSClick(TravelTrackerHomePage.next24Hours,
						"next24Hours Button"));
				flags.add(JSClick(TravelTrackerHomePage.last31Days,
						"last31Days Button"));
				break;

			case "Next 1 to 7 days":
				flags.add(click(TravelTrackerHomePage.location, "location"));
				Driver.findElement(By.xpath(".//select[@id='presetSelect']"))
						.sendKeys(Keys.ENTER);
				flags.add(isElementPresent(TravelTrackerHomePage.next1to7Days,
						"next1to7Days"));
				flags.add(JSClick(TravelTrackerHomePage.next1to7Days,
						"next1to7Days Button"));
				flags.add(JSClick(TravelTrackerHomePage.next24Hours,
						"next24Hours Button"));
				break;

			case "Next 8 to 31 days":
				flags.add(click(TravelTrackerHomePage.location, "location"));
				Driver.findElement(By.xpath(".//select[@id='presetSelect']"))
						.sendKeys(Keys.UP);
				Driver.findElement(By.xpath(".//select[@id='presetSelect']"))
						.sendKeys(Keys.ENTER);
				flags.add(waitForInVisibilityOfElement(
						TravelTrackerHomePage.loadingImage, "Loading Image"));
				/*flags.add(waitForElementPresent(
						TravelTrackerHomePage.next8to31Days,
						"next8to31Days", 60));*/
				flags.add(isElementPresent(TravelTrackerHomePage.next8to31Days,
						"next8to31Days"));
				flags.add(JSClick(TravelTrackerHomePage.next8to31Days,
						"next8to31Days Button"));
				flags.add(JSClick(TravelTrackerHomePage.next1to7Days,
						"next1to7Days Button"));
				break;

			default:
				break;
			}

			flags.add(waitForInVisibilityOfElement(
					TravelTrackerHomePage.loadingImage, "Loading Image"));
			int travellersCount = getCountryCount();
			LOG.info("travellersCount is " + travellersCount);
			Driver.switchTo().defaultContent();

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("The application is refreshed successfully and the appropriate travellers count in Map-UI is displayed.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult
					.add("The application is NOT refreshed and the appropriate travellers count in Map-UI is NOT displayed");
		}
		return flag;
	}


	public boolean typeNewsReportName(String chars) throws Throwable {
		boolean flag = true;
		try {
			// List<Boolean> flags = new ArrayList<>();
			// setComponentStartTimer();
			if (browser.equalsIgnoreCase("ie")) {
				Robot rb = new Robot();
				char[] ch = chars.toCharArray();
				int i = ch.length;
				int j = 0;
				while (j < i) {
					int kcode = (int) ch[j] - 32;
					rb.keyPress(kcode);
					rb.keyRelease(kcode);
					j++;
				}
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
				// setComponentEndTimer();
				// // long duration = (endTime - startTime);
				// LOG.info("createReport took " + duration +
				// " milliseconds");
			} else {
				Alert alert = Driver.switchTo().alert();
				alert.sendKeys(chars);
				alert.accept();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();

		}
		return flag;
	}

}
