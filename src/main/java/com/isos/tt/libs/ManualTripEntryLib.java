package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;

import com.automation.testrail.TestScriptDriver;
import com.isos.tt.page.ManualTripEntryPage;
import com.isos.tt.page.MyTripsPage;
import com.isos.tt.page.SiteAdminPage;
import com.isos.tt.page.TravelTrackerHomePage;

public class ManualTripEntryLib extends CommonLib {

	private static final Logger LOG = Logger.getLogger(ManualTripEntryLib.class);

	int randomNumber = generateRandomNumber();
	String tripName = "InternationalSOS" + randomNumber;
	@SuppressWarnings("unchecked")
	public boolean verifyManualTripEntryPage() throws Throwable {
		boolean flag = true;
		try {
			
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			new ManualTripEntryPage().ManualTripEntry_Page();
			/*flags.add(waitForElementPresent(
					TravelTrackerHomePage.tools,
					"tools", 60));*/
			
			flags.add(click(TravelTrackerHomePage.tools, "Tools Link"));
			flags.add(waitForVisibilityOfElement(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link"));
			flags.add(JSClick(ManualTripEntryPage.manualTripEntryLink,
					"Manual Trip Entry Link"));
			flags.add(assertElementPresent(ManualTripEntryPage.selectTraveller,
					"Select Traveller"));
			flags.add(isElementEnabled(ManualTripEntryPage.selectButton));
			flags.add(isElementEnabled(ManualTripEntryPage.createNewTravellerBtn));
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Verified ManualTrip Entry page successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add("ManualTrip Entry page verification failed.");
			componentEndTimer.add(getCurrentTime());
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean createNewTraveller(String firstName, String middleName, String lastName, String comments,
			String phoneNumber, String emailAddress, String contractorId)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			new SiteAdminPage().SiteAdmin_Page();
			new ManualTripEntryPage().ManualTripEntry_Page();
			flags.add(click(ManualTripEntryPage.createNewTravellerBtn,
					"createNewTravellerBtn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					ManualTripEntryPage.firstName,
					"firstName", 60));
			flags.add(isElementPresent(ManualTripEntryPage.firstName, "firstName"));
			flags.add(type(ManualTripEntryPage.firstName, firstName, "firstName"));
			flags.add(type(ManualTripEntryPage.middleName, middleName, "firstName"));
			flags.add(type(ManualTripEntryPage.lastName, lastName, "lastName"));
			flags.add(selectByIndex(ManualTripEntryPage.homeCountryList, 13,
					"homeCountryList"));
			flags.add(type(ManualTripEntryPage.comments, comments,
					"comments"));
			flags.add(selectByIndex(ManualTripEntryPage.homeSite, 2, "homeSite"));
			flags.add(selectByIndex(ManualTripEntryPage.phonePriority, 1,
					"phonePriority"));
			flags.add(selectByIndex(ManualTripEntryPage.phoneType, 2, "phoneType"));
			flags.add(selectByIndex(ManualTripEntryPage.countryCode, 12,
					"countryCode"));
			flags.add(type(ManualTripEntryPage.phoneNumber, phoneNumber,
					"phoneNumber"));
			flags.add(selectByIndex(ManualTripEntryPage.emailPriority, 1,
					"emailPriority"));
			flags.add(selectByIndex(ManualTripEntryPage.emailType, 3, "emailType"));
			flags.add(type(ManualTripEntryPage.emailAddress, emailAddress,
					"emailAddress"));
			flags.add(type(ManualTripEntryPage.contractorId, contractorId,
					"contractorId"));
			flags.add(selectByIndex(ManualTripEntryPage.department, 3,
					"department"));
			flags.add(selectByIndex(ManualTripEntryPage.documentCountryCode, 2,
					"department"));
			flags.add(selectByIndex(ManualTripEntryPage.documentType, 1,
					"department"));
			flags.add(click(ManualTripEntryPage.saveTravellerDetails,
					"saveTravellerDetails"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"firstName", 60));
			flags.add(assertElementPresent(
					ManualTripEntryPage.TravellerDetailsSuccessMessage,
					"TravellerDetailsSuccessMessage"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult
					.add("Traveller Search is performed successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean copyTripProfileDetails(String tripName, String addTraveller) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			new MyTripsPage().MyTrips_Page();
			new ManualTripEntryPage().ManualTripEntry_Page();
			new SiteAdminPage().SiteAdmin_Page();
			flags.add(click(ManualTripEntryPage.copyTrip, "copyTrip"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.tripName,
					"tripName", 60));
			flags.add(isElementPresent(MyTripsPage.tripName,
					"tripName"));
			flags.add(type(MyTripsPage.tripName, tripName, "tripName"));
			flags.add(type(ManualTripEntryPage.addTraveller, addTraveller, "addTraveller"));
			Thread.sleep(2000);
			flags.add(isElementPresent(ManualTripEntryPage.addTravellersDropDownPanel,
					"addTravellersDropDownPanel"));
			flags.add(click(ManualTripEntryPage.addTravellersDropDownPanel,
					"addTravellersDropDownPanel"));
			flags.add(click(ManualTripEntryPage.addTravellerbtn,
					"addTravellerbtn"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.saveTripInfo,
					"saveTripInfo", 60));
			flags.add(click(MyTripsPage.saveTripInfo,
					"saveTripInfo"));
			flags.add(waitForInVisibilityOfElement(
					SiteAdminPage.progressImage, "progressImage"));
			flags.add(waitForElementPresent(
					MyTripsPage.tripInfoSuccessMessage,
					"tripInfoSuccessMessage", 60));
			flags.add(assertElementPresent(MyTripsPage.tripInfoSuccessMessage,
					"tripInfoSuccessMessage"));
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Traveller Search is performed successfully.");
			
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Traveller Search is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterTripName_MTE(String ticketCountry) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			flags.add(isElementPresent(MyTripsPage.tripName, "Trip Name"));
			flags.add(type(MyTripsPage.tripName, tripName, "Trip Name"));
			LOG.info("The trip entered****** :" +tripName);
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Trip name is entered successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Trip name entry is failed.");
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean verifyTripCreated() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new TravelTrackerHomePage().TravelTracker_Page();
			new ManualTripEntryPage().ManualTripEntry_Page();
			flags.add(assertElementPresent(ManualTripEntryPage.verifyTripName,
					"verifyTripName"));
			String tripNameText = getText(ManualTripEntryPage.verifyTripName,
					"Trip Name");
			LOG.info("Create Trip Name : " + tripNameText);
			flags.add(assertTextMatching(ManualTripEntryPage.verifyTripName, tripName,
					"Trip Name"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Create Trip verification is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Create Trip verification is failed.");
		}
		return flag;
	}
	
	public boolean typeNewsReportName(String chars) throws Throwable {
		boolean flag = true;
		try {
			// List<Boolean> flags = new ArrayList<>();
			// 
			if (browser.equalsIgnoreCase("ie")) {
				Robot rb = new Robot();
				char[] ch = chars.toCharArray();
				int i = ch.length;
				int j = 0;
				while (j < i) {
					int kcode = (int) ch[j] - 32;
					rb.keyPress(kcode);
					rb.keyRelease(kcode);
					j++;
				}
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_TAB);
				rb.keyRelease(KeyEvent.VK_TAB);
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
				// setComponentEndTimer();
				// // long duration = (endTime - startTime);
				// LOG.info("createReport took " + duration +
				// " milliseconds");
			} else {
				Alert alert = Driver.switchTo().alert();
				alert.sendKeys(chars);
				alert.accept();
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();

		}
		return flag;
	}

}
