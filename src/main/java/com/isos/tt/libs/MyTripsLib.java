package com.isos.tt.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.isos.tt.page.MyTripsPage;
import com.isos.tt.page.RiskRatingsPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.sample.TestCaseCreationUtility.GetLatestCodeFromBitBucket;

public class MyTripsLib extends CommonLib {

	private static final Logger LOG = Logger.getLogger(MyTripsLib.class);

	TestEngineWeb tWeb = new TestEngineWeb();
	/*static String component_startTime;
	static String component_endTime;
	static String methodName;

	public static List componentNamesList = new ArrayList();
	public static List componentStartTimer = new ArrayList();
	public static List componentEndTimer = new ArrayList();*/

	int randomNumber = generateRandomNumber();
	String tripName = "InternationalSOS" + randomNumber;

	@SuppressWarnings("unchecked")
	public boolean myTripsLogin(String userName, String pwd) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			new TravelTrackerHomePage().TravelTracker_Page();
			LOG.info("My Trips Login");
			flags.add(type(MyTripsPage.userName, userName, "User Name"));
			flags.add(type(MyTripsPage.password, GetLatestCodeFromBitBucket.decrypt_text(pwd), "Password"));
			flags.add(click(MyTripsPage.loginButton, "Login Button"));
			componentActualresult.add("My Trips Login is successful.");
			componentEndTimer.add(getCurrentTime());
			
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("My Trips Login is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean clickCreateNewTrip() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			flags.add(isElementPresent(MyTripsPage.createNewTripTab,
					"Create New Trip Tab"));
			flags.add(click(MyTripsPage.createNewTripTab, "Create New Trip Tab"));
			flags.add(isElementPresent(MyTripsPage.tripName, "Trip Name"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Create New Trip is successful.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Create New Trip is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterTripName_myTrips(String ticketCountry) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			flags.add(isElementPresent(MyTripsPage.tripName, "Trip Name"));
			flags.add(type(MyTripsPage.tripName, tripName, "Trip Name"));
			LOG.info("The trip entered****** :" +tripName);
			flags.add(type(MyTripsPage.ticketCountry, ticketCountry, "ticketCountry"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Trip name is entered successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Trip name entry is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterFlightDetails(String airline, String departureCity,
			String arrivalCity, String flightNumber) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			flags.add(isElementPresent(MyTripsPage.addFlightTab,
					"Add Flight Tab"));
			flags.add(click(MyTripsPage.addFlightTab, "Add Flight Tab"));
			flags.add(isElementPresent(MyTripsPage.airline, "Airline"));
			flags.add(type(MyTripsPage.airline, airline, "Airline"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.airlineLoading,
					"airlineLoading"));
			//Thread.sleep(2000);
			/*List<WebElement> flights = Driver.findElements(By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_airline_txtSmartTextBoxAirline')]"));
			for(WebElement fli : flights)
				if(fli.getText().equalsIgnoreCase(airline)) {
					LOG.info(fli.getText());
					fli.click();
				}*/
			Driver.findElement(By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_airline_txtSmartTextBoxAirline')]")).sendKeys(Keys.ARROW_DOWN);;
			Driver.findElement(By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_airline_txtSmartTextBoxAirline')]")).sendKeys(Keys.ENTER);
			
			flags.add(type(MyTripsPage.flightDepartureCity, departureCity,
					"Departure City"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.flightDepartureCityLoading,
					"flightDepartureCityLoading"));
			/*List<WebElement> cities = Driver.findElements(By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_departureCity_txtSmartTextBoxAirport')]"));
			for(WebElement city : cities)
				if(city.getText().equalsIgnoreCase(departureCity)) {
					LOG.info(city.getText());
					city.click();
				}*/
			//Thread.sleep(2000);
			
			Driver.findElement(
					By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_departureCity_txtSmartTextBoxAirport')]"))
					.sendKeys(Keys.ARROW_DOWN);
			Driver.findElement(
					By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_departureCity_txtSmartTextBoxAirport')]"))
					.sendKeys(Keys.ENTER);
			flags.add(type(MyTripsPage.flightArrivalCity, arrivalCity,
					"Arrival City"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.flightArrivalCityLoading,
					"flightArrivalCityLoading"));
			Driver.findElement(
					By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_arrivalCity_txtSmartTextBoxAirport')]"))
					.sendKeys(Keys.ARROW_DOWN);
			Driver.findElement(
					By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddEditFlightSegment_arrivalCity_txtSmartTextBoxAirport')]"))
					.sendKeys(Keys.ENTER);
			flags.add(type(MyTripsPage.flightNumber, flightNumber,
					"Flight Number"));
			flags.add(type(MyTripsPage.departureDate, getCurrentDate(),
					"Departure Date"));
			flags.add(type(MyTripsPage.arrivalDate, getFutureDate(),
					"Arrival Date"));
			flags.add(selectByValue(MyTripsPage.departureHr, "21",
					"Departure Hour"));
			flags.add(selectByValue(MyTripsPage.departureMin, "10",
					"Departure Min"));
			flags.add(selectByValue(MyTripsPage.arrivalHr, "09", "Arrival Hour"));
			flags.add(selectByValue(MyTripsPage.arrivalMin, "15", "Arrival Min"));
			flags.add(click(MyTripsPage.saveFlight, "Save Flight Details"));
			flags.add(waitForInVisibilityOfElement(MyTripsPage.imageProgress,
					"Loading Image Progress"));
			flags.add(waitForElementPresent(
					MyTripsPage.flightSuccessMessage,
					"flightSuccessMessage", 60));
			flags.add(assertElementPresent(MyTripsPage.flightSuccessMessage,
					"Flight Details successfully saved"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Flight details are entered successfully.");

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Flight details entry is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterAccommodationDetails(String hotelName, String city,
			String country, String accommodationType) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			flags.add(isElementPresent(MyTripsPage.addAccommodationTab,
					"Add Accommodation Tab"));
			flags.add(click(MyTripsPage.addAccommodationTab,
					"Add Accommodation Tab"));
			flags.add(isElementPresent(MyTripsPage.hotelName, "Hotel Name"));
			flags.add(type(MyTripsPage.hotelName, hotelName, "Hotel Name"));
			flags.add(type(MyTripsPage.checkInDate, getCurrentDate(),
					"Check-In Date"));
			flags.add(type(MyTripsPage.checkOutDate, getFutureDate(),
					"Check-Out Date"));
			flags.add(click(MyTripsPage.address, "Address"));
			flags.add(isElementPresent(MyTripsPage.city, "City"));
			flags.add(type(MyTripsPage.city, city, "City"));
			flags.add(type(MyTripsPage.country, country, "Country"));
			flags.add(click(MyTripsPage.findButton, "Find Button"));
			flags.add(waitForInVisibilityOfElement(MyTripsPage.spinnerImage,
					"Spinner Image"));
			flags.add(click(MyTripsPage.suggestedAddress, "Suggested Address"));
			flags.add(click(MyTripsPage.selectAddress, "Select Address"));
			flags.add(waitForInVisibilityOfElement(MyTripsPage.spinnerImage,
					"Spinner Image"));
			flags.add(selectByVisibleText(MyTripsPage.accommodationType, accommodationType, "accommodationType"));
			flags.add(isElementPresent(MyTripsPage.saveAccommodation,
					"Save Accommodation Button"));
			flags.add(click(MyTripsPage.saveAccommodation,
					"Save Accommodation Details"));

			flags.add(waitForInVisibilityOfElement(MyTripsPage.imageProgress,
					"Loading Image Progress"));
			flags.add(assertElementPresent(
					MyTripsPage.accommodationSuccessMessage,
					"Accommodation Details successfully saved"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentActualresult.add("Accommodation details are entered successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Accommodation details entry is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean enterTrainDetails(String departureCity, String arrivalCity,
			String trainNo) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			flags.add(isElementPresent(MyTripsPage.addTrainTab, "Add Train Tab"));
			flags.add(click(MyTripsPage.addTrainTab, "Add Train Tab"));
			flags.add(isElementPresent(MyTripsPage.trainCarrier,
					"Train Carrier"));
			flags.add(selectByIndex(MyTripsPage.trainCarrier, 4, "Train Carrier"));
			flags.add(click(MyTripsPage.trainCarrier, "Train Carrier"));
			flags.add(click(MyTripsPage.trainCarrierValue,
					"Train Carrier Value"));
			flags.add(type(MyTripsPage.trainDepartureCity, departureCity,
					"Departure City"));
			//Thread.sleep(2000);
			flags.add(waitForVisibilityOfElement(MyTripsPage.trainDepartureCityLoading,
					"trainDepartureCityLoading"));
			/*Driver.findElement(
					By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_departureCity_txtSmartTextBoxRailStation')]"))
					.sendKeys(Keys.ARROW_DOWN);*/
			Driver.findElement(
					By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_departureCity_txtSmartTextBoxRailStation')]"))
					.sendKeys(Keys.ENTER);
			flags.add(type(MyTripsPage.trainArrivalCity, arrivalCity,
					"Arrival City"));
			//Thread.sleep(2000);
			flags.add(waitForVisibilityOfElement(MyTripsPage.trainArrivalCityLoading,
					"trainArrivalCityLoading"));
			/*Driver.findElement(
					By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_arrivalCity_txtSmartTextBoxRailStation')]"))
					.sendKeys(Keys.ARROW_DOWN);*/
			Driver.findElement(
					By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddTrainSegment_arrivalCity_txtSmartTextBoxRailStation')]"))
					.sendKeys(Keys.ENTER);
			flags.add(type(MyTripsPage.trainNumber, trainNo, "Train Number"));
			flags.add(type(MyTripsPage.trainDepartureDate, getCurrentDate(),
					"Departure Date"));
			flags.add(type(MyTripsPage.trainArrivalDate, getFutureDate(),
					"Arrival Date"));

			flags.add(selectByValue(MyTripsPage.trainDepartureHr, "23",
					"Departure Hour"));
			flags.add(selectByValue(MyTripsPage.trainDepartureMin, "10",
					"Departure Min"));
			flags.add(selectByValue(MyTripsPage.trainArrivalHr, "14",
					"Arrival Hour"));
			flags.add(selectByValue(MyTripsPage.trainArrivalMin, "10",
					"Arrival Min"));

			flags.add(JSClick(MyTripsPage.saveTrain, "Save Train Details"));
			flags.add(waitForInVisibilityOfElement(MyTripsPage.imageProgress,
					"Loading Image Progress"));
			flags.add(waitForElementPresent(
					MyTripsPage.trainSuccessMessage,
					"trainSuccessMessage", 60));
			flags.add(assertElementPresent(MyTripsPage.trainSuccessMessage,
					"Train Details successfully saved"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Train details are entered successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Train details entry is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean entergroundTransportationDetails(String transportName,
			String pickUpCityCountry, String dropOffCityCountry)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			flags.add(isElementPresent(MyTripsPage.addGroundTransportTab,
					"Add Ground Transportation Tab"));
			flags.add(click(MyTripsPage.addGroundTransportTab,
					"Add Ground Transportation Tab"));
			flags.add(isElementPresent(MyTripsPage.transportName,
					"Transport Name"));
			flags.add(type(MyTripsPage.transportName, transportName,
					"Transport Name"));
			flags.add(type(MyTripsPage.pickUpCityCountry, pickUpCityCountry,
					"Pickup City Country"));
			//Thread.sleep(2000);
			flags.add(waitForVisibilityOfElement(MyTripsPage.pickUpCityCountryLoading,
					"pickUpCityCountryLoading"));
			/*Driver.findElement(
					By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlPickupCityCountry_txtSmartTextBoxCityWithCountrty')]"))
					.sendKeys(Keys.ARROW_DOWN);*/
			Driver.findElement(
					By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlPickupCityCountry_txtSmartTextBoxCityWithCountrty')]"))
					.sendKeys(Keys.ENTER);
			flags.add(type(MyTripsPage.dropOffCityCountry, dropOffCityCountry,
					"DropOff City Country"));
			//Thread.sleep(2000);
			flags.add(waitForVisibilityOfElement(MyTripsPage.dropOffCityCountryLoading,
					"dropOffCityCountryLoading"));
			/*Driver.findElement(
					By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlDropoffCityCountry_txtSmartTextBoxCityWithCountrty')]"))
					.sendKeys(Keys.ARROW_DOWN);*/
			Driver.findElement(
					By.xpath(".//input[contains(@id,'MainContent_ucCreateTrip_ucAddGroundTransportSegment_SmartTextControlDropoffCityCountry_txtSmartTextBoxCityWithCountrty')]"))
					.sendKeys(Keys.ENTER);
			flags.add(type(MyTripsPage.pickUpDate, getCurrentDate(),
					"Pickup Date"));
			flags.add(type(MyTripsPage.dropOffDate, getFutureDate(),
					"DropOff Date"));
			flags.add(isElementPresent(MyTripsPage.pickUpHr, "Pickup Hour"));
			flags.add(selectByValue(MyTripsPage.pickUpHr, "22", "Pickup Hour"));
			flags.add(selectByValue(MyTripsPage.pickUpMin, "10", "Pickup Min"));
			flags.add(selectByValue(MyTripsPage.dropOffHr, "12", "Dropoff Hour"));
			flags.add(selectByValue(MyTripsPage.dropOffMin, "10", "Dropoff Min"));
			flags.add(JSClick(MyTripsPage.saveTransport,
					"Save Ground Transportation Button"));

			flags.add(waitForInVisibilityOfElement(MyTripsPage.imageProgress,
					"Loading Image Progress"));
			flags.add(waitForElementPresent(
					MyTripsPage.groundTransportationSuccessMessage,
					"groundTransportationSuccessMessage", 60));
			flags.add(assertElementPresent(
					MyTripsPage.groundTransportationSuccessMessage,
					"Ground Transportation Details successfully saved"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentActualresult.add("Ground Transportation details are entered successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Ground Transportation details entry is failed.");
		}
		return flag;
	}
	

	@SuppressWarnings("unchecked")
	public boolean enterTravelInfoDetails(String abroadPhoneNumber,
			String contactName, String contactRelationship,
			String emergencyPhoneNo, String overseasProgram, String deanName)
			throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();

			/*flags.add(click(MyTripsPage.abroadCountryCode,
					"Abroad Country Code"));*/
			/*flags.add(click(MyTripsPage.abroadCountryCodeValue,
					"Abroad Country Code Value"));
			flags.add(type(MyTripsPage.abroadPhoneNumber, abroadPhoneNumber,
					"Abroad Phone Number"));*/
			flags.add(type(MyTripsPage.contactName, contactName, "Contact Name"));
			/*flags.add(type(MyTripsPage.contactRelationship,
					contactRelationship, "Contact Relationship"));*/
			flags.add(type(MyTripsPage.emergencyPhoneNo, emergencyPhoneNo,
					"Emergency Phone Number"));
			/*flags.add(type(MyTripsPage.overseasProgram, overseasProgram,
					"Overseas Program"));
			flags.add(type(MyTripsPage.deanName, deanName, "Dean Name"));*/
		/*	flags.add(click(MyTripsPage.visitType, "Visit Type"));
			flags.add(click(MyTripsPage.visitTypeValue, "Visit Type Value"));*/
			flags.add(selectByIndex(MyTripsPage.visitType, 2, "Visit Type"));
			flags.add(waitForVisibilityOfElement(MyTripsPage.saveTripInfo,
					"Save Trip Information Button"));
			flags.add(click(MyTripsPage.saveTripInfo,
					"Save Trip Information Button"));
			flags.add(waitForInVisibilityOfElement(MyTripsPage.imageProgress,
					"Loading Image Progress"));
			flags.add(waitForElementPresent(
					MyTripsPage.tripInfoSuccessMessage,
					"tripInfoSuccessMessage", 60));
			flags.add(assertElementPresent(MyTripsPage.tripInfoSuccessMessage,
					"Trip Information is successfully saved"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Travel Information details are entered successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Travel Information details entry.");
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean saveTripInformation() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			flags.add(JSClick(MyTripsPage.saveTripInfo, "saveTripInfo"));
			flags.add(waitForInVisibilityOfElement(MyTripsPage.imageProgress,
					"Loading Image Progress"));
			flags.add(waitForElementPresent(
					MyTripsPage.tripInfoSuccessMessage,
					"tripInfoSuccessMessage", 60));
			flags.add(assertElementPresent(MyTripsPage.tripInfoSuccessMessage,
					"Trip Information is successfully saved"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Saving Trip Information is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Saving Trip Information is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyCreatedTrip() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			flags.add(click(MyTripsPage.myProfileTab, "My Profile Trips Tab"));
			Driver.switchTo().alert().accept();
			flags.add(isElementPresent(MyTripsPage.verifyTripName_myTrips, "Trip Name"));
			String tripNameText = getText(MyTripsPage.verifyTripName_myTrips,
					"Trip Name");
			LOG.info("Created Trips Names : " + tripNameText);
			LOG.info("The trip name is assert ****:"+tripName);
			flags.add(assertTextMatching(MyTripsPage.verifyTripName_myTrips, tripName,
					"Trip Name"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Create Trip verification is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentStartTimer.add(getCurrentTime());
			componentActualresult.add("Create Trip verification is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyNewUserLink() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			flags.add(isElementPresent(MyTripsPage.newUserRegistration,
					"New User Registration Link"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("New User Registration Link verification is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("New User Registration Link verification is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean clickNewUserLink() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			flags.add(click(MyTripsPage.newUserRegistration,
					"New User Registration Link"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("New User Registration Link is clicked successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("New User Registration Link is NOT clicked.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean fillNewUserDetailsAndSave(String firstName, String lastName,
			String email, String pwd, String reenterPwd, String answer1,
			String answer2) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			//flags.add(waitForElementPresent(MyTripsPage.title, "Title", 60));
			flags.add(selectByValue(MyTripsPage.title, "Mr.", "Title"));
			flags.add(type(MyTripsPage.firstName, firstName, "First Name"));
			flags.add(type(MyTripsPage.lastName, lastName, "Last Name"));
			flags.add(type(MyTripsPage.email, email, "Email"));
			flags.add(type(MyTripsPage.pwd, pwd, "Password"));
			flags.add(type(MyTripsPage.reenterPwd, reenterPwd,
					"Reenter Password"));
			flags.add(click(MyTripsPage.securityQstn1, "Security Question 1"));
			flags.add(click(MyTripsPage.securityQstn1Option,
					"Security Question1 option"));
			flags.add(type(MyTripsPage.securityAnswer1, answer1,
					"Security Answer 1"));
			flags.add(selectByIndex(MyTripsPage.securityQstn2, 9,
					"Security Question 2"));
			flags.add(type(MyTripsPage.securityAnswer2, answer2,
					"Security Answer 2"));
			flags.add(click(MyTripsPage.submitButton, "Submit Button"));
			Driver.switchTo().alert().accept();
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("New User details are filled successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Issue with filling New User details .");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyNewUserRegistrationSuccessMsg() throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new MyTripsPage().MyTrips_Page();
			flags.add(assertElementPresent(
					MyTripsPage.myTripsSuccessRegistrationMsg,
					"New User is registered successfully"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("New User Registration Success Message verification is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("New User Registration Success Message verification is failed.");
		}
		return flag;
	}

}
