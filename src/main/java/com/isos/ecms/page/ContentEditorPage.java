package com.isos.ecms.page;

import org.openqa.selenium.By;

public class ContentEditorPage {

	public static By ContentTree;
	public static By PandemicSiteTree;
	public static By PandemicTree;
	public static By HomeTree;
	public static By NewsEditorialsTree;
	public static By NewsTree;
	public static By EditorialTree;
	public static By Inserticon;
	public static By NewsReport;
	public static By newsReportHeadline;
	public static By newsReportSummaryBody;
	public static By newsReportManagerBodyHtml;
	public static By ManagerFrame;
	public static By Acceptbutton;
	public static By Geocode;
	public static By Disease;
	public static By CreatedDate;
	public static By CreatedTime;
	public static By UpdatedDate;
	public static By UpdatedTime;
	public static By SubmitButton;
	public static By Newsroom;
	public static By News;
	public static By PublishedNews;
	public static By managerBodyTextArea;
	public static By RightImage;
	public static By menuIcon;
	public static By exitIcon;
	public static By pageEditorHome;
	public static By pageEditorLogout;
	public static By launchpadLogout;
	public static By userName;
	public static By password;
	public static By loginButton;
	public static By language;
	public static By PandemicLatestNews;
	public static By PandemicMembershipNo;
	public static By PandemicLogIn;
	public static By LatestNewsHeader;
	public static By PandemicMember;
	public static By PandemicLogout;
	public static By MicrositesTree;
	public static By SMicrositeTree;
	public static By HomeIcon;
	public static By HeadlineField;
	public static By publishTab;
	public static By publishIcon;
	public static By publishItem;

	// sampath

	public static By corporateSite;
	public static By corporateTree;
	public static By corporate_homeTree;
	public static By homeLink;
	public static By publishMainMenu;
	public static By publish_pageEditor;
	public static By componentButton;
	public static By addHere_caseDesc;
	public static By userStoriesShowCase;
	public static By Okbtn;
	public static By createNewContent;
	public static By contentName;
	public static By siteModuleBLocksTree;
	public static By contentTitle;
	public static By saveIcon;
	public static By reviewMenu;
	public static By submitMenu;
	public static By approveMenu;

	public static By publishButton;
	public static By finishButton;
	public static By microsite_titleVerify;
	public static By microsite_DescVerify;

	public static By ReportSearch;
	public static By ReportRightImage;
	public static By Search;
	public static By SearchRightImage;

	// Arun
	public static By options;
	public static By desktop;
	public static By desktopPage;
	public static By contentEditorPage;
	public static By pageEditorPage;
	public static By launchPadPage;
	public static By desktopStart;
	public static By logOff;
	public static By pageEditor;
	public static By launchPad;
	public static By allApplicationsMenu;
	public static By newSiteWizard;
	public static By siteName;
	public static By siteTemplate;
	public static By nextButton;
	public static By siteFolder;
	public static By siteTheme;
	public static By siteLoginMechanism;
	public static By siteHostName;
	public static By sitePhysicalFolder;
	public static By siteVirtualFolder;
	public static By siteProgressImage;
	public static By siteFinishBtn;
	public static By siteCoreTree;
	public static By account;
	public static By submit;
	public static By approve;
	public static By forgottenpwdHeader;
	public static By loginHeader;
	public static By pwdupdHeader;
	public static By regHeader;
	public static By regValHeaader;
	public static By resetPwdHeader;
	public static By systemTree;;
	public static By sitesTree;
	public static By headline;
	public static By neverPublish;

	// masood
	public static By SaveButton;

	public static By overviewEditHtml;
	public static By transmissionEditHtml;
	public static By symptomsEditHtml;
	public static By diagnosisEditHtml;
	public static By treatmentEditHtml;
	public static By preventionEditHtml;
	public static By riskToTravellersEditHtml;
	public static By textArea;
	public static By acceptEditHTML;
	public static By MFrame;
	public static By SFrame;
	public static By activeDiseaseSearch;
	public static By activeDiseaseResult;
	public static By activeDiseaseRightArrow;
	public static By Diseases;
	public static By RclickInsert;
	public static By RclickInsertDisease;
	public static By Submit;
	public static By Publish;
	public static By Review;
	public static By ApproveButton;

	// add AlertReport
	public static By RclickInsertNewsReport;
	public static By newReportSearchDisease;
	public static By newReportDiseaseResult;
	public static By newReportDiseaseRightArrow;

	public static By RclickInsertEditorialReport;

	public void ContentEditor_Page()

	{
		ContentTree = By.xpath("//*[@id='Tree_Glyph_0DE95AE441AB4D019EB067441B7C2450']");
		PandemicSiteTree = By.xpath("//*[@id='Tree_Glyph_7BB41307D7F54552AF954DBDF00A60EA']");
		PandemicTree = By.xpath("//*[@id='Tree_Glyph_64F54E90744443669BA0897B2EA5C55F']");
		HomeTree = By.xpath("//*[@id='Tree_Glyph_CDD1DE320BC147F6950DE1622088ABB7']");
		NewsEditorialsTree = By.xpath("//*[@id='Tree_Glyph_480D257775454EB9A92864940739B884']");
		NewsTree = By.xpath("//a[contains(@class,'scContentTreeNode')]//*[text()='News']");
		Inserticon = By.xpath("//table[@class='scMenu lang_en']//*[text()='Insert']");
		NewsReport = By.xpath("(//table[@class='scMenu lang_en']//*[text()='News Report'])[2]");

		newsReportHeadline = By.xpath("//div[text()='Headline:']//following-sibling::div/input");
		newsReportSummaryBody = By.xpath("//div[text()='SummaryBody:']//following-sibling::div/textarea");
		newsReportManagerBodyHtml = By
				.xpath("//div[text()='ManagerBody:']/preceding-sibling::div/a[text()='Edit Html']");
		// ManagerFrame = By.xpath("//*[@id='scContentIframeId0']");
		// ManagerBodyField = By.xpath("//*[@id='ctl00_ctl00_ctl04_Html']");
		managerBodyTextArea = By.xpath("//textarea[contains(@id,'Html')]");
		Acceptbutton = By.xpath("//*[@id='OK']");
		Geocode = By.xpath("//div[text()='Geocode:']//following-sibling::div/input");

		Disease = By.xpath("(//*[@class='scContentControlMultilistBox']/option[2])[3]");
		RightImage = By.xpath("(//img[contains(@id,'btnRightFIELD')])[3]");

		newReportSearchDisease = By.xpath("//div[text()='Disease:']/following-sibling::div//table//input");
		newReportDiseaseResult = By.xpath("//div[text()='Disease:']/following-sibling::div//table//option");
		newReportDiseaseRightArrow = By
				.xpath("//div[text()='Disease:']/following-sibling::div//table//img[contains(@id,'btnRight')]");

		CreatedDate = By
				.xpath("//*[contains(text(),'CreatedDate')]//parent::div//following-sibling::div//table[1]//input");
		CreatedTime = By
				.xpath("//*[contains(text(),'CreatedDate')]//parent::div//following-sibling::div//table[2]//input");
		UpdatedDate = By
				.xpath("//*[contains(text(),'UpdatedDate')]//parent::div//following-sibling::div//table[1]//input");
		UpdatedTime = By
				.xpath("//*[contains(text(),'UpdatedDate')]//parent::div//following-sibling::div//table[2]//input");

		SaveButton = By.xpath("//a[@class='scRibbonToolbarLargeButton']//span[text()='Save']");
		SubmitButton = By.xpath("//a[@class='scRibbonToolbarSmallButton']//span[text()='Submit']");
		Newsroom = By.xpath("//*[@id='nav-container']/nav/ul/li[5]/a");
		News = By.xpath("(//ul[@class='right-align']/li[1]/a)[1]");
		PublishedNews = By.xpath("//ul[@class='scrollable-container']//li[1]//a");

		// Logout
		menuIcon = By.xpath("//*[@id='SystemMenu']");
		exitIcon = By.xpath("//*[text()='Exit']");
		pageEditorHome = By.xpath("//a[@id='Ribbon_Nav_PageEditorStrip']");
		pageEditorLogout = By.xpath("//a[@title='Log off.']");
		launchpadLogout = By.xpath("//a[text()='Logout']");

		// Login
		userName = By.xpath("//*[@id='Login_UserName']");
		password = By.xpath("//*[@id='Login_Password']");
		loginButton = By.xpath("//*[@id='Login_Login']");
		language = By.xpath(".//select[@id='Language']");

		PandemicLatestNews = By.xpath("//*[@class='vert-scroll']//li[1]/a");
		PandemicMembershipNo = By.xpath("//*[@id='site-container']//*[@id='MemberId']");
		PandemicLogIn = By.xpath("//*[@id='site-container']//*[@class='submit']");
		LatestNewsHeader = By.xpath("//*[@id='content-container']//*[@class='meta']//h2");
		PandemicMember = By.xpath("//*[text()='Member']");
		PandemicLogout = By.xpath("//*[@id='login-form-header']//*[text()='Logout']");

		MicrositesTree = By.xpath(".//img[@id='Tree_Glyph_B5B7D28BE5CA41C5A102CDA6274B37A4']");
		SMicrositeTree = By.xpath("//*[@id='Tree_Glyph_5772A649F35D471B8430FE4B933423BE']");
		HomeIcon = By.xpath("//*[@id='Tree_Node_FA7D29C47FB746FEBFBAFA21A3A3EEC4']/span");
		HeadlineField = By.xpath("(//*[text()='Headline:']//parent::div//following-sibling::div//input)[1]");
		publishTab = By.xpath("//a[@id='Ribbon_Nav_PublishStrip']");
		publishIcon = By.xpath(".//a[@id='B414550BADAF4542C9ADF44BED5FA6CB3E_menu_button']/span");
		publishItem = By.xpath(".//div[@id='Popup0']//td[text()='Publish Item']");
		publishButton = By.xpath(".//button[@id='NextButton']");

		// sampath
		corporateSite = By.xpath(".//*[@id='Tree_Glyph_542BEA6EF29D4B6CAE3719C64388664F']");
		corporateTree = By.xpath(".//*[@id='Tree_Glyph_45233090C4E84C57B718AF174D2F3E37']");
		corporate_homeTree = By.xpath(".//*[@id='Tree_Glyph_CECEC97F44E84D1187C4211B8DE78211']");

		homeLink = By.xpath(".//*[@id='Tree_Node_CECEC97F44E84D1187C4211B8DE78211']/span");
		publishMainMenu = By.xpath(".//*[@id='Ribbon_Nav_PublishStrip']");

		publish_pageEditor = By.xpath(".//span[text()='Page Editor']");
		componentButton = By.xpath("//a[@id='scRibbonButton_Insert Component']/img");
		addHere_caseDesc = By.xpath(".//*[@id='home']/div[37]/div[2]/span");
		userStoriesShowCase = By.xpath("//span[text()='UserStoriesShowcase']");
		Okbtn = By.xpath(".//*[@id='OK']");
		createNewContent = By.xpath(".//*[@id='CreateNew']");
		contentName = By.xpath(".//*[@id='NewDatasourceName']");
		siteModuleBLocksTree = By.xpath(".//*[@id='Tree_Glyph_BB5DABD17D5B4F7390DF00E836FD69BE']");
		contentTitle = By.xpath("//*[text()='Title:']/parent::td/div[2]/input");
		saveIcon = By.xpath("//span[text()='Save']");
		reviewMenu = By.xpath(".//*[@id='Ribbon_Nav_ReviewStrip']");
		publishIcon = By
				.xpath(".//*[@title='Publish the item in all languages to all publishing targets.']/parent::div/a[2]");
		publishItem = By.xpath("//*[@class='scPopup']//td[text()='Publish Item']");
		finishButton = By.xpath(".//button[@id='CancelButton']");
		microsite_titleVerify = By.xpath(".//*[@id='site-container']/div[6]/div/div/h1");
		microsite_DescVerify = By.xpath(".//*[@id='site-container']/div[8]/div/div[1]/h1");

		ReportSearch = By.xpath(
				"//*[@class='scEditorSections']/table[2]//*[@class='scContentControlMultilistBox']//*[text()='Events (Taxonomy Keyword - Corporate - 1 - en)']");
		ReportRightImage = By.xpath("//*[@class='scEditorSections']/table[2]//img[contains(@id,'btnRightFIELD')]");
		ApproveButton = By.xpath("//a[@class='scRibbonToolbarSmallButton']//span[text()='Approve']");
		Search = By.xpath(
				"//*[@class='scContentControlMultilistBox']//*[text()='Emerging Safety and Security Issues (Taxonomy Keyword - Corporate - 1 - en)']");
		SearchRightImage = By.xpath("(//img[contains(@id,'btnRightFIELD')])[1]");

		options = By.xpath("//a[text()='Options']");
		desktop = By.xpath(".//button[@id='AdvancedDesktop']");
		desktopStart = By.xpath(".//img[@id='StartButton']");
		desktopPage = By.xpath("//div[@id='Desktop']");
		contentEditorPage = By.xpath("//body[@id='Body']");
		pageEditorPage = By.xpath("//*[@id='Buttons']");
		launchPadPage = By.xpath("//body[@class='sc']");
		logOff = By.xpath(".//div[@id='Popup0']//span[@id='Logout']");
		pageEditor = By.xpath(".//button[@id='AdvancedWebEdit']");
		launchPad = By.xpath(".//button[@id='AdvancedAppLauncher']");
		allApplicationsMenu = By.xpath(".//div[@id='Popup0']//a[@id='StartMenuProgramsItem']");
		newSiteWizard = By.xpath(".//div[@id='Popup2']//td[text()='New Site Wizard']");
		siteName = By.xpath(".//input[@id='SiteName']");
		siteTemplate = By.xpath(".//select[@id='BranchTemplate']");
		nextButton = By.xpath(".//button[@id='NextButton']");
		siteFolder = By.xpath(".//select[@id='SiteCollection']");
		siteTheme = By.xpath(".//select[@id='SiteTheme']");
		siteLoginMechanism = By.xpath(".//select[@id='SiteLogin']");
		siteHostName = By.xpath(".//input[@id='Hostname']");
		sitePhysicalFolder = By.xpath(".//input[@id='PhysicalFolder']");
		siteVirtualFolder = By.xpath(".//input[@id='VirtualFolder']");
		siteFinishBtn = By.xpath(".//button[@id='CancelButton']");
		siteCoreTree = By.xpath("//a[@id='Tree_Node_11111111111111111111111111111111']");
		submit = By.xpath("//span[text()='Submit']");
		approve = By.xpath("//span[text()='Approve']");
		forgottenpwdHeader = By.xpath(".//a[text()='Forgotten Password']");
		loginHeader = By.xpath(".//a[text()='Login']");
		pwdupdHeader = By.xpath(".//a[text()='Password Updated']");
		regHeader = By.xpath(".//a[text()='Register']");
		regValHeaader = By.xpath(".//a[text()='Registration Validation']");
		resetPwdHeader = By.xpath(".//a[text()='Reset Password']");
		systemTree = By.xpath(".//img[@id='Tree_Glyph_13D6D6C6C50B4BBDB3312B04F1A58F21']");
		sitesTree = By.xpath(".//img[@id='Tree_Glyph_67E6AF748A3F4E69B32532887B63A25F']");

		overviewEditHtml = By.xpath("//div[text()='Overview:']/preceding-sibling::div/a[text()='Edit Html']");
		transmissionEditHtml = By.xpath("//div[text()='Transmission:']/preceding-sibling::div/a[text()='Edit Html']");
		symptomsEditHtml = By.xpath("//div[text()='Symptoms:']/preceding-sibling::div/a[text()='Edit Html']");
		diagnosisEditHtml = By.xpath("//div[text()='Diagnosis:']/preceding-sibling::div/a[text()='Edit Html']");
		treatmentEditHtml = By.xpath("//div[text()='Treatment:']/preceding-sibling::div/a[text()='Edit Html']");
		preventionEditHtml = By.xpath("//div[text()='Prevention:']/preceding-sibling::div/a[text()='Edit Html']");
		riskToTravellersEditHtml = By
				.xpath("//div[text()='RiskToTravellers:']/preceding-sibling::div/a[text()='Edit Html']");
		textArea = By.xpath("//textarea[contains(@id,'Html')]");
		acceptEditHTML = By.id("OK");
		MFrame = By.id("jqueryModalDialogsFrame");
		SFrame = By.id("scContentIframeId0");
		activeDiseaseSearch = By.xpath("//div[text()='Diseases:']/following-sibling::div//table//input");
		activeDiseaseResult = By.xpath("//div[text()='Diseases:']/following-sibling::div//table//td[1]//option");
		activeDiseaseRightArrow = By
				.xpath("//div[text()='Diseases:']/following-sibling::div//table//img[contains(@id,'btnRight')]");
		headline = By.xpath("//div[@id='EditorPanel']/div[3]/table[1]//table[1]//td[2]/div[2]/input");
		neverPublish = By.xpath(".//div[contains(text(),'Never publish')]/following-sibling::div/input");

		Diseases = By.xpath("//a[@id='Tree_Node_C30B6C2671DC4A38B4A2F9D562714742']/span");
		RclickInsert = By.xpath("//td[text()='Insert']");
		RclickInsertDisease = By.xpath("(.//td[text()='Disease'])[2]");
		Submit = By.xpath("//a[@title='Submit']");
		Publish = By.xpath("//a[@title='Publish']");
		Review = By.id("Ribbon_Nav_ReviewStrip");
		RclickInsertNewsReport = By.xpath("//div[@id='Popup2']//td[text()='News Report']");

		// Editorial
		EditorialTree = By.xpath("//a[contains(@class,'scContentTreeNode')]//*[text()='Editorials']");
		RclickInsertEditorialReport = By.xpath("//div[@id='Popup2']//td[text()='Editorial Report']");

	}

}
