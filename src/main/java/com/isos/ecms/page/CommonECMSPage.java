package com.isos.ecms.page;

import org.openqa.selenium.By;

public class CommonECMSPage {
	/**
	 * 
	 */

	public static By PandemicMembershipNo;
	public static By PandemicLogIn;
	public static By PandemicMemberLogin;
	public static By PandemicMembershipNoInit;
	public static By PandemicLoginInit;
	public static By pandemicLoginHeader;
	
	public void CommonECMS_Page() {

		
		PandemicMembershipNo = By.xpath("//*[@id='site-container']//*[@id='MemberId']");
		PandemicLogIn = By.xpath("//*[@id='site-container']//*[@class='submit']");
		PandemicMemberLogin = By.xpath("//div[@id='utility-container']//a[text()='Member Log In']");
		PandemicMembershipNoInit = By.xpath("//span[@id='login-form-header']/input[@id='MemberId']");
		PandemicLoginInit = By.xpath("//*[@id='login-form-header']/input[@value = 'Log In']");
		pandemicLoginHeader = By.xpath("//div[@id='main']/div/h2[text()='Login']");
		
		
	}

}
