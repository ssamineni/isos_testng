package com.isos.ecms.libs;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.isos.ecms.page.CommonECMSPage;
import com.isos.ecms.page.ContentEditorPage;
import com.isos.ecms.page.DeliveryPandemicPage;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.TravelTrackerHomePage;
import com.sample.TestCaseCreationUtility.GetLatestCodeFromBitBucket;

public class CommonECMSLib extends CommonLib{
	
	private final Logger LOG = Logger.getLogger(CommonECMSLib.class);
	static String methodName;

	//	public static List componentNamesList = new ArrayList();
	/*public static List componentStartTimer = new ArrayList();
	public static List componentEndTimer = new ArrayList();
	public static List componentActualresult = new ArrayList();*/
	
	
	public static String getCurrentTime() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		return sdf.format(date);
	}
	
	@SuppressWarnings("unchecked")
	public boolean openBrowserECMS(String url, String title) throws Exception{
		boolean flag = true;
		
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(CommonLib.getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			
			if (url.contains("ecms-qa-pandemic"))
				openBrowser("http://ecms-qa-pandemic.intlsos.com/");
			else if (url.contains("ecms-qa-delivery"))
				openBrowser("http://ecms-qa-delivery.intlsos.com/");
			else if (url.contains("ecms-qa-site.intlsos"))
				openBrowser("http://ecms-qa-site.intlsos.com/"+title);
			else 
				openBrowser("http://ecms-qa.intlsos.com/sitecore/login");
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("User logged in successfully.");
		} catch (IOException e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add("User login failed.");
			componentEndTimer.add(getCurrentTime());
		}
		
		return flag;		
	}
	
	public boolean openBrowser(String url) throws IOException,
	InterruptedException {
		boolean flag = true;
		try {
			driverInitiation(url);			
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();			
		}
		return flag;
	}

	@SuppressWarnings({ "unchecked", "unchecked" })
	public boolean loginToPandemic(String userName, String loginType) throws Throwable {
		boolean flag = true;
		try {

			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new CommonECMSPage().CommonECMS_Page();
			
			if (loginType.equals("IntermediateLogin")){
				flags.add(type(CommonECMSPage.PandemicMembershipNo,userName, "User Name"));
				flags.add(click(CommonECMSPage.PandemicLogIn, "Login Button"));
				flags.add(waitForInVisibilityOfElement(CommonECMSPage.pandemicLoginHeader, "Login Header"));				
			}
			else if(loginType.equals("InitialLogin")){
				flags.add(type(CommonECMSPage.PandemicMembershipNo,userName, "User Name"));
				flags.add(click(CommonECMSPage.PandemicLogIn, "Login Button"));
				flags.add(waitForInVisibilityOfElement(CommonECMSPage.PandemicLogIn, "Login Button"));
			}
			
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("User logged in successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add("User login failed.");
			componentEndTimer.add(getCurrentTime());
		}
		return flag;
	}
	
	public boolean loginToECMS(String userName, String pwd, String loginType, String language) {

		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			
			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			flags.add(type(ContentEditorPage.userName, userName, "User Name"));
			flags.add(type(ContentEditorPage.password, GetLatestCodeFromBitBucket.decrypt_text(pwd), "Password"));

			switch(loginType) {
			
			case "Content Editor": flags.add(click(ContentEditorPage.options, "options"));
								   break;
								   
			case "Desktop":       flags.add(click(ContentEditorPage.options, "options"));
								  flags.add(waitForVisibilityOfElement(ContentEditorPage.desktop, "desktop"));
								  flags.add(click(ContentEditorPage.desktop, "desktop"));
								  break;
								  
			case "Page Editor":   flags.add(click(ContentEditorPage.options, "options"));
								  flags.add(waitForVisibilityOfElement(ContentEditorPage.pageEditor, "pageEditor"));
								  flags.add(click(ContentEditorPage.pageEditor, "pageEditor"));
								  break;
								  
			case "launchPad":     flags.add(click(ContentEditorPage.options, "options"));
								  flags.add(click(ContentEditorPage.launchPad, "launchPad"));
								  break;
			}
			
			flags.add(selectByVisibleText(ContentEditorPage.language, language, "language"));
			flags.add(click(ContentEditorPage.loginButton, "Login Button"));
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Login is Successful.");
			
		} catch (Throwable e) {
			flag = false;
			componentEndTimer.add(getCurrentTime());
			e.printStackTrace();
			componentActualresult.add("Login is Failed.");

		}

		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean logoutECMS() throws Throwable {
		boolean flag = true;
		try {

			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			String pageName = typeOfPage();
			LOG.info("Page name is: "+pageName);
			if(pageName == "Desktop") {
				flags.add(waitForVisibilityOfElement(ContentEditorPage.desktopStart, "desktopStart"));
				flags.add(click(ContentEditorPage.desktopStart, "desktopStart"));
				flags.add(waitForVisibilityOfElement(ContentEditorPage.logOff, "Logoff"));
				flags.add(click(ContentEditorPage.logOff, "Logoff"));
			}
			else if(pageName == "Content Editor"){
				flags.add(waitForVisibilityOfElement(ContentEditorPage.menuIcon, "Menuicon"));
				flags.add(click(ContentEditorPage.menuIcon, "Menuicon"));
				flags.add(waitForVisibilityOfElement(ContentEditorPage.exitIcon, "exitIcon"));
				flags.add(click(ContentEditorPage.exitIcon, "exitIcon"));
			}
			else if(pageName == "Page Editor"){
				Driver.switchTo().frame("scWebEditRibbon");
				flags.add(click(ContentEditorPage.pageEditorHome, "pageEditorHome"));
				flags.add(waitForVisibilityOfElement(ContentEditorPage.pageEditorLogout, "pageEditorLogout"));
				flags.add(click(ContentEditorPage.pageEditorLogout, "pageEditorLogout"));
			}
			else if(pageName == "Page Editor"){
				flags.add(click(ContentEditorPage.launchpadLogout, "launchpadLogout"));
			}

			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("User logged out successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentActualresult.add("User logout failed.");
			componentEndTimer.add(getCurrentTime());
		}
		return flag;
	}
	
	public String typeOfPage() throws Throwable {
		
		new ContentEditorPage().ContentEditor_Page();
		if(assertElementPresent(ContentEditorPage.desktopPage, "desktopPage"))
			return "Desktop";
		else if(assertElementPresent(ContentEditorPage.contentEditorPage, "contentEditorPage"))
			return "Content Editor";
		else if(assertElementPresent(ContentEditorPage.pageEditorPage, "pageEditorPage"))
			return "Page Editor";
		else if(assertElementPresent(ContentEditorPage.launchPadPage, "launchPadPage"))
			return "Launchpad";
		else
			return null;
	}
	
	@SuppressWarnings("unchecked")
	public boolean closeBrowserECMS() throws Exception {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());
			Driver.close();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Browser Closed successfully.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Browser failed to Close.");
			
		}		
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public static void setMethodName(String MethodName) throws IOException {
		methodName = MethodName;
		//	componentNamesList.add(MethodName);
	}

	public static String getMethodname() throws IOException {
		return methodName;

	}
	
	public String getBrowser() {
		return this.browser;
	}
	
	public By mergeLocator(String locator1, String treeValue, String locator2) {
		By actualLocator = null;

		String actualLocator1 = locator1 + treeValue + locator2;
		actualLocator = By.xpath(actualLocator1);

		return actualLocator;

	}

	public boolean switchToFrame(By by, String locator) throws IOException{
		boolean status=false;
		try{
			WebElement Frame = Driver.findElement(by);
			Driver.switchTo().frame(Frame);
			status = true;
			LOG.info("Switched to : "+ locator);
			LOG.info("Switched to : " + locator);
		}
		catch(Exception e){
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot(locator);
		}
		return status;
	}
	
	public boolean switchToDefaultFrame() throws IOException{
		boolean status=false;
		try{			
			Driver.switchTo().defaultContent();
			status = true;
			LOG.info("Switched to Default Frame");
			LOG.info("Switched to Default Frame");
		}
		catch(Exception e){
			LOG.info(e.getMessage());
			e.printStackTrace();
			takeScreenshot("Switch to Default Frame");
		}
		return status;
	}
		
}
