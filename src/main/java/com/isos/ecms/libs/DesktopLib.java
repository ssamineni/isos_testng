package com.isos.ecms.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.isos.ecms.page.ContentEditorPage;
import com.isos.tt.page.TravelTrackerHomePage;
import com.sample.TestCaseCreationUtility.GetLatestCodeFromBitBucket;

public class DesktopLib extends CommonECMSLib{

	@SuppressWarnings("unchecked")
	public boolean createNewSiteWizard(String siteName, String siteHostName, String sitePhysicalFolder, String siteVirtualFolder) throws Throwable {
		boolean flag = true;
		
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			
			flags.add(click(ContentEditorPage.desktopStart, "desktopStart"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.allApplicationsMenu,
					"allApplicationsMenu"));
			flags.add(click(ContentEditorPage.allApplicationsMenu,
					"allApplicationsMenu"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.newSiteWizard, "newSiteWizard"));
			flags.add(click(ContentEditorPage.newSiteWizard, "newSiteWizard"));
				
			Driver.switchTo().frame(Driver.findElement(By.xpath("//iframe[contains(@id,'FRAME')]")));
			Driver.switchTo().frame("AppFrame");
			flags.add(waitForVisibilityOfElement(ContentEditorPage.siteName,
					"siteName"));
			flags.add(type(ContentEditorPage.siteName, siteName, "siteName"));
			flags.add(selectByIndex(ContentEditorPage.siteTemplate, 1, "siteName"));
			flags.add(click(ContentEditorPage.nextButton, "nextButton"));
			flags.add(click(ContentEditorPage.nextButton, "nextButton"));
			flags.add(selectByIndex(ContentEditorPage.siteFolder, 3, "siteFolder"));
			flags.add(selectByIndex(ContentEditorPage.siteLoginMechanism, 2, "siteLoginMechanism"));
			flags.add(click(ContentEditorPage.nextButton, "nextButton"));
			flags.add(type(ContentEditorPage.siteHostName, siteHostName, "siteHostName"));
			flags.add(type(ContentEditorPage.sitePhysicalFolder, sitePhysicalFolder, "sitePhysicalFolder"));
			flags.add(type(ContentEditorPage.siteVirtualFolder, siteVirtualFolder, "siteVirtualFolder"));
			flags.add(click(ContentEditorPage.nextButton, "nextButton"));
			flags.add(click(ContentEditorPage.nextButton, "nextButton"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.siteFinishBtn, "siteFinishBtn"));
			flags.add(click(ContentEditorPage.siteFinishBtn, "siteFinishBtn"));
			Driver.switchTo().alert().accept();
			Driver.switchTo().defaultContent();
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			
			componentActualresult.add("Creation of New Site Wizard is Successful.");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Creation of New Site Wizard is Failed.");
		}

		return flag;
	}


}
