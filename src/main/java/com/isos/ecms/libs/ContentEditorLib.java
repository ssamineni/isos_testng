package com.isos.ecms.libs;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.automation.CSVUility.CsvHandler;
import com.automation.accelerators.ActionEngine;
import com.automation.accelerators.TestEngineWeb;
import com.isos.ecms.page.CommonECMSPage;
import com.isos.ecms.page.ContentEditorPage;
import com.isos.tt.libs.CommonLib;
import com.isos.tt.page.TravelTrackerHomePage;
import com.sample.TestCaseCreationUtility.GetLatestCodeFromBitBucket;

public class ContentEditorLib extends CommonECMSLib{

	private static final Logger LOG = Logger.getLogger(ContentEditorLib.class);

	@SuppressWarnings("unchecked")
	public boolean approveSitePages(String siteName) throws Throwable {
		boolean flag = true;
		
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			
			LOG.info("Site name: "+siteName);
			flags.add(click(ContentEditorPage.siteCoreTree, "siteCoreTree"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.ContentTree,
					"ContentTree"));
			flags.add(click(ContentEditorPage.ContentTree,
					"ContentTree"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.MicrositesTree,
					"MicrositesTree"));
			flags.add(click(ContentEditorPage.MicrositesTree,
					"MicrositesTree"));
			Driver.findElement(By.xpath(".//span[text()='"+siteName+"']/../preceding-sibling::img")).click();
			Driver.findElement(By.xpath(".//span[text()='Home']/../preceding-sibling::img")).click();
			Driver.findElement(By.xpath(".//span[text()='"+siteName+"']/../following-sibling::div//span[text()='account']/../preceding-sibling::img")).click();
			Driver.findElement(By.xpath(".//span[text()='"+siteName+"']/../following-sibling::div//span[text()='account']/../following-sibling::div//span[text()='Forgotten Password']")).click();
			
			flags.add(waitForVisibilityOfElement(ContentEditorPage.reviewMenu,
					"reviewMenu"));
			flags.add(click(ContentEditorPage.reviewMenu,
					"reviewMenu"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.submit,
					"submit"));
			flags.add(click(ContentEditorPage.submit,
					"submit"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.approve,
					"approve"));
			flags.add(click(ContentEditorPage.approve,
					"approve"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.forgottenpwdHeader,
					"forgottenpwdHeader"));
			Driver.findElement(By.xpath(".//span[text()='"+siteName+"']/../following-sibling::div//span[text()='account']/../following-sibling::div//span[text()='Login']")).click();
			flags.add(waitForVisibilityOfElement(ContentEditorPage.submit,
					"submit"));
			flags.add(click(ContentEditorPage.submit,
					"submit"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.approve,
					"approve"));
			flags.add(click(ContentEditorPage.approve,
					"approve"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.loginHeader,
					"loginHeader"));
			Driver.findElement(By.xpath(".//span[text()='"+siteName+"']/../following-sibling::div//span[text()='account']/../following-sibling::div//span[text()='Password Updated']")).click();
			flags.add(waitForVisibilityOfElement(ContentEditorPage.submit,
					"submit"));
			flags.add(click(ContentEditorPage.submit,
					"submit"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.approve,
					"approve"));
			flags.add(click(ContentEditorPage.approve,
					"approve"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.pwdupdHeader,
					"pwdupdHeader"));
			Driver.findElement(By.xpath(".//span[text()='"+siteName+"']/../following-sibling::div//span[text()='account']/../following-sibling::div//span[text()='Register']")).click();
			flags.add(waitForVisibilityOfElement(ContentEditorPage.submit,
					"submit"));
			flags.add(click(ContentEditorPage.submit,
					"submit"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.approve,
					"approve"));
			flags.add(click(ContentEditorPage.approve,
					"approve"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.regHeader,
					"regHeader"));
			Driver.findElement(By.xpath(".//span[text()='"+siteName+"']/../following-sibling::div//span[text()='account']/../following-sibling::div//span[text()='Registration Validation']")).click();
			flags.add(waitForVisibilityOfElement(ContentEditorPage.submit,
					"submit"));
			flags.add(click(ContentEditorPage.submit,
					"submit"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.approve,
					"approve"));
			flags.add(click(ContentEditorPage.approve,
					"approve"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.regValHeaader,
					"regValHeaader"));
			Driver.findElement(By.xpath(".//span[text()='"+siteName+"']/../following-sibling::div//span[text()='account']/../following-sibling::div//span[text()='Reset Password']")).click();
			flags.add(waitForVisibilityOfElement(ContentEditorPage.submit,
					"submit"));
			flags.add(click(ContentEditorPage.submit,
					"submit"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.approve,
					"approve"));
			flags.add(click(ContentEditorPage.approve,
					"approve"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.resetPwdHeader,
					"resetPwdHeader"));
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			
			componentActualresult.add("Creation of New Site Wizard is Successful.");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Creation of New Site Wizard is Failed.");
		}

		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean publishItemContent() throws Throwable {
		boolean flag = true;
		
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			
			Driver.switchTo().frame("jqueryModalDialogsFrame");
			Driver.switchTo().frame("scContentIframeId0");
			
			flags.add(waitForVisibilityOfElement(ContentEditorPage.publishButton,"publishButton"));
			/*
			WebElement we = Driver.findElement(By.xpath(".//button[@id='NextButton']"));
			JavascriptExecutor jse = (JavascriptExecutor)Driver;
			jse.executeScript("arguments[0].scrollIntoView()", we);
			
			Actions actions = new Actions(Driver);
			actions.moveToElement(Driver.findElement(By.xpath(".//button[@id='NextButton']"))).click().perform();
			*/
			
			flags.add(click(ContentEditorPage.publishButton,"publishButton"));
			
			flags.add(waitForVisibilityOfElement(ContentEditorPage.finishButton,"finishButton"));
			/*
			WebElement we2 = Driver.findElement(By.xpath(".//button[@id='CancelButton']"));
			JavascriptExecutor jse2 = (JavascriptExecutor)Driver;
			jse2.executeScript("arguments[0].scrollIntoView()", we2);
			
			Actions actions2 = new Actions(Driver);
			actions2.moveToElement(Driver.findElement(By.xpath(".//button[@id='CancelButton']"))).click().perform();
			*/
			
			flags.add(click(ContentEditorPage.finishButton,"finishButton"));
			Driver.switchTo().defaultContent();
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			
			componentActualresult.add("Creation of New Site Wizard is Successful.");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Creation of New Site Wizard is Failed.");
		}

		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean verifyCreatedSite(String siteName) throws Throwable {
		boolean flag = true;
		
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			
			flags.add(waitForVisibilityOfElement(ContentEditorPage.siteCoreTree,
					"siteCoreTree"));
			flags.add(click(ContentEditorPage.siteCoreTree,
					"siteCoreTree"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.systemTree, "systemTree"));
			flags.add(click(ContentEditorPage.systemTree, "systemTree"));
			flags.add(waitForVisibilityOfElement(ContentEditorPage.sitesTree,
					"sitesTree"));
			flags.add(click(ContentEditorPage.sitesTree,
					"sitesTree"));
			if(Driver.findElement(By.xpath(".//a[@id='Tree_Node_67E6AF748A3F4E69B32532887B63A25F']/following-sibling::div//span[contains(text(),'"+siteName+"')]")).isDisplayed())
					componentActualresult.add("New Site is saved Successfully.");
			else
					componentActualresult.add("New Site is	NOT saved .");
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("New Site is	NOT saved .");
		}

		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean selectRibbonMenu(String item, String subItem, String subsubItem) throws Throwable {
		boolean flag = true;
		
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			
			LOG.info("item : "+item);
			LOG.info("subItem : "+subItem);
			LOG.info("subsubItem : "+subsubItem);
			
			String item_xpath = ".//a[text()='"+item+"']";
			String subItem_xpath = "//span[text()='"+subItem+"']/..";
			String subsubItem_xpath = "//div[@id='Popup0']//td[contains(text(),'"+subsubItem+"')]";
			
			if(subsubItem == "") {
				Driver.findElement(By.xpath(".//a[text()='"+item+"']")).click();
				Driver.findElement(By.xpath("//span[text()='"+subItem+"']/..")).click();
			}
			else
				Driver.findElement(By.xpath(".//a[text()='"+item+"']")).click();
				Driver.findElement(By.xpath("//span[text()='"+subItem+"']/..")).click();
				Driver.findElement(By.xpath("//div[@id='Popup0']//td[contains(text(),'"+subsubItem+"')]")).click();
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			
			componentActualresult.add("Creation of New Site Wizard is Successful.");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Creation of New Site Wizard is Failed.");
		}

		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean selectFolderFromTreeContent(String folderPath) throws Throwable {
		boolean flag = true;
		try {

			LOG.info("start of the method");
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();

			// By
			// expand_structure1=By.xpath("//span[text()='%s']/../../img[contains(@src,'expand')]");

			String  item_structure1 = "//span[text()='";
			String item_structure2 = "']/..";

			String expand_structure1 = "//span[text()='";
			String expand_structure2 = "']/../../img[contains(@src,'expand')]";

			String collapse_structure1 = "//span[text()='";
			String collapse_structure2 = "']/../../img[contains(@src,'collapse')]";

			String document_structure1 = "//span[text()='";
			String document_structure2 = "']/../../img[contains(@src,'')]";
			//By document_structure2 = By.xpath("']/../../img[contains(@src,'')]");
			String[] folder = folderPath.split("/");
			LOG.info(folder[0]);

			flags.add(click(mergeLocator(item_structure1, folder[0], item_structure2), "Item Structure"));
			flags.add(click(mergeLocator(collapse_structure1, folder[0], collapse_structure2), "Item Structure"));

			for (int i = 0; i < folder.length; i++) {
				flags.add(waitForElementPresent(mergeLocator(item_structure1, folder[i], item_structure2),
						"Item Structure", 20));
				flags.add(click(mergeLocator(expand_structure1, folder[i], expand_structure2), "Item Structure"));
			}

			flags.add(
					click(mergeLocator(item_structure1, folder[folder.length - 1], item_structure2), "Item Structure"));

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Click on Latest News is successful.");
			
			LOG.info("End of the method");
			
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Latest News is failed.");
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean addDiseasePandemic(String diseaseName, String diseaseComment) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();

			flags.add(waitForElementPresent(ContentEditorPage.Diseases, "Diseases", 10));
			
			WebElement ele = Driver.findElement(ContentEditorPage.Diseases);
			flags.add(rightclick(ele, "Diseases"));

			flags.add(waitForElementPresent(ContentEditorPage.RclickInsert, "Insert", 10));
			flags.add(click(ContentEditorPage.RclickInsert, "Insert"));

			flags.add(waitForElementPresent(ContentEditorPage.RclickInsertDisease, "Insert Disease", 10));
			flags.add(click(ContentEditorPage.RclickInsertDisease, "Insert Disease"));

			flags.add(waitForAlertToPresent());
			flags.add(enterTextIntoAlert(diseaseName));

			flags.add(waitForElementPresent(ContentEditorPage.Review, "Review", 10));
			flags.add(click(ContentEditorPage.Review, "Review"));

			flags.add(waitForElementPresent(ContentEditorPage.Submit, "Submit", 10));
			flags.add(click(ContentEditorPage.Submit, "Submit"));

			flags.add(waitForElementPresent(ContentEditorPage.ApproveButton, "ApproveButton", 10));
			flags.add(click(ContentEditorPage.ApproveButton, "ApproveButton"));
			
			/*flags.add(waitForElementPresent(ContentEditorPage.Publish, "Publish", 10));
			flags.add(click(ContentEditorPage.Publish, "Publish"));*/

			/*flags.add(waitForAlertToPresent());
			flags.add(enterTextIntoAlert(diseaseComment));*/

			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Click on Latest News is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Click on Latest News is failed.");
		}
		return flag;
	}
 
	@SuppressWarnings("unchecked")
	public boolean fillDiseaseDetails(String overviewText, String transmissionText, String symptomsText, String diagnosisText,
			String treatmentText, String preventionText, String riskToTravellersText) throws Throwable {
		boolean flag = true;

		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			
			flags.add(waitForElementPresent(ContentEditorPage.overviewEditHtml, "overview_EditHtml ", 10));
			flags.add(click(ContentEditorPage.overviewEditHtml, "overview _EditHtml"));
			
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			
			/*WebElement Frame1 = Driver.findElement(ContentEditorPage.MFrame);
			Driver.switchTo().frame(Frame1);
			WebElement Frame2 = Driver.findElement(ContentEditorPage.SFrame);
			Driver.switchTo().frame(Frame2);*/
			
			flags.add(waitForElementPresent(ContentEditorPage.textArea, "text area", 3));
			flags.add(type(ContentEditorPage.textArea, overviewText,"Overview Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML, "Accept Button", 3));
			flags.add(click(ContentEditorPage.acceptEditHTML, "Accept Button"));
			
			flags.add(switchToDefaultFrame());
			//Driver.switchTo().defaultContent();
			
			//********************
			flags.add(waitForElementPresent(ContentEditorPage.transmissionEditHtml, "Transmission Edit Html", 10));
			flags.add(click(ContentEditorPage.transmissionEditHtml, "transmission Edit Html"));
			
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
						
			flags.add(waitForElementPresent(ContentEditorPage.textArea, "text area", 3));
			flags.add(type(ContentEditorPage.textArea, transmissionText,"Overview Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML, "Accept Button", 3));
			flags.add(click(ContentEditorPage.acceptEditHTML, "Accept Button"));
			flags.add(switchToDefaultFrame());
			
			//********************
			flags.add(waitForElementPresent(ContentEditorPage.symptomsEditHtml, "Symptoms Edit Html", 10));
			flags.add(click(ContentEditorPage.symptomsEditHtml, "Symptoms Edit Html"));
			
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			
			flags.add(waitForElementPresent(ContentEditorPage.textArea, "text area", 3));
			flags.add(type(ContentEditorPage.textArea, symptomsText,"Overview Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML, "Accept Button", 3));
			flags.add(click(ContentEditorPage.acceptEditHTML, "Accept Button"));
			flags.add(switchToDefaultFrame());
			
			//********************
			flags.add(waitForElementPresent(ContentEditorPage.diagnosisEditHtml, "Diagnosis Edit Html", 10));
			flags.add(click(ContentEditorPage.diagnosisEditHtml, "Diagnosis Edit Html"));
			
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			
			flags.add(waitForElementPresent(ContentEditorPage.textArea, "text area", 3));
			flags.add(type(ContentEditorPage.textArea, diagnosisText,"Overview Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML, "Accept Button", 3));
			flags.add(click(ContentEditorPage.acceptEditHTML, "Accept Button"));
			flags.add(switchToDefaultFrame());
			
			//********************
			flags.add(waitForElementPresent(ContentEditorPage.treatmentEditHtml, "Treatment Edit Html", 10));
			flags.add(click(ContentEditorPage.treatmentEditHtml, "Treatment Edit Html"));
			
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			
			flags.add(waitForElementPresent(ContentEditorPage.textArea, "text area", 3));
			flags.add(type(ContentEditorPage.textArea, treatmentText,"Overview Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML, "Accept Button", 3));
			flags.add(click(ContentEditorPage.acceptEditHTML, "Accept Button"));
			flags.add(switchToDefaultFrame());
			
			//********************
			flags.add(waitForElementPresent(ContentEditorPage.preventionEditHtml, "Prevention Edit Html", 10));
			flags.add(click(ContentEditorPage.preventionEditHtml, "Prevention Edit Html"));
			
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			
			flags.add(waitForElementPresent(ContentEditorPage.textArea, "text area", 3));
			flags.add(type(ContentEditorPage.textArea, preventionText,"Overview Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML, "Accept Button", 3));
			flags.add(click(ContentEditorPage.acceptEditHTML, "Accept Button"));
			flags.add(switchToDefaultFrame());
			
			//********************
			flags.add(waitForElementPresent(ContentEditorPage.riskToTravellersEditHtml, "RiskToTravellers Edit Html", 10));
			flags.add(click(ContentEditorPage.riskToTravellersEditHtml, "RiskToTravellers Edit Html"));
			
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			
			flags.add(waitForElementPresent(ContentEditorPage.textArea, "text area", 3));
			flags.add(type(ContentEditorPage.textArea, riskToTravellersText,"Overview Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML, "Accept Button", 3));
			flags.add(click(ContentEditorPage.acceptEditHTML, "Accept Button"));
			flags.add(switchToDefaultFrame());
			
			//********************
			
			flags.add(waitForElementPresent(ContentEditorPage.SaveButton, "Save Button", 5));
			flags.add(click(ContentEditorPage.SaveButton, "Save Button"));
			flags.add(accecptTheAlert());
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentActualresult.add("Creation of New Site Wizard is Successful .");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Creation of New Site Wizard is Failed .");
		}

		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean searchingAndSelectingDisease(String Disease) throws Throwable {
		boolean flag = true;

		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			flags.add(waitForElementPresent(ContentEditorPage.activeDiseaseSearch, "Disease Name", 10));
			flags.add(type(ContentEditorPage.activeDiseaseSearch, Disease, "Disease Name"));
			flags.add(waitForElementPresent(ContentEditorPage.activeDiseaseResult, "Disease Result", 10));
			
			Boolean DiseaseResult = verify(getText(ContentEditorPage.activeDiseaseResult, "Disease Result"),Disease,"Disease");
			if (! DiseaseResult){
				Thread.sleep(1000);
				
				flags.add(type(ContentEditorPage.activeDiseaseSearch, "", "Disease Name"));
				flags.add(waitForElementPresent(ContentEditorPage.activeDiseaseResult, "Disease Result", 10));
				
				flags.add(type(ContentEditorPage.activeDiseaseSearch, Disease, "Disease Name"));
				flags.add(waitForElementPresent(ContentEditorPage.activeDiseaseResult, "Disease Result", 10));				
			}
			
			flags.add(click(ContentEditorPage.activeDiseaseResult, "Disease Result"));
			flags.add(verify(getText(ContentEditorPage.activeDiseaseResult, "Disease Result"),Disease,"Disease"));
			flags.add(waitForElementPresent(ContentEditorPage.activeDiseaseRightArrow, "Right Arrow", 10));
			flags.add(click(ContentEditorPage.activeDiseaseRightArrow,"Move to Right pane"));
			flags.add(waitForElementPresent(ContentEditorPage.SaveButton, "Save Button", 10));
			flags.add(click(ContentEditorPage.SaveButton,"Save Button"));
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentActualresult.add("Creation of New Site Wizard is  Successful.");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Creation of New Site Wizard is Failed.");
		}

		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean createNewsReport(String ReportName) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();

			flags.add(waitForElementPresent(ContentEditorPage.NewsTree, "News Report", 10));
			
			WebElement ele = Driver.findElement(ContentEditorPage.NewsTree);
			flags.add(rightclick(ele, "NewsReport"));

			flags.add(waitForElementPresent(ContentEditorPage.RclickInsert, "Insert", 10));
			flags.add(click(ContentEditorPage.RclickInsert, "Insert"));

			flags.add(waitForElementPresent(ContentEditorPage.RclickInsertNewsReport, "Insert News", 10));
			flags.add(click(ContentEditorPage.RclickInsertNewsReport, "Insert News"));

			flags.add(waitForAlertToPresent());
			flags.add(enterTextIntoAlert(ReportName));

			flags.add(waitForElementPresent(ContentEditorPage.Review, "Review", 10));
			flags.add(click(ContentEditorPage.Review, "Review"));

			flags.add(waitForElementPresent(ContentEditorPage.Submit, "Submit", 10));
			flags.add(click(ContentEditorPage.Submit, "Submit"));

			flags.add(waitForElementPresent(ContentEditorPage.ApproveButton, "ApproveButton", 10));
			flags.add(click(ContentEditorPage.ApproveButton, "ApproveButton"));
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("News Report creation is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("News Report creation is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean fillNewsReportDetails(String headline_text, String summary_text, String manager_body_text, String geocode_text,
			String search_disease_text) throws Throwable {
		boolean flag = true;

		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			
			//********************
			
			flags.add(waitForElementPresent(ContentEditorPage.newsReportHeadline, "News Report headline", 10));
			flags.add(type(ContentEditorPage.newsReportHeadline, headline_text,"News Report headline"));
			
			flags.add(waitForElementPresent(ContentEditorPage.newsReportSummaryBody, "News Report Summary Body", 10));
			flags.add(type(ContentEditorPage.newsReportSummaryBody, summary_text, "News Report Summary Body"));
			
			flags.add(waitForElementPresent(ContentEditorPage.newsReportManagerBodyHtml, "Manager_EditHtml ", 10));
			flags.add(click(ContentEditorPage.newsReportManagerBodyHtml, "Manager _EditHtml"));
			
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			
			flags.add(waitForElementPresent(ContentEditorPage.managerBodyTextArea, "Manager body text area", 3));
			flags.add(type(ContentEditorPage.managerBodyTextArea, manager_body_text,"Manager body Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML, "Accept Button", 3));
			flags.add(click(ContentEditorPage.acceptEditHTML, "Accept Button"));
			
			flags.add(switchToDefaultFrame());			
			
			flags.add(waitForElementPresent(ContentEditorPage.Geocode, "Geo Code", 10));
			flags.add(type(ContentEditorPage.Geocode, geocode_text,"Geo Code"));
			
			flags.add(waitForElementPresent(ContentEditorPage.newReportSearchDisease, "Disease search", 10));
			flags.add(type(ContentEditorPage.newReportSearchDisease, search_disease_text,"Disease search"));
			
			flags.add(waitForElementPresent(ContentEditorPage.newReportDiseaseResult, "Disease Result", 10));
			
			if(! verify(getText(ContentEditorPage.newReportDiseaseResult,"Disease Result"), search_disease_text, "Disease Result")){
				Thread.sleep(1000);
				
				flags.add(type(ContentEditorPage.newReportSearchDisease, "","Disease search"));
				flags.add(waitForElementPresent(ContentEditorPage.newReportDiseaseResult, "Disease Result", 10));
					
				flags.add(type(ContentEditorPage.newReportSearchDisease, search_disease_text,"Disease search"));
				flags.add(waitForElementPresent(ContentEditorPage.newReportDiseaseResult, "Disease Result", 10));
				
			}
			
			flags.add(click(ContentEditorPage.newReportDiseaseResult, "Disease Result"));
			flags.add(click(ContentEditorPage.newReportDiseaseRightArrow, "Disease Right Arrow"));
						
			//********************
			
			flags.add(waitForElementPresent(ContentEditorPage.SaveButton, "Save Button", 5));
			flags.add(click(ContentEditorPage.SaveButton, "Save Button"));
			flags.add(accecptTheAlert());
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentActualresult.add("News Report Details filling is Successful.");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("News Report Details filling is Failed.");
		}

		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean createEditorialReport(String EditorialName) throws Throwable {
		boolean flag = true;
		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();

			flags.add(waitForElementPresent(ContentEditorPage.EditorialTree, "Editorial Report", 10));
			
			WebElement ele = Driver.findElement(ContentEditorPage.EditorialTree);
			flags.add(rightclick(ele, "NewsReport"));

			flags.add(waitForElementPresent(ContentEditorPage.RclickInsert, "Insert", 10));
			flags.add(click(ContentEditorPage.RclickInsert, "Insert"));

			flags.add(waitForElementPresent(ContentEditorPage.RclickInsertEditorialReport, "Insert Editorial", 10));
			flags.add(click(ContentEditorPage.RclickInsertEditorialReport, "Insert Editorial"));

			flags.add(waitForAlertToPresent());
			flags.add(enterTextIntoAlert(EditorialName));

			flags.add(waitForElementPresent(ContentEditorPage.Review, "Review", 10));
			flags.add(click(ContentEditorPage.Review, "Review"));

			flags.add(waitForElementPresent(ContentEditorPage.Submit, "Submit", 10));
			flags.add(click(ContentEditorPage.Submit, "Submit"));

			flags.add(waitForElementPresent(ContentEditorPage.ApproveButton, "ApproveButton", 10));
			flags.add(click(ContentEditorPage.ApproveButton, "ApproveButton"));
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}
			componentActualresult.add("Editorial Report creation is successful.");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Editorial Report creation is failed.");
		}
		return flag;
	}

	@SuppressWarnings("unchecked")
	public boolean fillEitorialReportDetails(String headline_text, String summary_text, String manager_body_text, String geocode_text,
			String search_disease_text,String CreatedDate, String CreatedTime, String UpdatedDate, String UpatedTime) throws Throwable {
		boolean flag = true;

		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			
			//********************
			
			flags.add(waitForElementPresent(ContentEditorPage.newsReportHeadline, "News Report headline", 10));
			flags.add(type(ContentEditorPage.newsReportHeadline, headline_text,"News Report headline"));
			
			flags.add(waitForElementPresent(ContentEditorPage.newsReportSummaryBody, "News Report Summary Body", 10));
			flags.add(type(ContentEditorPage.newsReportSummaryBody, summary_text, "News Report Summary Body"));
			
			flags.add(waitForElementPresent(ContentEditorPage.newsReportManagerBodyHtml, "Manager_EditHtml ", 10));
			flags.add(click(ContentEditorPage.newsReportManagerBodyHtml, "Manager _EditHtml"));
			
			flags.add(switchToFrame(ContentEditorPage.MFrame, "Main Frame"));
			flags.add(switchToFrame(ContentEditorPage.SFrame, "Sub Frame"));
			
			flags.add(waitForElementPresent(ContentEditorPage.managerBodyTextArea, "Manager body text area", 3));
			flags.add(type(ContentEditorPage.managerBodyTextArea, manager_body_text,"Manager body Text"));
			flags.add(waitForElementPresent(ContentEditorPage.acceptEditHTML, "Accept Button", 3));
			flags.add(click(ContentEditorPage.acceptEditHTML, "Accept Button"));
			
			flags.add(switchToDefaultFrame());			
			
			flags.add(waitForElementPresent(ContentEditorPage.Geocode, "Geo Code", 10));
			flags.add(type(ContentEditorPage.Geocode, geocode_text,"Geo Code"));
			
			flags.add(waitForElementPresent(ContentEditorPage.newReportSearchDisease, "Disease search", 10));
			flags.add(type(ContentEditorPage.newReportSearchDisease, search_disease_text,"Disease search"));
			
			flags.add(waitForElementPresent(ContentEditorPage.newReportDiseaseResult, "Disease Result", 10));
			
			if(! verify(getText(ContentEditorPage.newReportDiseaseResult,"Disease Result"), search_disease_text, "Disease Result")){
				Thread.sleep(1000);
				
				flags.add(type(ContentEditorPage.newReportSearchDisease, "","Disease search"));
				flags.add(waitForElementPresent(ContentEditorPage.newReportDiseaseResult, "Disease Result", 10));
					
				flags.add(type(ContentEditorPage.newReportSearchDisease, search_disease_text,"Disease search"));
				flags.add(waitForElementPresent(ContentEditorPage.newReportDiseaseResult, "Disease Result", 10));
				
			}
			
			flags.add(click(ContentEditorPage.newReportDiseaseResult, "Disease Result"));
			flags.add(click(ContentEditorPage.newReportDiseaseRightArrow, "Disease Right Arrow"));

			flags.add(waitForElementPresent(ContentEditorPage.CreatedDate, "Created Date", 10));
			flags.add(type(ContentEditorPage.CreatedDate, CreatedDate,"Created Date"));
			
			flags.add(waitForElementPresent(ContentEditorPage.CreatedTime, "Created Time", 10));
			flags.add(type(ContentEditorPage.CreatedTime, CreatedTime,"Created Time"));
			
			flags.add(waitForElementPresent(ContentEditorPage.UpdatedDate, "Upated Date", 10));
			flags.add(type(ContentEditorPage.UpdatedDate, UpdatedDate,"Upated Date"));
			
			flags.add(waitForElementPresent(ContentEditorPage.UpdatedTime, "Upated Time", 10));
			flags.add(type(ContentEditorPage.UpdatedTime, UpatedTime,"Upated Time"));
			
			//********************
			
			flags.add(waitForElementPresent(ContentEditorPage.SaveButton, "Save Button", 5));
			flags.add(click(ContentEditorPage.SaveButton, "Save Button"));
			flags.add(accecptTheAlert());
			
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentActualresult.add("News Report Details filling is Successful.");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("News Report Details filling is Failed.");
		}

		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean editHeadline(String headline) throws Throwable {
		boolean flag = true;

		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			flags.add(type(ContentEditorPage.headline,
					headline, "headline"));
			flags.add(waitForElementPresent(ContentEditorPage.SaveButton, "Save Button", 5));
			flags.add(click(ContentEditorPage.SaveButton, "Save Button"));
			Thread.sleep(2000);
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentActualresult.add("Headline is edited Successfully.");
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Headline editing is Failed.");
		}

		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean verifyHeadline(String headline) throws Throwable {
		boolean flag = true;

		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			waitForVisibilityOfElement(ContentEditorPage.headline, "headline");
			if(Driver.findElement(By.xpath("//div[@id='EditorPanel']/div[3]/table[1]//table[1]//td[2]/div[2]/input[@value='"+headline+"']")).isDisplayed())
				componentActualresult.add("Verification of headline is Successful.");
			else
				componentActualresult.add("Verification of headline Failed.");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

			
		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Creation of New Site Wizard is Failed.");
		}

		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean unpublishSiteContent() throws Throwable {
		boolean flag = true;

		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			waitForVisibilityOfElement(ContentEditorPage.neverPublish, "neverPublish");
			if(!isElementSelected(ContentEditorPage.neverPublish)) {
				flags.add(click(ContentEditorPage.neverPublish, "neverPublish"));
				flags.add(click(ContentEditorPage.SaveButton, "SaveButton"));
			}
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

			componentActualresult.add("Never Publish Site is clicked successfully.");

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("Never Publish Site is NOT clicked");
		}

		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean verifyUnpublishMessage(String msg) throws Throwable {
		boolean flag = true;

		try {
			setMethodName(Thread.currentThread().getStackTrace()[1].getMethodName());
			componentStartTimer.add(getCurrentTime());

			List<Boolean> flags = new ArrayList<>();
			new ContentEditorPage().ContentEditor_Page();
			waitForVisibilityOfElement(ContentEditorPage.neverPublish, "neverPublish");
			if(Driver.findElement(By.xpath(".//div[contains(text(),'"+msg+"')]")).isDisplayed())
				componentActualresult.add("This item will never be published because its Publishable option is disabled is displayed successfully.");
			componentEndTimer.add(getCurrentTime());
			if (flags.contains(false)) {
				throw new Exception();
			}

		} catch (Exception e) {

			flag = false;
			e.printStackTrace();
			componentEndTimer.add(getCurrentTime());
			componentActualresult.add("This item will never be published is NOT displayed");
		}

		return flag;
	}


}
